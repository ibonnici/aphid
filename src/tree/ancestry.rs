// Finding nodes ancestry, searching from the bottom-up.

use std::collections::HashSet;

use super::Tree;

//==================================================================================================
// Iterate from the given node index to the root index.
// Always yields at least the input node index,
// unless the starting index does not identify a node in the tree.

impl<'t, B, IN, TN> Tree<B, IN, TN> {
    pub(crate) fn to_root_from(&'t self, istart: usize) -> ToRootFrom<'t, B, IN, TN> {
        ToRootFrom {
            tree: (istart < self.nodes.len()).then_some(self),
            current: istart,
        }
    }
}

pub(crate) struct ToRootFrom<'t, B, IN, TN> {
    tree: Option<&'t Tree<B, IN, TN>>, // None once consumed.
    current: usize,
}

impl<B, IN, TN> Iterator for ToRootFrom<'_, B, IN, TN> {
    type Item = usize;
    fn next(&mut self) -> Option<Self::Item> {
        if let Some(tree) = self.tree {
            let yielded = self.current;
            let parent = tree.nodes[self.current].parent();
            if parent == yielded {
                // Root reached, last iteration.
                self.tree = None;
            } else {
                self.current = parent;
            }
            Some(yielded)
        } else {
            None
        }
    }
}

//==================================================================================================
// Find a common parent between two nodes.
// This method is assymetric in that the "prior" node
// is hinted to maybe happen to be the parent of the "new" node,
// which is hinted to be a leaf.
// Returns the ancestor index and flag.
// The flag is raised if "new" descends from the first (left) subtree of the ancestor.
// If "new" happens to be the ancestor itself, then
// the flag is raised if "prior" descends from the second (right) subtree.
// The flag has no value if both inputs were actually the same node.

impl<B, IN, TN> Tree<B, IN, TN> {
    pub(crate) fn common_parent(&self, new: usize, prior: usize) -> (usize, Option<bool>) {
        // Degenerated case out of the way.
        if prior == new {
            return (prior, None);
        }

        // Climb up from new to root and gather nodes indices found on the way.
        let mut path = HashSet::new();
        let mut up = self.to_root_from(new);
        let mut current = up.next().unwrap();

        loop {
            path.insert(current);
            if let Some(nextup) = up.next() {
                if nextup == prior {
                    // The prior was the sought ancestor.
                    return (prior, Some(self.is_direct_first_child(current, nextup)));
                }
                current = nextup;
            } else {
                // Root reached without a common ancestor found yet.
                break;
            }
        }

        // Prior is not ancestor of new.
        // Climb up from the prior until we come accross the path just walked from new.
        let mut up = self.to_root_from(prior);
        let mut current = up.next().unwrap();
        loop {
            if let Some(nextup) = up.next() {
                if path.contains(&nextup) {
                    return (nextup, Some(!self.is_direct_first_child(current, nextup)));
                }
                current = nextup;
            }
        }
    }

    // Retrieve ancestor index of the given named nodes.
    // None if no index was provided.
    pub(crate) fn ancestor_of(&self, mut nodes: impl Iterator<Item = usize>) -> Option<usize> {
        if let Some(mut ancestor) = nodes.next() {
            for leaf in nodes {
                (ancestor, _) = self.common_parent(leaf, ancestor);
            }
            Some(ancestor)
        } else {
            None
        }
    }
}

//==================================================================================================
#[cfg(test)]
mod tests {
    use std::iter;

    use itertools::Itertools;

    use crate::tree::{parse::tests::TestTree, Node};

    #[test]
    fn degenerated_trees() {
        let zero = TestTree::<usize>::empty();
        // Cannot call .to_root_from or .common_parent with valid indices.
        assert_eq!(zero.ancestor_of(iter::empty()), None);

        let unit = TestTree::<usize>::from_input("A:1;", &mut ()).unwrap();
        assert_eq!(unit.to_root_from(0).next(), Some(0));
        assert_eq!(unit.common_parent(0, 0), (0, None));
        assert_eq!(unit.ancestor_of(iter::once(0)), Some(0));
    }

    #[test]
    fn find_ancestors() {
        use Node as N;

        let input = "(A:1,(B:2,(((C:3,D:4):5,E:6):7,F:8):9):10):0;";
        //
        //  Node ids:                       Branches lengths:
        //
        //        │                              │0
        //     ┌──0──────┐                    ┌──┴──────┐
        //     │         │                    │         │10
        //     │   ┌─────2─────┐              │   ┌─────┴─────┐
        //     │   │           │              │   │           │9
        //     │   │        ┌──4───┐          │   │        ┌──┴───┐
        //     │   │        │      │          │   │        │7     │
        //     │   │     ┌──5──┐   │          │1  │2    ┌──┴──┐   │
        //     │   │     │     │   │          │   │     │5    │   │8
        //     │   │   ┌─6─┐   │   │          │   │   ┌─┴─┐   │6  │
        //     1   3   7   8   9   10         │   │   │3  │4  │   │
        //     A   B   C   D   E   F          A   B   C   D   E   F

        //------------------------------------------------------------------------------------------
        // Basic climbing up.
        let tree = TestTree::<usize>::from_input(input, &mut ()).unwrap();
        let upfrom = |start, expected_sequence: &[usize]| {
            let actual = tree.to_root_from(start).collect::<Vec<_>>();
            assert_eq!(actual, expected_sequence);
        };

        upfrom(0, &[0]);
        upfrom(1, &[1, 0]);
        upfrom(2, &[2, 0]);
        upfrom(3, &[3, 2, 0]);
        upfrom(7, &[7, 6, 5, 4, 2, 0]);

        //------------------------------------------------------------------------------------------
        // Find paired common parents.

        let comm = |(a, b), (parent, first)| {
            println!("Testing that {a} and {b} have {parent} ({first:?}) as a common parent.");
            let (p, f) = tree.common_parent(a, b);
            assert_eq!((parent, first), (p, f));
            if let Some(f) = f {
                println!("  Same parent with flipped left-descendance for {b} and {a}..");
                assert_eq!((parent, Some(!f)), tree.common_parent(b, a));
            }
        };

        // Degenerated pair is always its own parent.
        for i in 0..tree.nodes.len() {
            comm((i, i), (i, None));
        }

        // Root node is always the common parent.
        comm((1, 0), (0, Some(true)));
        for i in 2..tree.nodes.len() {
            // (all other nodes are right-descendants of the root)
            comm((i, 0), (0, Some(false)));
        }

        // Pairs with one fathering the other.
        comm((3, 2), (2, Some(true)));
        comm((7, 4), (4, Some(true)));
        comm((8, 4), (4, Some(true)));
        comm((7, 2), (2, Some(false)));
        comm((8, 2), (2, Some(false)));
        comm((10, 2), (2, Some(false)));
        comm((9, 5), (5, Some(false)));
        comm((8, 5), (5, Some(true)));

        // Leaves vs unrelated.
        comm((1, 2), (0, Some(true)));
        comm((1, 5), (0, Some(true)));
        comm((1, 8), (0, Some(true)));
        comm((3, 5), (2, Some(true)));
        comm((8, 3), (2, Some(false)));
        comm((8, 10), (4, Some(true)));

        //------------------------------------------------------------------------------------------
        // Find ancestor by names.

        let anc = |res, names: &str| {
            // The result should be the same for every permutation of the given group.
            let names = names.split_whitespace().collect::<Vec<_>>();
            let perms = names.iter().copied().permutations(names.len());
            for names in perms {
                println!(
                    "Testing that {:?} have {res} as a common parent.",
                    names.iter().join(" ")
                );
                assert_eq!(
                    tree.ancestor_of(tree.nodes.iter().enumerate().filter_map(|(i, node)| {
                        match node {
                            N::Terminal(tnode) => {
                                names.iter().any(|&name| tnode.payload == name).then_some(i)
                            }
                            N::Internal(_) => None,
                        }
                    }))
                    .unwrap(),
                    res,
                );
            }
        };

        // All single leaves yield themselves.
        for name in "A B C D E F".split_whitespace() {
            let i = tree
                .nodes
                .iter()
                .position(|node| match node {
                    N::Terminal(tnode) => tnode.payload == name,
                    N::Internal(_) => false,
                })
                .unwrap();
            anc(i, name);
        }

        // Beyond single leaves, the most outgroup name is necessary,
        // but only one name among the others is required.
        let anc = |res, outgroup: &str, inners: &str| {
            let inners = inners.split_whitespace().collect::<Vec<_>>();
            for n_inners in 1..=inners.len() {
                let combinations = inners.iter().copied().combinations(n_inners);
                for comb in combinations {
                    let names = format!("{outgroup} {}", comb.join(" "));
                    anc(res, &names);
                }
            }
        };
        anc(6, "D", "C");
        anc(5, "E", "C D");
        anc(4, "F", "C D E");
        anc(2, "B", "C D E F");
        anc(0, "A", "C D E F B");
    }
}
