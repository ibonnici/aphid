// Same as root_to_leaves, but return None to not descend into the given node.

use super::{InternalNode, Node, TerminalNode, Tree};

impl<B, IN, TN> Tree<B, IN, TN> {
    pub(crate) fn filter_node_to_leaves<'t, A, T, I, V>(
        &'t self,
        i_start: usize,
        init: I,
        accumulator: A,
        terminator: T,
    ) -> FilterNodeToLeaves<'t, B, IN, TN, A, T, I, V>
    where
        I: Clone,
        A: Fn(I, (usize, &'t InternalNode<B, IN>, Option<bool>)) -> Option<I>,
        T: Fn(I, (usize, &'t TerminalNode<B, TN>, Option<bool>)) -> Option<V>,
    {
        FilterNodeToLeaves::new(self, i_start, init, accumulator, terminator)
    }

    #[cfg(test)]
    pub(crate) fn filter_root_to_leaves<'n, A, T, I, V>(
        &'n self,
        init: I,
        accumulator: A,
        terminator: T,
    ) -> FilterNodeToLeaves<'n, B, IN, TN, A, T, I, V>
    where
        I: Clone,
        A: Fn(I, (usize, &'n InternalNode<B, IN>, Option<bool>)) -> Option<I>,
        T: Fn(I, (usize, &'n TerminalNode<B, TN>, Option<bool>)) -> Option<V>,
    {
        self.filter_node_to_leaves(self.root(), init, accumulator, terminator)
    }
}

pub(crate) struct FilterNodeToLeaves<'n, B, IN, TN, A, T, I, V>
where
    I: Clone,
    A: Fn(I, (usize, &'n InternalNode<B, IN>, Option<bool>)) -> Option<I>,
    T: Fn(I, (usize, &'n TerminalNode<B, TN>, Option<bool>)) -> Option<V>,
{
    nodes: &'n [Node<B, IN, TN>],
    stack: Vec<(I, usize, usize, Option<bool>)>,
    i_node: usize,
    first_child: Option<bool>,
    next_child: usize,
    accumulator: A,
    terminator: T,
    accumulated_value: Option<I>,
}

impl<'n, B, IN, TN, A, T, I, V> FilterNodeToLeaves<'n, B, IN, TN, A, T, I, V>
where
    I: Clone,
    A: Fn(I, (usize, &'n InternalNode<B, IN>, Option<bool>)) -> Option<I>,
    T: Fn(I, (usize, &'n TerminalNode<B, TN>, Option<bool>)) -> Option<V>,
{
    fn new(
        tree: &'n Tree<B, IN, TN>,
        i_start: usize,
        init: I,
        accumulator: A,
        terminator: T,
    ) -> Self {
        let first_child = (i_start > 0).then(|| {
            let parent = tree.nodes[i_start].parent();
            tree.is_direct_first_child(i_start, parent)
        });
        Self {
            nodes: &tree.nodes,
            i_node: i_start,
            first_child,
            next_child: 0,
            stack: Vec::new(),
            accumulator,
            terminator,
            accumulated_value: if tree.is_empty() { None } else { Some(init) },
        }
    }

    // Extracted from Iterator::next() for factorization purpose.
    fn backtrack(&mut self) {
        // Backwards iteration from the tip to the last unfinished internal node.
        'b: while let Some((a, i, c, f)) = self.stack.pop() {
            if c == 1 {
                // Both children of this internal node have already been visited.
                continue 'b;
            }
            // Extract this unfinished terminal node for further forward exploration.
            self.i_node = i;
            self.accumulated_value = Some(a); // Won't happen if the stack is empty.
            self.next_child = c + 1;
            self.first_child = f;
            break;
        }
    }
}

impl<'n, B, IN, TN, A, T, I, V> Iterator for FilterNodeToLeaves<'n, B, IN, TN, A, T, I, V>
where
    I: Clone,
    A: Fn(I, (usize, &'n InternalNode<B, IN>, Option<bool>)) -> Option<I>,
    T: Fn(I, (usize, &'n TerminalNode<B, TN>, Option<bool>)) -> Option<V>,
{
    type Item = (usize, V);
    fn next(&mut self) -> Option<Self::Item> {
        use Node as N;
        'f: loop {
            self.accumulated_value.as_ref()?; // Iterator consumed if this is not anymore available.
            match &self.nodes[self.i_node] {
                N::Internal(node) => {
                    if self.next_child == 0 {
                        // First time encountering this internal node: update accumulator.
                        let acc = (self.accumulator)(
                            // Extract owned value to pass it through the accumulator.
                            self.accumulated_value.take().unwrap(),
                            (self.i_node, node, self.first_child),
                        );
                        if acc.is_none() {
                            // The caller wishes to skip this node.
                            self.backtrack(); // Reset the accumulator to a previous value.
                            continue 'f;
                        }
                        self.accumulated_value = acc;
                    }
                    // Record for further backtracking. Cloning of the accumulator happens here.
                    self.stack.push((
                        self.accumulated_value.as_ref().unwrap().clone(),
                        self.i_node,
                        self.next_child,
                        self.first_child,
                    ));
                    // Step down to focal child.
                    self.i_node = node.children[self.next_child];
                    self.first_child = Some(self.next_child == 0);
                    // Going forward, only non-visited nodes can be found.
                    self.next_child = 0;
                    continue 'f;
                }
                N::Terminal(node) => {
                    // We have found a tip. Update accumulator to this terminal node.
                    let value = (self.terminator)(
                        self.accumulated_value.take().unwrap(),
                        (self.i_node, node, self.first_child),
                    );
                    if let Some(value) = value {
                        let to_yield = (self.i_node, value);
                        self.backtrack();
                        return Some(to_yield);
                    }
                    // The caller wishes to skip this leaf.
                    self.backtrack();
                    continue 'f;
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashSet;

    use super::*;
    use crate::tree::parse::tests::TestTree;

    #[test]
    #[allow(unused_mut, unused_variables)] // Triggered when all nodes are elided in the test.
    #[allow(clippy::too_many_lines)] // Testing a lot leads to numerous lines.
    fn paths_and_lengths() {
        use Node as N;

        // Iterate over root-to-tips paths, filtering nodes out.
        // (copied and adapted to root_to_leaves test)
        macro_rules! test_root_to_tips {
        (:$($input:literal =>
            $( skips $skips:tt
                $(from $start_node:literal: {$($tip:ident: $path:tt $flags:tt -> $length:expr)*})*
            )*
           )*) => {$({
                let input = $input;
                println!("Testing skips on tree:\n  {input:?}");
                let tree = TestTree::<usize>::from_input(input, &mut ()).unwrap();
                $(
                  let skips = $skips.into_iter().collect::<HashSet<usize>>();
                  println!("  skips {:?}", skips);
                  let included = |i: usize| !skips.contains(&i);
                  $(
                      let i_start = $start_node;
                      println!("    From node {i_start}.");
                      let mut paths = tree.filter_node_to_leaves(i_start,
                          (Vec::new(), Vec::new()),
                          |(mut path, mut flags), (i, _, f)| {
                              path.push(i);
                              flags.push(f);
                              included(i).then_some((path, flags))
                          },
                          |(mut path, mut flags), (i, _, f)| {
                              path.push(i);
                              flags.push(f);
                              included(i).then_some((path, flags))
                          },
                      );
                      let mut lengths = tree.filter_node_to_leaves(i_start,
                          0,
                          |length, (i, inode, _)| included(i).then(||length + inode.branch),
                          |length, (i, tnode, _)| included(i).then(|| (tnode.payload, length + tnode.branch)),
                          );
                     $({
                         let name = stringify!($tip);
                         let i = tree.nodes.iter().position(|node| match node {
                             N::Terminal(tnode) => tnode.payload == name,
                             N::Internal(_) => false,
                         }).unwrap_or_else(|| panic!("Invalid node name: {name:?}."));
                         let flags = $flags.map(|f| (f<2).then_some(f != 0))
                                           .into_iter().collect::<Vec<_>>();
                         assert_eq!(Some((i, (vec!$path, flags))), paths.next(),
                             "Invalid path from root to tip.");

                         let (j, (leaf_name, act)) = lengths.next().expect("Missing branch length.");
                         assert_eq!(i, j, "Iterators should yield leaves in the same order.");
                         assert_eq!(name, leaf_name, "Iterators should yield leaves in the same order.");
                         assert_eq!($length, act, "Invalid length from root totip.");
                 })*
               )*
               assert_eq!(paths.next(), None, "Unexpected path.");
               )*
            })*};
        }

        let r = 2;
        test_root_to_tips! {:

            // Degenerated unit tree.
            "A:1;" => skips [] from 0: { A: [0] [r] -> 1}
            "A:1;" => skips [0] from 0: {}

            // Basic tree.
            //i:1   2   0
            "(A:1,B:2):100;" =>
                // Skip nothing.
                skips [] from 0: {
                    A: [0, 1] [r, 1] -> 101
                    B: [0, 2] [r, 0] -> 102
                }
                // Skip one leaf.
                skips [1] from 0: { B: [0, 2] [r, 0] -> 102 }
                          from 1: {}
                          from 2: { B: [2] [0] -> 2 }
                skips [2] from 0: { A: [0, 1] [r, 1] -> 101 }
                          from 1: { A: [1] [1] -> 1 }
                          from 2: {}

                // Skip root.
                skips [0] from 0: {}
                          // Node can't be skipped if starting point is lower.
                          from 1: { A: [1] [1] -> 1 }
                          from 2: { B: [2] [0] -> 2 }
                // Skip all leaves and equivalents.
                skips [1, 2]    from 0: {} from 1: {} from 2: {}
                skips [0, 1, 2] from 0: {} from 1: {} from 2: {}
                skips [0, 1]    from 0: {} from 1: {} from 2: { B: [2] [0] -> 2 }
                skips [0, 2]    from 0: {} from 2: {} from 1: { A: [1] [1] -> 1 }

            // Triplet.
            //i:1    3   4  2  0
            "(A:1,(B:2,C:3):4):100;" =>
                // Skip nothing.
                skips []
                    from 0: {
                        A: [0, 1]    [r, 1]    -> 101
                        B: [0, 2, 3] [r, 0, 1] -> 106
                        C: [0, 2, 4] [r, 0, 0] -> 107
                    }
                    from 1: { A: [1] [1] -> 1 }
                    from 2: {
                        B: [2, 3] [0, 1] -> 6
                        C: [2, 4] [0, 0] -> 7
                    }
                    from 3: { B: [3] [1] -> 2 }
                    from 4: { C: [4] [0] -> 3 }
                // Skip first leaf.
                skips [1]
                    from 0: {
                        B: [0, 2, 3] [r, 0, 1] -> 106
                        C: [0, 2, 4] [r, 0, 0] -> 107
                    }
                    from 1: {}
                    // Silent skip because the visit does not run accross it.
                    from 2: {
                        B: [2, 3] [0, 1] -> 6
                        C: [2, 4] [0, 0] -> 7
                    }
                    from 3: { B: [3] [1] -> 2 }
                    from 4: { C: [4] [0] -> 3 }
                // Skip last leaves
                skips [3]
                    from 0: {
                        A: [0, 1]    [r, 1]    -> 101
                        C: [0, 2, 4] [r, 0, 0] -> 107
                    }
                    from 1: { A: [1] [1] -> 1 }
                    from 2: { C: [2, 4] [0, 0] -> 7 }
                    from 3: {}
                    from 4: { C: [4] [0] -> 3 }
                skips [4]
                    from 0: {
                        A: [0, 1]    [r, 1]    -> 101
                        B: [0, 2, 3] [r, 0, 1] -> 106
                    }
                    from 1: { A: [1] [1] -> 1 }
                    from 2: { B: [2, 3] [0, 1] -> 6 }
                    from 3: { B: [3] [1] -> 2 }
                    from 4: {}
                // Skip internal node.
                skips [2]
                    from 0: { A: [0, 1] [r, 1] -> 101 }
                    from 1: { A: [1] [1] -> 1 }
                    from 2: {}
                    from 3: { B: [3] [1] -> 2 }
                    from 4: { C: [4] [0] -> 3 }
                // Skip root or equivalent..
                skips [0]
                    from 0: {}
                    from 1: { A: [1] [1] -> 1 }
                    from 2: {
                        B: [2, 3] [0, 1] -> 6
                        C: [2, 4] [0, 0] -> 7
                    }
                    from 3: { B: [3] [1] -> 2 }
                    from 4: { C: [4] [0] -> 3 }
                skips [1, 2]
                    from 0: {}
                    from 1: {}
                    from 2: {}
                    from 3: { B: [3] [1] -> 2 }
                    from 4: { C: [4] [0] -> 3 }
                skips [1, 3, 4] from 0: {} from 1: {} from 2: {} from 3: { } from 4: { }

            // One more elaborate example:
            //
            //    (branches lengths)          (nodes indexes)
            //
            //       │11                          │
            //   ┌───┴────┐                   ┌───0────┐
            //   │        │10                 │        │
            //   │   ┌────┴──────┐            │   ┌────2──────┐
            //   │   │           │9           │   │           │
            //   │   │       ┌───┴───┐        │   │       ┌───4───┐
            //   │   │       │7      │        │   │       │       │
            //   │1  │2    ┌─┴───┐   │        │   │     ┌─5───┐   │
            //   │   │     │     │   │        │   │     │     │   │
            //   │   │     │5    │6  │8       │   │     │     │   │
            //   │   │   ┌─┴─┐   │   │        │   │   ┌─6─┐   │   │
            //   │   │   │3  │4  │   │        1   3   7   8   9   10
            //   A   B   C   D   E   F        A   B   C   D   E   F
            //
            "(A:1,(B:2,(((C:3,D:4):5,E:6):7,F:8):9):10):11;" =>
                // Skip nothing.
                skips []
                    from 0: {
                        A: [0, 1]              [r, 1]             -> 11 + 1
                        B: [0, 2, 3]           [r, 0, 1]          -> 11 + 10 + 2
                        C: [0, 2, 4, 5, 6, 7]  [r, 0, 0, 1, 1, 1] -> 11 + 10 + 9 + 7 + 5 + 3
                        D: [0, 2, 4, 5, 6, 8]  [r, 0, 0, 1, 1, 0] -> 11 + 10 + 9 + 7 + 5 + 4
                        E: [0, 2, 4, 5, 9]     [r, 0, 0, 1, 0]    -> 11 + 10 + 9 + 7 + 6
                        F: [0, 2, 4, 10]       [r, 0, 0, 0]       -> 11 + 10 + 9 + 8
                    }
                    from 5: {
                        C: [5, 6, 7]  [1, 1, 1] -> 7 + 5 + 3
                        D: [5, 6, 8]  [1, 1, 0] -> 7 + 5 + 4
                        E: [5, 9]     [1, 0]    -> 7 + 6
                    }
                    from 6: {
                        C: [6, 7]  [1, 1] -> 5 + 3
                        D: [6, 8]  [1, 0] -> 5 + 4
                    }
                // Skip individual leaves.
                skips [3, 9]
                    from 0: {
                        A: [0, 1]              [r, 1]             -> 11 + 1
                        C: [0, 2, 4, 5, 6, 7]  [r, 0, 0, 1, 1, 1] -> 11 + 10 + 9 + 7 + 5 + 3
                        D: [0, 2, 4, 5, 6, 8]  [r, 0, 0, 1, 1, 0] -> 11 + 10 + 9 + 7 + 5 + 4
                        F: [0, 2, 4, 10]       [r, 0, 0, 0]       -> 11 + 10 + 9 + 8
                    }
                    from 5: {
                        C: [5, 6, 7]  [1, 1, 1] -> 7 + 5 + 3
                        D: [5, 6, 8]  [1, 1, 0] -> 7 + 5 + 4
                    }
                    from 6: {
                        C: [6, 7]  [1, 1] -> 5 + 3
                        D: [6, 8]  [1, 0] -> 5 + 4
                    }
                skips [3, 9, 7, 10]
                    from 0: {
                        A: [0, 1]              [r, 1]             -> 11 + 1
                        D: [0, 2, 4, 5, 6, 8]  [r, 0, 0, 1, 1, 0] -> 11 + 10 + 9 + 7 + 5 + 4
                    }
                    from 5: { D: [5, 6, 8]  [1, 1, 0] -> 7 + 5 + 4 }
                    from 6: { D: [6, 8]     [1, 0]    ->     5 + 4 }
                // Skip internal nodes.
                skips [6]
                    from 0: {
                        A: [0, 1]              [r, 1]             -> 11 + 1
                        B: [0, 2, 3]           [r, 0, 1]          -> 11 + 10 + 2
                        E: [0, 2, 4, 5, 9]     [r, 0, 0, 1, 0]    -> 11 + 10 + 9 + 7 + 6
                        F: [0, 2, 4, 10]       [r, 0, 0, 0]       -> 11 + 10 + 9 + 8
                    }
                    from 5: { E: [5, 9] [1, 0] -> 7 + 6 }
                    from 6: {}
                skips [5]
                    from 0: {
                        A: [0, 1]              [r, 1]             -> 11 + 1
                        B: [0, 2, 3]           [r, 0, 1]          -> 11 + 10 + 2
                        F: [0, 2, 4, 10]       [r, 0, 0, 0]       -> 11 + 10 + 9 + 8
                    }
                    from 5: {}
                    from 6: {
                        C: [6, 7]  [1, 1] -> 5 + 3
                        D: [6, 8] [1, 0] -> 5 + 4
                    }
                skips [4]
                    from 0: {
                        A: [0, 1]              [r, 1]             -> 11 + 1
                        B: [0, 2, 3]           [r, 0, 1]          -> 11 + 10 + 2
                    }
                    from 5: {
                        C: [5, 6, 7]  [1, 1, 1] -> 7 + 5 + 3
                        D: [5, 6, 8]  [1, 1, 0] -> 7 + 5 + 4
                        E: [5, 9]     [1, 0]    -> 7 + 6
                    }
                    from 6: {
                        C: [6, 7]  [1, 1] -> 5 + 3
                        D: [6, 8]  [1, 0] -> 5 + 4
                    }
                skips [2]
                    from 0: { A: [0, 1] [r, 1] -> 11 + 1 }
                    from 5: {
                        C: [5, 6, 7]  [1, 1, 1] -> 7 + 5 + 3
                        D: [5, 6, 8]  [1, 1, 0] -> 7 + 5 + 4
                        E: [5, 9]     [1, 0]    -> 7 + 6
                    }
                    from 6: {
                        C: [6, 7]  [1, 1] -> 5 + 3
                        D: [6, 8]  [1, 0] -> 5 + 4
                    }
                // Skip a mix.
                skips [5, 3]
                    from 0: {
                        A: [0, 1]              [r, 1]             -> 11 + 1
                        F: [0, 2, 4, 10]       [r, 0, 0, 0]       -> 11 + 10 + 9 + 8
                    }
                    from 5: {}
                    from 6: {
                        C: [6, 7]  [1, 1] -> 5 + 3
                        D: [6, 8]  [1, 0] -> 5 + 4
                    }
                // Skip root or equivalent.
                skips [0] from 0: {}
                skips [1, 2] from 0: {}
                skips [1, 3, 4] from 0: {}
                skips [1, 3, 10, 5] from 0: {}
                skips [1, 3, 10, 9, 6] from 0: {}
                skips [1, 3, 10, 9, 7, 8] from 0: {}
        }
    }

    #[test]
    fn empty_tree() {
        let zero = TestTree::<f64>::empty();
        assert!(zero
            .filter_root_to_leaves(
                0.,
                |acc, (_, inode, _)| Some(acc + inode.branch),
                |acc, (_, tnode, _)| Some(acc + tnode.branch),
            )
            .next()
            .is_none());
    }
}
