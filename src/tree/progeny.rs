// Finding nodes progeny, from the top down.

use super::{Node, TerminalNode, Tree};

impl<B, IN, TN> Tree<B, IN, TN> {
    // Iterate over all children of the given node (depth-first, post-order).
    pub(crate) fn children_of(&self, index: usize) -> impl Iterator<Item = usize> + '_ {
        Children::new(&self.nodes, index)
    }

    // Iterate over all children *leaves* of the given node.
    pub(crate) fn leaves_of(&self, index: usize) -> impl Iterator<Item = &TerminalNode<B, TN>> {
        self.children_of(index).filter_map(|i| {
            if let Node::Terminal(node) = &self.nodes[i] {
                Some(node)
            } else {
                None
            }
        })
    }
}

// Iterate over children, depth-first, post-order.
struct Children<'n, B, IN, TN> {
    // Ref to original nodes.
    nodes: &'n Vec<Node<B, IN, TN>>,
    // The last element in the stack is the next to be yielded.
    // The associated flag is raised when all children have been pushed.
    stack: Vec<(usize, bool)>,
}

impl<'n, B, IN, TN> Children<'n, B, IN, TN> {
    fn new(nodes: &'n Vec<Node<B, IN, TN>>, init: usize) -> Self {
        use Node as N;
        let mut i = init;
        let mut stack = Vec::new();
        while let N::Internal(node) = &nodes[i] {
            stack.push((i, false));
            i = node.children[0];
        }
        stack.push((i, false));
        Self { nodes, stack }
    }
}

impl<B, IN, TN> Iterator for Children<'_, B, IN, TN> {
    type Item = usize;
    fn next(&mut self) -> Option<Self::Item> {
        use Node as N;
        let (mut i_node, all_children_pushed) = self.stack.pop()?;
        if let (N::Internal(node), false) = (&self.nodes[i_node], all_children_pushed) {
            // Don't yield current node yet,
            // because we need to yield its children first.
            self.stack.push((i_node, true));
            let mut i = node.children[1];
            while let N::Internal(node) = &self.nodes[i] {
                self.stack.push((i, false));
                i = node.children[0];
            }
            i_node = i;
        }
        Some(i_node)
    }
}

#[cfg(test)]
mod tests {
    use crate::tree::parse::tests::TestTree;

    #[test]
    fn children() {
        let input = "(A:1,(B:2,(((C:3,D:4):5,E:6):7,F:8):9):10):0;";
        //
        //   (branches lengths)          (nodes indexes)
        //          1
        //  ┌───────────── A            ┌────────────1 A
        // 0│         2                 │
        // ─┤    ┌──────── B           ─0    ┌───────3 B
        //  │    │       3              │    │
        //  │ 10 │    5┌── C            │    │     ┌─7 C
        //  └────┤  7┌─┤ 4              └────2   ┌─6
        //       │ ┌─┤ └── D                 │ ┌─5 └─8 D
        //       │ │ │ 6                     │ │ │
        //       │9│ └──── E                 │ │ └───9 E
        //       └─┤  8                      └─4
        //         └────── F                   └────10 F
        //

        let mut tree = TestTree::<usize>::from_input(input, &mut ()).unwrap();

        macro_rules! children {
            ($i:literal => $children:tt) => {
                assert_eq!(tree.children_of($i).collect::<Vec<_>>(), vec!$children);
            };
        }
        children!(7 => [7]);
        children!(6 => [7, 8, 6]);
        children!(5 => [7, 8, 6, 9, 5]);
        children!(4 => [7, 8, 6, 9, 5, 10, 4]);
        children!(2 => [3, 7, 8, 6, 9, 5, 10, 4, 2]);
        children!(0 => [1, 3, 7, 8, 6, 9, 5, 10, 4, 2, 0]);

        // Iterate degenerated trees.
        tree = TestTree::from_input("A:1;", &mut ()).unwrap();
        children!(0 => [0]);
    }

    #[test]
    fn leaves() {
        // Same example.
        let input = "(A:1,(B:2,(((C:3,D:4):5,E:6):7,F:8):9):10):0;";
        let mut tree = TestTree::<usize>::from_input(input, &mut ()).unwrap();

        macro_rules! leaves {
            ($i:literal => [$($leaf:ident),+]) => {
                assert_eq!(tree.leaves_of($i).map(|node| node.payload).collect::<Vec<_>>(),
                           vec![$(stringify!($leaf)),+]);
            };
        }
        leaves!(7 => [C]);
        leaves!(6 => [C, D]);
        leaves!(5 => [C, D, E]);
        leaves!(4 => [C, D, E, F]);
        leaves!(2 => [B, C, D, E, F]);
        leaves!(0 => [A, B, C, D, E, F]);

        // Iterate degenerated trees.
        tree = TestTree::from_input("A:1;", &mut ()).unwrap();
        leaves!(0 => [A]);
    }

    #[test]
    #[should_panic = "index out of bounds: the len is 0 but the index is 0"]
    #[allow(unused_must_use)]
    fn empty_tree_children() {
        assert_eq!(TestTree::<()>::empty().children_of(0).next(), None);
    }

    #[test]
    #[should_panic = "index out of bounds: the len is 0 but the index is 0"]
    #[allow(unused_must_use)]
    fn empty_tree_leaves() {
        assert_eq!(TestTree::<()>::empty().leaves_of(0).next(), None);
    }
}
