// Parse tree from Newick input format strings.
// Do this by extending the lexer
// so it can parse a tree from the input.

use super::{InternalNode, LexerError, Node, TerminalNode, Tree};
use crate::lexer::{errout, Lexer};

// Gene trees in particular intern species names strings as they encounter them,
// so they need to carry on some mutable state.
// Abstract over this with this trait: a value implementing this should be given
// to the lexer so it can parse the tree.
pub(crate) trait TreeDataReader<'i, B, IN, TN> {
    fn parse_branch_data(
        &mut self,
        input: &'i str,
        // Some additional context useful to produce error messages, but not only.
        i_node: usize,
        is_internal: bool,
        is_last: bool,
    ) -> Result<B, LexerError>;
    fn parse_internal_node_data(&mut self, input: &'i str, i_node: usize)
        -> Result<IN, LexerError>;
    fn parse_terminal_node_data(&mut self, input: &'i str, i_node: usize)
        -> Result<TN, LexerError>;
}

impl<'i> Lexer<'i> {
    // List of special chars supposed to delimit the newick format tokens,
    const SPECIALS: [char; 5] = ['(', ')', ',', ':', ';'];

    // Parse tree from the begininng of the input string,
    // and return the remaining unparsed slice.
    #[allow(clippy::too_many_lines)] // Extracting more methods to shorten it actually complicates it.
    pub(crate) fn read_tree<B, IN, TN>(
        &mut self,
        reader: &mut impl TreeDataReader<'i, B, IN, TN>,
    ) -> Result<Tree<B, IN, TN>, LexerError>
    where
        B: Default,
        IN: Default,
    {
        use Node as N;

        // Nodes are constructed as soon as their existence is proven (pre-order)
        // and resolved later with the additional input (post-order).
        // In particular, the indexes of "parent" internal nodes
        // are not immediately known.
        // Keep track of all such unresolved nodes,
        // and of their child currently needing resolution.
        struct Unresolved {
            i_parent: usize,
            i_child: u8,
        }

        // Rule out empty tree.
        if self.trim_start().strip_char([';']).is_some() {
            return Ok(Tree::empty());
        };

        let mut nodes = Vec::new();
        let mut unresolved = Vec::new();

        // Factorize common procedures.
        // Index into nodes when we can prove that the result is an internal node.
        macro_rules! internal {
            ($nodes:ident[$i:expr]) => {
                match &mut $nodes[$i] {
                    N::Internal(node) => node,
                    N::Terminal(_) => unreachable!(), // TODO: uncheck?
                }
            };
        }

        // One iteration per created node.
        'n: loop {
            let i_new_node = nodes.len();

            // Every new node contributes to the resolution of one parent.
            let (mut i_parent, mut i_child) = match unresolved.last() {
                Some(&Unresolved { i_parent, i_child }) => {
                    internal!(nodes[i_parent]).children[i_child as usize] = i_new_node;
                    (i_parent, i_child)
                }
                None => {
                    // We are parsing the first node aka. root node.
                    // The parent to resolve is the root itself, as it is its own parent.
                    (0, 0)
                }
            };

            // Parse the node.
            if self.trim_start().strip_char(['(']).is_some() {
                // This is an internal node: push it as the next unresolved parent and start over.
                nodes.push(N::Internal(InternalNode {
                    children: [0, 0],       // Actual values shall be resolved later.
                    payload: IN::default(), // Resolved later after the closing parenthesis.
                    branch: B::default(),   // Resolved later after the closing parenthesis.
                    parent: i_parent,
                }));
                unresolved.push(Unresolved { i_parent: i_new_node, i_child: 0 });
                continue 'n;
            }

            // This is a terminal node, which can be "resolved" right away.
            let last = unresolved.is_empty(); // Degenerated "root is terminal" case.

            // Read/parse node payload.
            let (read, stop) = self.read_payload("terminal", i_new_node, last)?;
            let payload = reader.parse_terminal_node_data(read, i_new_node)?;

            // Read/parse branch data.
            let (read, mut separator) = if last && stop == ';' {
                ("", stop)
            } else {
                self.read_branch_data("terminal", i_new_node, last)?
            };
            let branch = reader.parse_branch_data(read.trim_end(), i_new_node, false, last)?;

            // Construct the node.
            nodes.push(N::Terminal(TerminalNode {
                payload,
                branch,
                parent: i_parent,
            }));

            // After terminal nodes, parentheses may be closed,
            // therefore resolving formerly constructed, yet unresolved internal nodes.
            // One iteration per resolved internal node.
            'r: loop {
                match (i_child, separator) {
                    (0, ',') => {
                        // The first child of current parent has been resolved,
                        // expect a second node then, with the same parent.
                        unresolved.last_mut().unwrap().i_child = 1;
                        continue 'n;
                    }

                    (1, ')') => {
                        // All children for the current unresolved node have been constructed.
                        // This "parent" can be resolved.
                        let parent = internal!(nodes[i_parent]);
                        let last = unresolved.len() == 1;

                        // Read/parse node payload.
                        let (mut read, stop) = self.read_payload("internal", i_parent, last)?;
                        let payload = reader.parse_internal_node_data(read, i_parent)?;

                        // Read/parse branch data.
                        (read, separator) = if last && stop == ';' {
                            ("", stop)
                        } else {
                            self.read_branch_data("internal", i_parent, last)?
                        };
                        let branch =
                            reader.parse_branch_data(read.trim_end(), i_parent, true, last)?;

                        // Resolved.
                        parent.payload = payload;
                        parent.branch = branch;
                        unresolved.pop();

                        // Look for resolution of the previously unresolved parent.
                        if let Some(next_to_resolve) = unresolved.last() {
                            Unresolved { i_parent, i_child } = *next_to_resolve;
                            continue 'r;
                        }
                        if separator != ';' {
                            errout!(
                                "Could not find termination character ';' after tree termination."
                            );
                        }
                        break 'n;
                    }

                    (1, ',') | (0, ')') => {
                        if i_child == 0 {
                            errout!("Internal node {i_parent} has only 1 child.");
                        } else {
                            errout!("Internal node {i_parent} has more than 2 children.");
                        }
                    }

                    (0, ';') => {
                        // Degenerated single-node tree with a terminal root.
                        break 'n;
                    }
                    _ => unreachable!(),
                }
            }
        }
        Ok(Tree { nodes })
    }

    // Read a node payload, stopping on either delimiter,
    // but some would reveal bugs or incorrect input.
    // The returned separator is either ':' or ';'.
    fn read_payload(
        &mut self,
        terminal: &str,
        i_node: usize,
        last: bool,
    ) -> Result<(&'i str, char), LexerError> {
        match self.trim_start().read_until(Self::SPECIALS) {
            Some((read, sep @ (':' | ';'))) if last || sep != ';' => Ok((read.trim_end(), sep)),
            Some((read, sep)) => {
                let (exp, last) = if last { (';', " the last") } else { (':', "") };
                errout!(
                    "Missing branch data after{last} {terminal} node {i_node} payload {read:?} \
                     (found {sep:?} instead of {exp:?})."
                );
            }
            None => {
                errout!(
                    "Unexpected end of tree input while reading {terminal} node {i_node} payload, \
                     is the terminal ';' marker missing?"
                );
            }
        }
    }

    // Read branch data, stopping on either delimiter,
    // but returning it as this information is useful for further parsing.
    // The returned separator is either ',', ')' or ';'.
    fn read_branch_data(
        &mut self,
        terminal: &str,
        i_node: usize,
        last: bool,
    ) -> Result<(&'i str, char), LexerError> {
        if let Some((read, sep)) = self.trim_start().read_until(Self::SPECIALS) {
            match sep {
                ',' | ')' | ';' if (sep == ';') == last => Ok((read.trim_end(), sep)),
                _ => {
                    let (exp, last) = if last { ("';'", " the last") } else { ("',' or ')'", "") };
                    errout!(
                        "Expected {exp} delimiter after{last} {terminal} branch data ({read:?}), \
                         found {sep:?} instead (node {i_node})."
                    );
                }
            }
        } else {
            let last = if last { " the last" } else { "" };
            errout!(
                "Unexpected end of tree input while reading{last} {terminal} branch data \
                 (node {i_node})."
            );
        }
    }
}

#[cfg(test)]
pub(crate) mod tests {

    impl<B, IN, TN> Tree<B, IN, TN>
    where
        B: Default,
        IN: Default,
    {
        pub(crate) fn from_input<'i>(
            input: &'i str,
            reader: &mut impl TreeDataReader<'i, B, IN, TN>,
        ) -> Result<Self, LexerError> {
            Lexer::new(input).read_tree(reader)
        }
    }

    use std::{fmt::Display, str::FromStr};

    use super::*;
    use crate::lexer::lexerr;

    // One simple tree type useful for testing purpose..
    pub(crate) type TestTree<'i, B> = Tree<B, Option<&'i str>, &'i str>;
    // .. with a stateless reader.
    impl<'i, B> TreeDataReader<'i, B, Option<&'i str>, &'i str> for ()
    where
        B: FromStr,
        <B as FromStr>::Err: Display,
    {
        fn parse_branch_data(
            &mut self,
            input: &'i str,
            _i_node: usize,
            _is_internal: bool,
            _is_last: bool,
        ) -> Result<B, LexerError> {
            input
                .parse::<B>()
                .map_err(|e| lexerr!("Could not parse {input:?} as branch data: {e}"))
        }

        fn parse_internal_node_data(
            &mut self,
            input: &'i str,
            _i_node: usize,
        ) -> Result<Option<&'i str>, LexerError> {
            Ok((!input.is_empty()).then_some(input))
        }

        fn parse_terminal_node_data(
            &mut self,
            input: &'i str,
            i_node: usize,
        ) -> Result<&'i str, LexerError> {
            if input.is_empty() {
                errout!("No leaf name given to node {i_node}.");
            }
            Ok(input)
        }
    }

    #[test]
    #[allow(clippy::too_many_lines)] // Long because it's a sequence of tests.
    fn parse_newick_trees() {
        use Node as N;

        // Convenience macro to ease testing with simple syntax.
        macro_rules! test_newick_parsing {

            // Entry point to specify various tests in one invocation.
            (:$($test:tt)*) => { $(test_newick_parsing!($test);)* };
            ({$input:literal => $error:literal}) => {
                println!("Testing Newick input (expecting failure):\n  {}", $input);
                match TestTree::<usize>::from_input($input, &mut ()) {
                    Ok(_) => {
                        panic!("Unexpected successful parsing while expecting the following error: \
                               \n  {}", $error);
                    }
                    Err(e) => { assert_eq!($error, &format!("{e}")); }
                }
            };

            ({$input:literal => {$($name:ident -> $i:literal),*$(,)?} $($node:tt)*
            }) => {
                println!("Testing Newick input (expecting success):\n  {}", $input);
                let mut input = String::from($input);
                input.push_str(" rest"); // Extra input to *not* consume.
                let mut lexer = Lexer::new(&input);
                let tree: Result<TestTree<usize>, _> = lexer.read_tree(&mut ());
                #[allow(clippy::vec_init_then_push)]
                match tree {
                    Ok(tree) => {
                        #[allow(unused_mut)] // Unused on empty tree.
                        let mut nodes = Vec::new();
                        $(test_newick_parsing!(node $node, nodes);)*
                        // Check underlying array of nodes.
                        for (act, ref exp) in tree.nodes.iter().zip(nodes) {
                            assert_eq!(act, exp);
                        }
                        // Check that the lexer has not over-consumed input.
                        assert_eq!(lexer.rest(), " rest");
                    }
                    Err(e) => {
                        panic!("Unexpected parse failure:\n  {e}");
                    }
                }
            };

            // Unnamed internal node (explicit index then).
            (node [<$id:literal>: $parent:literal -> ($a:literal, $b:literal) : $branch:expr],
             $nodes:expr) => {
                assert_eq!($nodes.len(), $id, "Expected nodes seem out of order.");
                $nodes.push(N::Internal(InternalNode {
                    children: [$a, $b],
                    payload: None,
                    branch: $branch,
                    parent: $parent,
                }));
            };
            // Named internal node.
            (node [$name:ident: $parent:literal -> ($a:literal, $b:literal) : $branch:expr],
             $nodes:expr) => {
                $nodes.push(N::Internal(InternalNode {
                    children: [$a, $b],
                    payload: Some(stringify!($name)),
                    branch: $branch,
                    parent: $parent,
                }));
            };
            // Terminal node.
            (node [$name:ident: $parent:literal -> () : $branch:expr],
             $nodes:expr) => {
                $nodes.push(N::Terminal(TerminalNode {
                    payload: stringify!($name),
                    branch: $branch,
                    parent: $parent,
                }));
            };

        }

        test_newick_parsing! {:

            // Degenerated null tree.
            { ";" => {} }

            // Degenerated unit tree, with only one (terminal) root node.
            { "A:0;" => { A -> 0 } [A: 0 -> () : 0] }
            // Whitespace allowed.
            { " A : 1 ;" => { A -> 0 } [A: 0 -> () : 1] }

            // Basic tree.
            {
                "(A:1,B:2):0;" => {A -> 1, B -> 2}
                 [ <0>: 0 -> (1, 2) : 0 ]
                 [ A: 0 -> () : 1 ]
                 [ B: 0 -> () : 2 ]
            }
            // Name root node.
            {
                "(A:1,B:2)root:0;" => {A -> 1, B -> 2, root -> 0} // <- So it appears in the index.
                 [ root: 0 -> (1, 2) : 0 ]
                 [ A: 0 -> () : 1 ]
                 [ B: 0 -> () : 2 ]
            }
            // Set root branch length.
            {
                "(A:1,B:2):3;" => {A -> 1, B -> 2}
                 [ <0>: 0 -> (1, 2) : 3 ]
                 [ A: 0 -> () : 1 ]
                 [ B: 0 -> () : 2 ]
            }
            // Do both.
            {
                "(A:1,B:2)root:3;" => {A -> 1, B -> 2, root -> 0}
                 [ root: 0 -> (1, 2) : 3 ]
                 [ A: 0 -> () : 1 ]
                 [ B: 0 -> () : 2 ]
            }
            // Whitespace allowed.
            {
                " ( A : 1 , B : 2 ) root : 3 ;" => {A -> 1, B -> 2, root -> 0}
                 [ root: 0 -> (1, 2) : 3 ]
                 [ A: 0 -> () : 1 ]
                 [ B: 0 -> () : 2 ]
            }
            // Check errors consistency.
            { "(A:1,B:2)" => "Unexpected end of tree input while reading internal node 0 payload, \
                              is the terminal ';' marker missing?" }
            { "(A:1,B:2;" => "Expected ',' or ')' delimiter after terminal branch data (\"2\"), \
                              found ';' instead (node 2)." }
            { "(A:1,B:);" => "Could not parse \"\" as branch data: \
                              cannot parse integer from empty string" }
            { "(A:1,B2);" => "Missing branch data after terminal node 2 payload \"B2\" \
                              (found ')' instead of ':')." }
            { "(A:1,:2);" => "No leaf name given to node 2." }
            { "(A:1B:2)" => "Expected ',' or ')' delimiter after terminal branch data (\"1B\"), \
                             found ':' instead (node 1)." }
            { "A:1,B:2)" => "Expected ';' delimiter after the last terminal branch data (\"1\"), \
                             found ',' instead (node 0)." }
            { "(A,B:2);" => "Missing branch data after terminal node 1 payload \"A\" \
                             (found ',' instead of ':')." }
            { "(A:1,B);" => "Missing branch data after terminal node 2 payload \"B\" \
                             (found ')' instead of ':')." }
            { "(A:1);" => "Internal node 0 has only 1 child." }
            { "(A:1,B:2,C:3);" => "Internal node 0 has more than 2 children." }

            // Triplet.
            {
                "(A:1,(B:2,C:3):4):0;" => {A -> 1, B -> 3, C -> 4}
                 [ <0> : 0 -> (1, 2) : 0 ]
                 [ A   : 0 -> ()     : 1 ]
                 [ <2> : 0 -> (3, 4) : 4 ]
                 [ B   : 2 -> ()     : 2 ]
                 [ C   : 2 -> ()     : 3 ]
            }
            { // Naming internal nodes, with a non-null root length.
                "(A:1,(B:2,C:3)D:4)root:5;" => {A -> 1, B -> 3, C -> 4, D-> 2, root -> 0}
                 [ root : 0 -> (1, 2) : 5 ]
                 [ A    : 0 -> ()     : 1 ]
                 [ D    : 0 -> (3, 4) : 4 ]
                 [ B    : 2 -> ()     : 2 ]
                 [ C    : 2 -> ()     : 3 ]
            }
            { "(A:1,(B:2,C:3)D)root:5;" => "Missing branch data after internal node 2 payload \"D\" \
                                            (found ')' instead of ':')." }
            { "(A:1,(B:2,C:3):root:5;" => "Expected ',' or ')' delimiter after internal branch data \
                                           (\"root\"), found ':' instead (node 2)." }

            // One more elaborate example:
            //
            //   (branches lengths)          (nodes indexes)
            //          1
            //  ┌───────────── A            ┌────────────1 A
            // 0│         2                 │
            // ─┤    ┌──────── B           ─0    ┌───────3 B
            //  │    │       3              │    │
            //  │ 10 │    5┌── C            │    │     ┌─7 C
            //  └────┤  7┌─┤ 4              └────2   ┌─6
            //       │ ┌─┤ └── D                 │ ┌─5 └─8 D
            //       │ │ │ 6                     │ │ │
            //       │9│ └──── E                 │ │ └───9 E
            //       └─┤  8                      └─4
            //         └────── F                   └────10 F
            //
            {
                "(A:1,(B:2,(((C:3,D:4):5,E:6):7,F:8):9):10):0;" => {
                    A -> 1, B -> 3,
                    C -> 7, D -> 8,
                    E -> 9, F -> 10,
                }
                [ <0> : 0 -> (1, 2)  : 0 ]  // 0
                [ A   : 0 -> ()      : 1 ]  // 1
                [ <2> : 0 -> (3, 4)  : 10 ] // 2
                [ B   : 2 -> ()      : 2 ]  // 3
                [ <4> : 2 -> (5, 10) : 9 ]  // 4
                [ <5> : 4 -> (6, 9)  : 7 ]  // 5
                [ <6> : 5 -> (7, 8)  : 5 ]  // 6
                [ C   : 6 -> ()      : 3 ]  // 7
                [ D   : 6 -> ()      : 4 ]  // 8
                [ E   : 5 -> ()      : 6 ]  // 9
                [ F   : 4 -> ()      : 8 ]  // 10
            }

            // Same example with all nodes named + root branch length.
            //
            //  ┌─────────────1 A
            //  │
            // ─0root┌────────3 B
            //  │    │
            //  │    │     ┌──7 C
            //  └───2J   ┌6E
            //       │ ┌5G └──8 D
            //       │ │ │
            //       │ │ └────9 F
            //       └4I
            //         └─────10 H
            //
            {
                "(A:1,(B:2,(((C:3,D:4)E:5,F:6)G:7,H:8)I:9)J:10)root:11;" => {
                    A -> 1, B -> 3, C -> 7, D -> 8, E -> 6,
                    F -> 9, G -> 5, H -> 10, I -> 4, J -> 2,
                    root -> 0,
                }
                [ root : 0 -> (1, 2)  : 11 ] // 0
                [ A    : 0 -> ()      : 1 ]  // 1
                [ J    : 0 -> (3, 4)  : 10 ] // 2
                [ B    : 2 -> ()      : 2 ]  // 3
                [ I    : 2 -> (5, 10) : 9 ]  // 4
                [ G    : 4 -> (6, 9)  : 7 ]  // 5
                [ E    : 5 -> (7, 8)  : 5 ]  // 6
                [ C    : 6 -> ()      : 3 ]  // 7
                [ D    : 6 -> ()      : 4 ]  // 8
                [ F    : 5 -> ()      : 6 ]  // 9
                [ H    : 4 -> ()      : 8 ]  // 10
            }

        }
    }
}
