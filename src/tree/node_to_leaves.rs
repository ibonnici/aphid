// Iterate over paths from root to leaves.

use super::{InternalNode, Node, TerminalNode, Tree};

impl<B, IN, TN> Tree<B, IN, TN> {
    // Iterate over every path from root to leaves,
    // given `fold`-like accumulator closures.
    // Every value yielded will have been accumulated on every such path.
    // Yields (i, v): index of yielded leaf and the value accumulated from the root.
    // The first accumulating closure is called
    // on every internal node on the way down.
    // The second is called on every terminal node, just before yielding.
    // They receive parameters:
    //    (accumulator, (node_index: usize, node, first_child: Option<bool>))
    // The flag 'first_child' is raised when the current node
    // is the left child of its parent, and is undefined for the root node.
    // The accumulated value must be cloneable so that its state is saved
    // before the various sub-trees are visited.
    // TODO: not sure whether the 'first_child' feature is actually needed,
    //       remove if not and it's standing in the way for some reason.
    pub(crate) fn node_to_leaves<'t, A, T, I, V>(
        &'t self,
        i_start: usize, // 0 to start from root node.
        init: I,
        accumulator: A,
        terminator: T,
    ) -> NodeToLeaves<'t, B, IN, TN, A, T, I, V>
    where
        I: Clone,
        A: Fn(I, (usize, &'t InternalNode<B, IN>, Option<bool>)) -> I,
        T: Fn(I, (usize, &'t TerminalNode<B, TN>, Option<bool>)) -> V,
    {
        NodeToLeaves::new(self, i_start, init, accumulator, terminator)
    }

    // Start from the root.
    #[cfg(test)]
    pub(crate) fn root_to_leaves<'t, A, T, I, V>(
        &'t self,
        init: I,
        accumulator: A,
        terminator: T,
    ) -> NodeToLeaves<'t, B, IN, TN, A, T, I, V>
    where
        I: Clone,
        A: Fn(I, (usize, &'t InternalNode<B, IN>, Option<bool>)) -> I,
        T: Fn(I, (usize, &'t TerminalNode<B, TN>, Option<bool>)) -> V,
    {
        self.node_to_leaves(self.root(), init, accumulator, terminator)
    }
}

// Accumulating iterator from root to leaves.
pub(crate) struct NodeToLeaves<'n, B, IN, TN, A, T, I, V>
where
    I: Clone,
    A: Fn(I, (usize, &'n InternalNode<B, IN>, Option<bool>)) -> I,
    T: Fn(I, (usize, &'n TerminalNode<B, TN>, Option<bool>)) -> V,
{
    // Ref to original nodes.
    nodes: &'n [Node<B, IN, TN>],

    // (accumulation, i_node, next_child, first_child)
    stack: Vec<(I, usize, usize, Option<bool>)>,

    i_node: usize,             // Current focal node index.
    first_child: Option<bool>, // Raise on first child of parent node.
    next_child: usize,         // Index of the next child to visit.

    // Accumulation value and closure.
    accumulator: A,
    terminator: T,
    accumulated_value: Option<I>,
    // The above value is wrapped into an option
    // so it can be safely moved out and in when passed to the closures.
    // When the value is eventually moved out, the iterator is consumed.
}

impl<'n, B, IN, TN, A, T, I, V> NodeToLeaves<'n, B, IN, TN, A, T, I, V>
where
    I: Clone,
    A: Fn(I, (usize, &'n InternalNode<B, IN>, Option<bool>)) -> I,
    T: Fn(I, (usize, &'n TerminalNode<B, TN>, Option<bool>)) -> V,
{
    fn new(
        tree: &'n Tree<B, IN, TN>,
        i_start: usize,
        init: I,
        accumulator: A,
        terminator: T,
    ) -> Self {
        let first_child = (i_start > 0).then(|| {
            let parent = tree.nodes[i_start].parent();
            tree.is_direct_first_child(i_start, parent)
        });
        Self {
            nodes: &tree.nodes,
            i_node: i_start,
            first_child,
            next_child: 0,
            stack: Vec::new(),
            accumulator,
            terminator,
            // The iterator is born dead on degenerated empty trees.
            accumulated_value: if tree.is_empty() { None } else { Some(init) },
        }
    }
}

impl<'n, B, IN, TN, A, T, I, V> Iterator for NodeToLeaves<'n, B, IN, TN, A, T, I, V>
where
    I: Clone,
    A: Fn(I, (usize, &'n InternalNode<B, IN>, Option<bool>)) -> I,
    T: Fn(I, (usize, &'n TerminalNode<B, TN>, Option<bool>)) -> V,
{
    type Item = (usize, V);
    fn next(&mut self) -> Option<Self::Item> {
        use Node as N;
        self.accumulated_value.as_ref()?; // Iterator consumed if this is not anymore available.
        'f: loop {
            match &self.nodes[self.i_node] {
                N::Internal(node) => {
                    if self.next_child == 0 {
                        // First time encountering this internal node: update accumulator.
                        self.accumulated_value = Some((self.accumulator)(
                            // Extract owned value to pass it through the accumulator.
                            self.accumulated_value.take().unwrap(),
                            (self.i_node, node, self.first_child),
                        ));
                    }
                    // Record for further backtracking. Cloning of the accumulator happens here.
                    self.stack.push((
                        self.accumulated_value.as_ref().unwrap().clone(),
                        self.i_node,
                        self.next_child,
                        self.first_child,
                    ));
                    // Step down to focal child.
                    self.i_node = node.children[self.next_child];
                    self.first_child = Some(self.next_child == 0);
                    // Going forward, only non-visited nodes can be found.
                    self.next_child = 0;
                    continue 'f;
                }
                N::Terminal(node) => {
                    // We have found a tip. Update accumulator to this terminal node.
                    let value = (self.terminator)(
                        self.accumulated_value.take().unwrap(),
                        (self.i_node, node, self.first_child),
                    );
                    let to_yield = (self.i_node, value);
                    // Backwards iteration from the tip to the last unfinished internal node.
                    'b: while let Some((a, i, c, f)) = self.stack.pop() {
                        if c == 1 {
                            // Both children of this internal node have already been visited.
                            continue 'b;
                        }
                        // Extract this unfinished terminal node for further forward exploration.
                        self.i_node = i;
                        self.accumulated_value = Some(a); // Won't happen if the stack is empty.
                        self.next_child = c + 1;
                        self.first_child = f;
                        break;
                    }
                    return Some(to_yield);
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::tree::parse::tests::TestTree;

    #[test]
    fn paths_and_lengths() {
        use Node as N;

        // Check iteration of root-to-tips paths.
        macro_rules! test_root_to_tips {
        (:$($input:literal =>
            $(from $start_node:literal: {$($tip:ident: $path:tt $flags:tt -> $length:expr)*})*
           )*) => {$({
                let input = $input;
                let tree = TestTree::<usize>::from_input(input, &mut ()).unwrap();
                // Check paths with sequences of node indexes
                // and sequences of first_child flags.
                println!("Testing tree lengths on tree:\n  {input:?}");
                $(
                  let i_start = $start_node;
                  println!("  From node {i_start}.");
                  let mut paths = tree.node_to_leaves(i_start,
                      (Vec::new(), Vec::new()),
                      |(mut path, mut flags), (i, _, f)| {
                          path.push(i);
                          flags.push(f);
                          (path, flags)
                      },
                      |(mut path, mut flags), (i, _, f)| {
                          path.push(i);
                          flags.push(f);
                          (path, flags)
                      },
                  );
                  // Check paths with their lengths (ignoring child ordering).
                  let mut lengths = tree.node_to_leaves(i_start,
                      0,
                      |length, (_, inode, _)| length + inode.branch,
                      |length, (_, tnode, _)| (tnode.payload, length + tnode.branch),
                      );
                  $({
                     // Check full path to this leaf.
                     let name = stringify!($tip);
                     let i = tree.nodes.iter().position(|node| match node {
                             N::Terminal(tnode) => tnode.payload == name,
                             N::Internal(_) => false,
                         }).unwrap_or_else(|| panic!("Invalid node name: {name:?}."));
                     // Input 1 for left child (first=true),
                     //       0 for right child (first=false),
                     //       2=r for root.
                     let flags = $flags.map(|f| (f<2).then_some(f != 0))
                                       .into_iter().collect::<Vec<_>>();
                     assert_eq!(paths.next(), Some((i, (vec!$path, flags))),
                                "Invalid path from root to tip.");

                     // Check full length to this leaf.
                     let (j, (leaf_name, act)) = lengths.next().expect("Missing branch length.");
                     assert_eq!(i, j, "Iterators should yield leaves in the same order.");
                     assert_eq!(name, leaf_name, "Iterators should yield leaves in the same order.");
                     assert_eq!($length, act, "Invalid length from root to tip.");
                 })*
               )*
               assert_eq!(paths.next(), None, "Unexpected path.");
            })*};
        }

        let r = 2; // Convenience to mark 'None' or 'root' flags.
        test_root_to_tips! {:

            // Degenerated unit tree.
            "A:1;" => from 0: {A: [0] [r] -> 1}

            // Basic tree.
            "(A:1,B:2):0;" => from 0: {
                A: [0, 1] [r, 1] -> 1
                B: [0, 2] [r, 0] -> 2
            }
            from 1: { A: [1] [1] -> 1 }
            from 2: { B: [2] [0] -> 2 }

            // Triplet.
            "(A:1,(B:2,C:3):4):0;" => from 0: {
                A: [0, 1]    [r, 1] -> 1
                B: [0, 2, 3] [r, 0, 1] -> 6
                C: [0, 2, 4] [r, 0, 0] -> 7
            }
            from 1: { A: [1] [1] -> 1 }
            from 2: {
                B: [2, 3] [0, 1] -> 4 + 2
                C: [2, 4] [0, 0] -> 4 + 3
            }
            from 3: { B: [3] [1] -> 2 }
            from 4: { C: [4] [0] -> 3 }

            // One more elaborate example:
            //
            //    (branches lengths)          (nodes indexes)
            //
            //       │11                          │
            //   ┌───┴────┐                   ┌───0────┐
            //   │        │10                 │        │
            //   │   ┌────┴──────┐            │   ┌────2──────┐
            //   │   │           │9           │   │           │
            //   │   │       ┌───┴───┐        │   │       ┌───4───┐
            //   │   │       │7      │        │   │       │       │
            //   │1  │2    ┌─┴───┐   │        │   │     ┌─5───┐   │
            //   │   │     │     │   │        │   │     │     │   │
            //   │   │     │5    │6  │8       │   │     │     │   │
            //   │   │   ┌─┴─┐   │   │        │   │   ┌─6─┐   │   │
            //   │   │   │3  │4  │   │        1   3   7   8   9   10
            //   A   B   C   D   E   F        A   B   C   D   E   F
            //
            "(A:1,(B:2,(((C:3,D:4):5,E:6):7,F:8):9):10):11;" => from 0: {
                A: [0, 1]              [r, 1]             -> 11 + 1
                B: [0, 2, 3]           [r, 0, 1]          -> 11 + 10 + 2
                C: [0, 2, 4, 5, 6, 7]  [r, 0, 0, 1, 1, 1] -> 11 + 10 + 9 + 7 + 5 + 3
                D: [0, 2, 4, 5, 6, 8]  [r, 0, 0, 1, 1, 0] -> 11 + 10 + 9 + 7 + 5 + 4
                E: [0, 2, 4, 5, 9]     [r, 0, 0, 1, 0]    -> 11 + 10 + 9 + 7 + 6
                F: [0, 2, 4, 10]       [r, 0, 0, 0]       -> 11 + 10 + 9 + 8
            }
            from 5: {
                C: [5, 6, 7]  [1, 1, 1] -> 7 + 5 + 3
                D: [5, 6, 8]  [1, 1, 0] -> 7 + 5 + 4
                E: [5, 9]     [1, 0]    -> 7 + 6
            }
            from 6: {
                C: [6, 7]  [1, 1] -> 5 + 3
                D: [6, 8]  [1, 0] -> 5 + 4
            }
        }
    }

    #[test]
    fn empty_tree() {
        let zero = TestTree::<f64>::empty();
        assert!(zero
            .root_to_leaves(
                0.,
                |acc, (_, inode, _)| acc + inode.branch,
                |acc, (_, tnode, _)| acc + tnode.branch,
            )
            .next()
            .is_none());
    }
}
