// Construct a new tree from an existing one,
// by removing all leaves not matching the given pattern,
// removing degenerated ancestors
// and fusing associated branches together
// so that the result is still a binary tree.

use AncestorColor as A;
use Color as C;
use Node as N;

use super::{InternalNode, Node, TerminalNode, Tree};

// In the first stage of the algorithm,
// "color" the ancestor nodes from the leaves upwards.
enum Color {
    Leaf(bool), // <- True if kept.
    Ancestor(AncestorColor),
}
// Ancestors are kept if both their children lead to kept leaves.
// They are dropped if neither is kept.
// They are dropped into a "fused branch" if only one child is kept.
enum AncestorColor {
    Drop,
    Fuse(u8), // <- Index of the child to keep.
    Keep,
}

impl AncestorColor {
    fn new() -> Self {
        Self::Drop
    }
    // Update color from a kept child.
    fn add_child(&mut self, i: u8) {
        match &self {
            A::Drop => *self = A::Fuse(i),
            A::Fuse(f) => {
                if *f != i {
                    *self = A::Keep;
                }
            }
            A::Keep => {}
        }
    }
}

// TODO: this algorithm performs numerous useless implicit/explicit checks.
// Keep them for safety as long as they are not a performance bottleneck,
// but come elide them otherwise. They are marked with 'CHECK' comments.
impl<B: Default, IN: Clone, TN: Clone> Tree<B, IN, TN> {
    // In the first stage, color all nodes from the leafs upwards.
    // Return None in case all nodes have been pruned.
    fn color_nodes_before_pruning(
        &self,
        mut pattern: impl FnMut(usize, &TerminalNode<B, TN>) -> bool,
    ) -> Option<Vec<Color>> {
        // Color the leaves.
        let mut leaves_kept = Vec::new();
        let mut colors = self
            .nodes
            .iter()
            .enumerate()
            .map(|(i, node)| match node {
                N::Internal(_) => C::Ancestor(A::new()),
                N::Terminal(n) => {
                    let keep = pattern(i, n);
                    if keep {
                        leaves_kept.push(i);
                    }
                    C::Leaf(keep)
                }
            })
            .collect::<Vec<_>>();

        // Oops, has everything has been pruned?
        if leaves_kept.is_empty() {
            return None;
        }

        // For every kept leaf, climb up to color its ancestry.
        for mut i_child in leaves_kept {
            // Extract first parent.
            let leaf = &self.nodes[i_child];
            let mut i_node = leaf.parent(); // CHECK.
            let n = &self.nodes[i_node];
            let N::Internal(node) = n else { unreachable!() }; // CHECK.
            let mut node = node; // (or match ergonomics weirdly stand in the way here)
            loop {
                if i_node == i_child {
                    break;
                };
                // Find out which children we came here up from to adequately color parent.
                let C::Ancestor(color) = &mut colors[i_node] else {
                    unreachable!() // CHECK.
                };
                let i = (node.children[0] != i_child).into();
                color.add_child(i);

                // Climb up.
                i_child = i_node;
                i_node = node.parent;
                let N::Internal(n) = &self.nodes[i_node] else {
                    unreachable!()
                };
                node = n;
            }
        }

        Some(colors)
    }

    // Return false from the given closure to exclude the leaf from the resulting tree.
    // The "fuse" closure specifies how to construct the branch data
    // resulting from two merged branches.
    // The "map" closures specify how to construct node payloads from original ones,
    // and possibly receive some common mutable state to pass on.
    pub(crate) fn pruned<M>(
        &self,
        pattern: impl FnMut(usize, &TerminalNode<B, TN>) -> bool,
        fuse: impl Fn(&B, &B) -> B,
        map_state: &mut M,
        mut map_internal: impl FnMut(usize, &IN, &mut M) -> IN,
        mut map_terminal: impl FnMut(usize, &TN, &mut M) -> TN,
    ) -> Self {
        // Start with tree coloring.
        let Some(colors) = self.color_nodes_before_pruning(pattern) else {
            return Tree::empty();
        };

        // Prepare fields to fill with the new tree.
        let mut nodes = Vec::new();
        let original_nodes = &self.nodes; // Just to make it clear.

        // With the colors correctly set, scroll all source nodes to fill target nodes.
        let mut i_current = 0; // Indexes in original nodes.
        let mut i_parent = 0; // Indexes in new nodes.
        let mut i_child = 0; // Indexes into children of current node.
        let mut fused_branches = B::default();
        // Information on a stack item:
        //  - index of next unresolved parent to resolve, in original nodes.
        //  - same index, but in new nodes.
        let mut stack = Vec::new();
        loop {
            let original_node = &original_nodes[i_current];
            let color = &colors[i_current];
            match (original_node, color) {
                (N::Internal(inode), C::Ancestor(A::Keep)) => {
                    let n = nodes.len(); // Id of the node about to be copied.
                    nodes.push(N::Internal(InternalNode {
                        children: [0, 0], // Dummy, updated later.
                        payload: map_internal(i_current, &inode.payload, map_state),
                        parent: i_parent,
                        branch: fuse(&fused_branches, &inode.branch),
                    }));
                    // Consumed.
                    fused_branches = B::default();
                    // Resolve our parents child.
                    let N::Internal(parent) = &mut nodes[i_parent] else {
                        unreachable!()
                    };
                    parent.children[i_child] = n;
                    // Go check first child.
                    i_parent = n;
                    i_child = 0;
                    stack.push((i_current, n)); // Remember to go check next child later.
                    i_current = inode.children[i_child];
                    continue;
                }
                (N::Internal(inode), &C::Ancestor(A::Fuse(i))) => {
                    // Don't copy this node:
                    // go check its child directly like it's transparent.
                    fused_branches = fuse(&fused_branches, &inode.branch); // Still collect its branch length.
                    i_current = inode.children[i as usize];
                    // This avoids even coming accross nodes marked as "drop".
                    continue;
                }
                (N::Terminal(tnode), C::Leaf(true)) => {
                    let n = nodes.len(); // Id of the leaf about to be copied.
                    nodes.push(N::Terminal(TerminalNode {
                        payload: map_terminal(n, &tnode.payload, map_state),
                        parent: i_parent,
                        branch: fuse(&fused_branches, &tnode.branch),
                    }));
                    // Consumed.
                    fused_branches = B::default();
                    // Resolve our parents child
                    // (if it's not internal, then we have been pruned to a single-leaf tree).
                    let N::Internal(parent) = &mut nodes[i_parent] else {
                        break;
                    };
                    parent.children[i_child] = n;
                    // Go check last unresolved parent.
                    if let Some((o, n)) = stack.pop() {
                        i_current = o;
                        i_parent = n;
                        i_child = 1;
                    } else {
                        // Last kept leaf copied.
                        break;
                    }
                }
                // The dropped nodes should never be encountered when iterating this way,
                // because "Fuse" guards will always direct towards kept nodes.
                // CHECK.
                _ => unreachable!(),
            }
            // The following node has already been added,
            // as we are currently backtracking from an added child.
            // Go directly check next child.
            let backtracked = &original_nodes[i_current];
            let N::Internal(unresolved) = backtracked else {
                unreachable!() // CHECK.
            };
            i_current = unresolved.children[i_child];
        }

        Self { nodes }
    }
}

#[cfg(test)]
mod tests {
    use core::fmt;
    use std::{collections::HashSet, iter};

    use super::*;
    use crate::tree::parse::tests::TestTree;

    #[test]
    fn prune_trees() {
        let input = "(A:1,((B:2,(C:3,D:4):5):6,(E:7,((F:8,(G:9,H:10):11):12,I:13):14):15):16):17;";
        //
        //  Node ids:                                Branches lengths:
        //
        //       │                                        │17
        //   ┌───0────────┐                           ┌───┴────────┐
        //   │            │                           │            │16
        //   │      ┌─────2──────┐                    │      ┌─────┴──────┐
        //   │      │            │                    │      │            │15
        //   │      │        ┌───8────────┐           │      │        ┌───┴────────┐
        //   │      │        │            │           │1     │6       │            │14
        //   │      │        │      ┌─────10─┐        │      │        │      ┌─────┴──┐
        //   │   ┌──3──┐     │      │        │        │   ┌──┴──┐     │      │12      │
        //   │   │     │     │   ┌──11─┐     │        │   │     │5    │7  ┌──┴──┐     │
        //   │   │     │     │   │     │     │        │   │2    │     │   │     │11   │13
        //   │   │   ┌─5─┐   │   │   ┌─13┐   │        │   │   ┌─┴─┐   │   │8  ┌─┴─┐   │
        //   1   4   6   7   9   12  14  15  16       │   │   │3  │4  │   │   │9  │10 │
        //   A   B   C   D   E   F   G   H   I        A   B   C   D   E   F   G   H   I

        let tree = TestTree::<usize>::from_input(input, &mut ()).unwrap();

        // Expected result with fused branches.
        let keep = "B D E F G H I";
        let expected = "((B:2,D:9):6,(E:7,((F:8,(G:9,H:10):11):12,I:13):14):15):33;";
        //
        //         │                                   │17
        //         │                                   │+
        //         │                                   │16
        //    ┌────0────┐                         ┌────┴────┐
        //    │         │                         │         │15
        //    │     ┌───4────────┐                │     ┌───┴────────┐
        //    │     │            │                │6    │            │14
        //    │     │      ┌─────7──┐             │     │      ┌─────┴──┐
        //  ┌─1─┐   │      │        │           ┌─┴─┐   │      │12      │
        //  │   │   │   ┌──8──┐     │           │   │   │7  ┌──┴──┐     │
        //  │   │   │   │     │     │           │2  │5  │   │     │11   │13
        //  │   │   │   │   ┌─10┐   │           │   │+  │   │8  ┌─┴─┐   │
        //  2   3   5   9   11  12  13          │   │4  │   │   │9  │10 │
        //  B   D   E   F   G   H   I           B   D   E   F   G   H   I

        let keep = keep.split_whitespace().collect::<HashSet<_>>();
        let expected = TestTree::from_input(expected, &mut ()).unwrap();
        let actual = pruned(&tree, |_, leaf| keep.contains(leaf.payload));
        assert_eq_trees(&expected, &actual);

        // Prune more.
        let remove = "E H";
        let expected = "((B:2,D:9):6,((F:8,G:20):12,I:13):29):33;";
        //
        //         │                            │17
        //         │                            │+
        //         │                            │16
        //    ┌────0─────┐                 ┌────┴─────┐
        //    │          │                 │          │15
        //    │          │                 │          │+
        //    │          │                 │6         │14
        //    │       ┌──4──┐              │       ┌──┴──┐
        //  ┌─1─┐     │     │            ┌─┴─┐     │12   │
        //  │   │   ┌─5─┐   │            │   │   ┌─┴─┐   │
        //  │   │   │   │   │            │2  │5  │   │11 │13
        //  │   │   │   │   │            │   │+  │8  │+  │
        //  2   3   6   7   8            │   │4  │   │9  │
        //  B   D   F   G   I            B   D   F   G   I

        let remove = remove.split_whitespace().collect::<HashSet<_>>();
        let expected = TestTree::from_input(expected, &mut ()).unwrap();
        let actual = pruned(&actual, |_, leaf| !remove.contains(leaf.payload));
        assert_eq_trees(&expected, &actual);

        // Even more.
        let remove = "B D I";
        let expected = "(F:8,G:20):74;";
        //
        //    │          │17
        //    │          │+
        //    │          │16
        //    │          │+
        //    │          │15
        //    │          │+
        //    │          │14
        //    │          │+
        //    │          │12
        //  ┌─0─┐      ┌─┴─┐
        //  │   │      │   │11
        //  │   │      │8  │+
        //  1   2      │   │9
        //  F   G      F   G

        let remove = remove.split_whitespace().collect::<HashSet<_>>();
        let expected = Tree::from_input(expected, &mut ()).unwrap();
        let actual = pruned(&actual, |_, leaf| !remove.contains(leaf.payload));
        assert_eq_trees(&expected, &actual);

        // To a single-leaf degenerated tree.
        let expected = "F:82;";
        let expected = Tree::from_input(expected, &mut ()).unwrap();
        let actual = pruned(&actual, |_, leaf| leaf.payload == "F");
        assert_eq_trees(&expected, &actual);

        // To an empty tree.
        assert_eq_trees(&pruned(&actual, |_, _| false), &Tree::empty());
    }

    fn pruned<'i>(
        tree: &TestTree<'i, usize>,
        filter: impl Fn(usize, &TerminalNode<usize, &str>) -> bool,
    ) -> TestTree<'i, usize> {
        tree.pruned(
            filter,
            |a, b| a + b,
            &mut (),
            |_, &name, ()| name,
            |_, &name, ()| name,
        )
    }

    #[rustfmt::skip]
    fn assert_eq_trees<B>(exp: &TestTree<B>, act: &TestTree<B>) where B: PartialEq + fmt::Debug {
        if exp != act {
            let e = exp.nodes.iter().map(Some).chain(iter::repeat(None));
            let a = act.nodes.iter().map(Some).chain(iter::repeat(None));
            let width = 85;
            println!("{:<width$} actual", "expected");
            for (e, a) in e.zip(a) {
                if e.is_none() && a.is_none() {
                    break;
                }
                let diff = if e == a { "✔" } else { "🗙" };
                let e = if let Some(e) = e { format!("{e:?}") } else { String::new() };
                let a = if let Some(a) = a { format!("{a:?}") } else { String::new() };
                println!("{e:<width$} {diff} {a}");
            }
            panic!("Different trees found.");
        }
    }
}
