// Use this module to specify the possible output(s) of aphid.

mod config;
pub mod csv;
pub mod detail;
pub mod global;

use std::fmt::Display;

pub use global::Global;
use serde::Serializer;

// Standard output filenames.
pub mod file {
    /// The whole configuration used.
    pub const CONFIG: &str = "config.json";

    /// Forest-level analysis results.
    pub const GLOBAL: &str = "global.json";

    /// Tree-level analysis results.
    pub const DETAIL: &str = "detail.json";

    /// Tree-level results in synthetic tabular form.
    pub const CSV: &str = "trees.csv";

    /// Search traces, one per starting point.
    pub mod trace {
        /// Traces folder name.
        // TODO: optionally (default) fuse with filenames in case there is only 1.
        pub const FOLDER: &str = "search";

        /// Starting point for this trace.
        pub const INIT: &str = "init.json";

        /// BFGS-level trace.
        pub const GLOBAL: &str = "global.csv";

        /// Wolfe search trace.
        pub const DETAIL: &str = "detail.csv";

        /// Search result.
        pub const STATUS: &str = "status.json";
    }
}

// Serialize anything as its 'display' feature.
pub(crate) fn ser_display<S, D>(d: D, ser: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
    D: Display,
{
    ser.serialize_str(&d.to_string())
}
