// This small util implements two ways of
// consuming an iterator of floating values (v) to calculate its mean (m):
//
// 1. Sum then divide:
//
//      m = sum_i(v_i) / n
//
//  Simple and cheap, good for pretty much any use case.
//
//
// 2. Continuous update:
//
//      m_{i+1} = i/(i+1) * m_i + 1/(i+1) * v_i
//
//  Avoids precision loss if the sum would otherwise drift too far away from zero,
//  or if the values have very widespread magnitudes.
//  But heavier computational cost, and more imprecise floating point calculations involved.
//

use core::fmt;

use num_traits::{Float, PrimInt};

pub trait SummedMean<F: Float>: Iterator<Item = F> + Sized {
    // Type parameter specifies the type to use for accumulating the denominator.
    fn summed_mean<I: PrimInt + fmt::Debug>(mut self) -> F {
        if let Some(first) = self.next() {
            let (sum, n) = self.fold((first, I::one()), |(sum, n), value| {
                (sum + value, n + I::one())
            });
            sum / F::from(n)
                .unwrap_or_else(|| panic!("Precision loss: cannot cast {n:?} to float."))
        } else {
            F::nan()
        }
    }
}

pub trait UpdatedMean<F: Float>: Iterator<Item = F> + Sized {
    fn updated_mean(mut self) -> F {
        // Special-case first iteration to return NaN in case the iterator is empty.
        if let Some(first) = self.next() {
            self.fold((first, F::one()), |(mean, n), value| {
                let np = n + F::one();
                let inp = F::one() / np;
                (n * inp * mean + inp * value, np)
            })
            .0
        } else {
            F::nan()
        }
    }
}

impl<F: Float, I: Iterator<Item = F>> SummedMean<F> for I {}
impl<F: Float, I: Iterator<Item = F>> UpdatedMean<F> for I {}

#[cfg(test)]
mod tests {
    use float_eq::float_eq;

    use super::*;

    #[test]
    fn iterative_means() {
        macro_rules! mean_cmp {
            ($exp:literal ~ $it:expr) => {
                let exp = $exp;
                let summed = $it.summed_mean::<usize>();
                let updated = $it.updated_mean();
                for (typ, act) in [("summed", summed), ("updated", updated)] {
                    assert!(
                        float_eq!(act, exp, ulps <= 1u64),
                        "Expected {typ} mean: {exp}, got {act}."
                    );
                }
            };
        }

        // Triangular mean.
        mean_cmp!(3.5 ~ (1..=6).map(f64::from));

        // Mean of arbitrary numbers.
        mean_cmp!(2.8 ~ vec![-1., 4., -8., 9., 10.].into_iter());

        // Degenerated mean.
        mean_cmp!(12. ~ vec![12.].into_iter());

        // Empty mean (NaN).
        assert!(Vec::<f64>::new()
            .into_iter()
            .summed_mean::<usize>()
            .is_nan());
        assert!(Vec::<f64>::new().into_iter().updated_mean().is_nan());
    }
}
