// Specify gene trees, with the following assumptions:
//  - There are exactly 2 children per internal node.
//  - All branches hold 'weight' or 'length' generic data.
//  - Nodes carry different payload whether they are terminal or not.
// The trees are stored in arrays,
// and nodes point to each other with their indices within it.
// The root node is reified as the actual first node in the array
// and points to itself as its own parent.
// The root node can be a terminal node,
// in which case the (degenerated) tree contains only one node.
// Null tree is also allowed.

use crate::lexer::Error as LexerError;

// Various methods implementations moved in dedicated modules.
pub(crate) mod ancestry;
pub(crate) mod filter_node_to_leaves;
pub(crate) mod node_to_leaves;
pub(crate) mod parse;
pub(crate) mod progeny;
pub(crate) mod prune;

#[cfg_attr(test, derive(Debug, PartialEq))]
pub(crate) struct Tree<B, IN, TN> {
    // Stored in pre-order corresponding to appearance order in original Newick input.
    pub(crate) nodes: Vec<Node<B, IN, TN>>,
}

#[cfg_attr(test, derive(Debug, PartialEq))]
pub(crate) enum Node<B, IN, TN> {
    Internal(InternalNode<B, IN>),
    Terminal(TerminalNode<B, TN>),
}

#[cfg_attr(test, derive(Debug, PartialEq))]
pub(crate) struct InternalNode<B, IN> {
    pub(crate) parent: usize,
    pub(crate) children: [usize; 2], // TODO: define and use some `NodeId` alias instead.
    pub(crate) branch: B,
    payload: IN,
}

#[cfg_attr(test, derive(Debug, PartialEq))]
pub struct TerminalNode<B, TN> {
    pub(crate) parent: usize,
    pub(crate) branch: B,
    pub payload: TN,
}

impl<B, IN, TN> Tree<B, IN, TN> {
    // Construct degenerated zero-tree.
    fn empty() -> Self {
        Self { nodes: Vec::new() }
    }

    pub(crate) fn len(&self) -> usize {
        self.nodes.len()
    }

    pub(crate) fn is_empty(&self) -> bool {
        self.nodes.is_empty()
    }

    #[allow(clippy::unused_self)] // May not always be.
    pub(crate) fn root(&self) -> usize {
        0
    }

    #[allow(clippy::unused_self)] // May not always be.
    pub(crate) fn is_root(&self, i: usize) -> bool {
        i == 0
    }

    // Assuming the two given nodes ids are each other's direct parents,
    // test whether the child is the first (left) child of parent.
    pub(crate) fn is_direct_first_child(&self, child: usize, parent: usize) -> bool {
        match &self.nodes[parent] {
            Node::Internal(parent) => parent.children[0] == child,
            Node::Terminal(_) => panic!("Terminal node cannot be a parent."),
        }
    }
}

impl<B, IN, TN> Node<B, IN, TN> {
    fn parent(&self) -> usize {
        use Node as N;
        match self {
            N::Internal(n) => n.parent,
            N::Terminal(n) => n.parent,
        }
    }
    pub(crate) fn branch(&self) -> &B {
        use Node as N;
        match self {
            N::Internal(n) => &n.branch,
            N::Terminal(n) => &n.branch,
        }
    }
    pub(crate) fn children(&self) -> Option<&[usize; 2]> {
        use Node as N;
        match self {
            N::Internal(n) => Some(&n.children),
            N::Terminal(_) => None,
        }
    }
}
