// Verify raw config consistency as we construct actual config from it.
// Take this opportunity to intern all species names and replace them with their symbols.

use std::{
    cmp::Ordering,
    collections::{HashMap, HashSet},
    fmt,
    path::{Path, PathBuf},
};

use arrayvec::ArrayVec;
use regex::Regex;
use snafu::{ensure, Snafu};

use super::{raw::ListOrScalar, ParmSpec};
use crate::{
    config::{
        defaults,
        raw::{self as raw, NodeInput, ParameterInput, RecordTrace},
        Search, SpeciesTriplet, Taxa, MAX_UNRESOLVED_LENGTH,
    },
    interner::{Interner, SpeciesSymbol},
    io::{self, read_file},
    model::{
        parameters::{GeneFlowTimes, MAX_N_GF_TIMES},
        scores::Scores,
    },
    optim::{
        self,
        bfgs::{self, Config as BfgsConfig},
        gd::Config as GdConfig,
        wolfe_search::Config as WolfeConfig,
        SlopeTrackingConfig,
    },
    output::file,
    Config, Filters, Parameters,
};

macro_rules! err {
    ($fmt:tt) => {
        ConfigErr { mess: format!$fmt }
    };
}

impl Config {
    // Read raw config from file, then check it and convert into a valid config value.
    // Also return a set of output sub-folders to create.
    pub fn from_file(
        path: &Path,
        output_folder: &Path,
        interner: &mut Interner,
    ) -> Result<(Self, OutputFolders), Error> {
        // Parse raw TOML file.
        let input = read_file(path)?;
        let raw = raw::Config::parse(&input)?;

        if let Some(ul) = raw.unresolved_length {
            ensure!(
                ul >= 0.,
                err!(
                    ("The minimum internal branch length for tree considered resolved \
                      cannot be negative. Received 'unresolved_length = {ul}'.")
                )
            );
            ensure!(
                ul <= MAX_UNRESOLVED_LENGTH,
                err!(
                    ("The minimum internal branch length for tree considered resolved \
                      is assumed to be small, \
                      so the program forbids that it be superior to {MAX_UNRESOLVED_LENGTH}. \
                      Received 'unresolved_length = {ul}'.")
                )
            );
        }

        let (search, output_folders) = Search::expand_from(&raw, output_folder)?;
        let taxa = raw.taxa.expand(interner)?;

        // Resolve relative paths relatively to the config file.
        let mut trees = raw.taxa.trees;
        if !trees.is_absolute() {
            let mut path = path
                .parent()
                .unwrap_or_else(|| {
                    panic!(
                        "Config file has been read but its path has no parent: {}",
                        path.display()
                    )
                })
                .to_owned();
            path.push(trees);
            trees = io::canonicalize(&path)?;
        }

        // Most checks implemented within `TryFrom` trait.
        Ok((
            Config {
                searches: search,
                taxa,
                trees,
                unresolved_length: raw.unresolved_length,
                filters: if let Some(ref raw) = raw.filters {
                    raw.try_into()?
                } else {
                    defaults::filters()
                },
            },
            output_folders,
        ))
    }

    // The union of all sections constitutes the set of "designated" species.
    pub fn designated_species(&self) -> impl Iterator<Item = SpeciesSymbol> + '_ {
        let Taxa { triplet, outgroup, other } = &self.taxa;
        triplet
            .iter()
            .chain(outgroup.iter().copied())
            .chain(other.iter().copied())
    }
}

type OutputFolders = Vec<PathBuf>;

//--------------------------------------------------------------------------------------------------
// Check filter parameters.
impl TryFrom<&raw::Filters> for Filters {
    type Error = Error;

    fn try_from(raw: &raw::Filters) -> Result<Self, Self::Error> {
        if let Some(mcr) = raw.max_clock_ratio {
            ensure!(
                mcr >= 1.,
                err!(
                    ("The maximum branch length ratio between triplet and outgroup \
                      cannot be lower than 1. Received 'max_clock_ratio = {mcr}'.")
                )
            );
        }
        Ok(Filters {
            max_clock_ratio: raw.max_clock_ratio,
            triplet_other_monophyly: raw
                .triplet_other_monophyly
                .unwrap_or(defaults::filters().triplet_other_monophyly),
        })
    }
}

//--------------------------------------------------------------------------------------------------
// Check search configuration.

impl Search {
    fn expand_from(
        raw: &raw::Config,
        output_folder: &Path,
    ) -> Result<(Vec<Self>, OutputFolders), Error> {
        let inits = try_from_parameters(&raw.init, &raw.gf_times)?;

        // Prepare paths to traces if required.
        let n = inits.len();
        let nd = n.checked_ilog10().unwrap_or(0) as usize + 1; // Number of digits for folder names.
        let mut res = Vec::new();
        let mut output_folders = Vec::new();
        let base = output_folder.join(file::trace::FOLDER);
        for (init_parms, i) in inits.into_iter().zip(1..) {
            let id = format!("{i:0nd$}");
            let folder = base.join(&id);
            let bfgs = BfgsConfig::try_from(&raw.search.bfgs, raw.gf_times.len(), id, &folder)?;
            let search = Search { init_parms, bfgs };
            res.push(search);
            output_folders.push(folder);
        }

        Ok((res, output_folders))
    }
}

impl TryFrom<&'_ raw::GdConfig> for GdConfig {
    type Error = Error;
    fn try_from(raw: &raw::GdConfig) -> Result<Self, Self::Error> {
        let &raw::GdConfig { max_iter, learning_rate, ref slope_tracking } = raw;
        Ok(Self {
            max_iter,
            step_size: learning_rate,
            slope_tracking: slope_tracking
                .as_ref()
                .map(|sl| SlopeTrackingConfig::new(sl.sample_size, sl.threshold, sl.grain))
                .transpose()?,
        })
    }
}

impl BfgsConfig {
    fn try_from(
        raw: &raw::BfgsConfig,
        n_gf_times: usize,
        id: String,
        output_folder: &Path,
    ) -> Result<Self, Error> {
        let &raw::BfgsConfig {
            max_iter,
            ref wolfe,
            step_size_threshold,
            ref slope_tracking,
            ref record_trace,
        } = raw;
        ensure!(
            0. <= step_size_threshold,
            err!(
                ("The threshold for defining small search steps \
                  must be null or positive. Received: {step_size_threshold}.")
            )
        );
        let (log, main, linsearch) = match record_trace {
            RecordTrace::No => (false, None, None),
            RecordTrace::Global => (true, Some(output_folder.join(file::trace::GLOBAL)), None),
            RecordTrace::Detail => (
                true,
                Some(output_folder.join(file::trace::GLOBAL)),
                Some(output_folder.join(file::trace::DETAIL)),
            ),
        };
        Ok(Self {
            max_iter,
            slope_tracking: slope_tracking
                .as_ref()
                .map(|sl| SlopeTrackingConfig::new(sl.sample_size, sl.threshold, sl.grain))
                .transpose()?,
            wolfe: wolfe.try_into()?,
            small_step: step_size_threshold,
            log: log.then(|| {
                bfgs::Log {
                    folder: output_folder.to_path_buf(),
                    id,
                    main,
                    linsearch,
                    // Unexposed: the meaning of variables is bound to the program internals.
                    variable_names: Some(
                        Scores::<()>::names_from_n(n_gf_times)
                            .map(Into::into)
                            .collect(),
                    ),
                }
            }),
        })
    }
}

impl TryFrom<&raw::WolfeSearchConfig> for WolfeConfig {
    type Error = Error;
    fn try_from(raw: &raw::WolfeSearchConfig) -> Result<Self, Self::Error> {
        let &raw::WolfeSearchConfig {
            c1,
            c2,
            init_step_size,
            step_decrease,
            step_increase,
            flat_gradient,
            bisection_threshold,
        } = raw;
        for (c, which, name) in [(c1, "upper", "c1"), (c2, "lower", "c2")] {
            ensure!(
                0. < c && c < 1.,
                err!(
                    ("The Wolfe constant for checking {which} bound \
                      must stand between 0 and 1 (excluded). \
                      Received {name}={c1}.")
                )
            );
        }
        ensure!(
            c1 < c2,
            err!(
                ("The Wolfe constant c1 and c2 must verify c1 < c2. \
                  Received: c1 = {c1} ⩾ {c2} = c2.")
            )
        );
        ensure!(
            init_step_size > 0.,
            err!(
                ("Initial step size for Wolfe search must be positive. \
                  Received {init_step_size}.")
            )
        );
        ensure!(
            0. < step_decrease && step_decrease < 1.,
            err!(
                ("Decrease factor for Wolfe search must stand between 0 and 1 (excluded). \
                  Received: {step_decrease}.")
            )
        );
        ensure!(
            1. < step_increase,
            err!(
                ("Increase factor for Wolfe search must stand above 1. \
                  Received: {step_increase}.")
            )
        );
        ensure!(
            0. <= flat_gradient,
            err!(
                ("Flat gradient threshold for Wolfe search must be positive or null. \
                  Received: {flat_gradient}.")
            )
        );
        ensure!(
            (0. ..=0.5).contains(&bisection_threshold),
            err!(
                ("The zoom bisection threshold for Wolfe search must stand between 0 and 0.5. \
                  Received: {bisection_threshold}.")
            )
        );
        Ok(Self {
            c1,
            c2,
            init_step: init_step_size,
            step_decrease,
            step_increase,
            flat_gradient,
            bisection_threshold,
        })
    }
}

//--------------------------------------------------------------------------------------------------
// Check initial parameters.

#[allow(clippy::too_many_lines)] // Lot of checking, difficult to disentangle.
#[allow(clippy::similar_names)] // (p_ab, p_bc, p_ac)
fn try_from_parameters(
    init: &raw::Init,
    raw_gft: &raw::GeneFlowTimes,
) -> Result<Vec<Parameters<ParmSpec>>, Error> {
    use ListOrScalar as LS;
    use ParameterInput as PI;

    // Main input: parameter name, index and value if set.
    type In = (&'static str, Option<(Index, PI<f64>)>);
    // Main output: name, index, value and fixed flag.
    type Out = (Option<(Index, f64)>, bool);

    // Scroll the whole list of user-set parameters from left to right,
    // checking every 'vertical slice' of it as a separate set of parameters.
    // The parameters not set become 'None' for all slices.
    // The parameters set to scalars are replicated among all slices.
    // The parameters set to arrays are checked for being all the same size.

    // Produce a values generator based on the three possible options
    // and the number of values to be yielded for arrays
    fn to_generator<'r>(
        name: &'static str,
        ls: Option<&'r LS<PI<f64>>>,
    ) -> (Option<usize>, Box<dyn FnMut() -> In + 'r>) {
        match ls {
            None => (None, Box::new(move || (name, None))),
            Some(LS::Scalar(value)) => (
                None,
                Box::new(move || (name, Some((Index { i: None, auto: false }, value.clone())))),
            ),
            Some(LS::List(vec)) => {
                let mut it = vec
                    .iter()
                    .cloned()
                    .enumerate()
                    .map(move |(i, v)| (Index { i: Some(i), auto: false }, v));
                (Some(vec.len()), Box::new(move || (name, it.next())))
            }
        }
    }

    let raw::Init {
        theta,
        tau_1,
        tau_2,
        p_ab,
        p_ac,
        p_bc,
        p_ancient_gf,
    } = init;

    // Call for each parameter,
    // collecting the number of values to be yielded for each.
    let mut n_values = HashMap::new();
    let mut collect = |ls, name| {
        let (n, mut res) = to_generator(name, ls);
        if let Some(n) = n {
            n_values.insert(n, name);
        }
        move || res()
    };

    let mut theta = collect(Some(theta), "theta");
    let mut tau_1 = collect(tau_1.as_ref(), "tau_1");
    let mut tau_2 = collect(tau_2.as_ref(), "tau_2");
    let mut p_ab = collect(p_ab.as_ref(), "p_ab");
    let mut p_ac = collect(p_ac.as_ref(), "p_ac");
    let mut p_bc = collect(p_bc.as_ref(), "p_bc");
    let mut p_ancient_gf = collect(p_ancient_gf.as_ref(), "p_ancient_gf");

    // Figure the total number of starting points.
    let n_points = match n_values.len() {
        0 => 1,
        1 => n_values.into_iter().next().unwrap().0,
        _ => {
            let mut it = n_values.into_iter();
            let (a, a_name) = it.next().unwrap();
            let (b, b_name) = it.next().unwrap();
            let s = |n| if n > 1 { "s" } else { "" };
            return ConfigErr {
                mess: format!(
                    "Inconsistent numbers of starting point parameters: \
                     {a} value{} provided for {a_name} but {b} value{} for {b_name}.",
                    s(a),
                    s(b)
                ),
            }
            .fail()?;
        }
    };

    // And collect this many "vertically", checking every slice for parameters consistency.
    let mut res = Vec::new();

    // Special checking for gene flow times (+ always fixed for now).
    let gft: GeneFlowTimes<f64> = raw_gft.try_into()?;
    let gft = GeneFlowTimes(
        gft.0
            .into_iter()
            .enumerate()
            .map(|(i, t)| (Some((Index { i: Some(i), auto: false }, t)), true))
            .collect(),
    );

    for _ in 0..n_points {
        // Keep parameter name around just long enough to emit errors if needed.
        type ROut = Result<(&'static str, Out), Error>;

        let parse_annotation = |input: In| -> ROut {
            let (name, parm) = input;
            Ok(if let Some((index, parm)) = parm {
                let fixed = parm
                    .is_fixed()
                    .map_err(|mess| ParameterValueErr { name, index, mess }.build())?;
                (name, (parm.value().map(|v| (index, *v)), fixed))
            } else {
                (name, (None, false))
            })
        };

        let check_positive = |input: In| -> ROut {
            let (name, (v, fixed)) = parse_annotation(input)?;
            if let Some((index, v)) = v {
                ensure!(
                    v > 0.,
                    ParameterValueErr {
                        name,
                        index,
                        mess: format!("must be positive, not {v}")
                    }
                );
            }
            Ok((name, (v, fixed)))
        };

        let check_probability = |input| -> ROut {
            let (name, (v, fixed)) = parse_annotation(input)?;
            if let Some((index, v)) = v {
                ensure!(
                    (0. ..=1.).contains(&v),
                    ParameterValueErr {
                        name,
                        index,
                        mess: format!("must be a probability (between 0 and 1), not {v}.")
                    }
                );
            }
            Ok((name, (v, fixed)))
        };

        // Strip parameter name.
        let strip = |rout: ROut| -> Result<(Option<(Index, f64)>, bool), Error> {
            let (out, fixed) = rout?.1;
            Ok((out, fixed))
        };
        let positive = |i| strip(check_positive(i));
        let probability = |i| strip(check_probability(i));

        // Individual value test (context-less).
        let p = Parameters {
            theta: positive(theta())?,
            tau_1: positive(tau_1())?,
            tau_2: positive(tau_2())?,
            p_ab: probability(p_ab())?,
            p_ac: probability(p_ac())?,
            p_bc: probability(p_bc())?,
            p_ancient: probability(p_ancient_gf())?,
            gf_times: gft.clone(),
        };

        // Transversal checks.
        tau_values(&p.tau_1.0, &p.tau_2.0)?;
        probability_sums([&p.p_ab.0, &p.p_ac.0, &p.p_bc.0])?;

        // All checks passed: this parameters slice is correct.
        res.push(p.map(|&(val, fixed)| ParmSpec {
            value: val.as_ref().map(|(_, v)| *v),
            fixed,
            index: val.and_then(|(i, _)| i.i),
        }));
    }

    Ok(res)
}

// (useful for the above).
impl<T> ParameterInput<T> {
    // Annotation aliases.
    pub(crate) fn is_fixed(&self) -> Result<bool, String> {
        let fix = ["fix", "fixed", "force", "forced", "pin", "pinned"];
        let learn = ["learn", "learnt", "learned", "opt", "optimize", "optimized"];
        match self.annotation() {
            None => Ok(false),
            Some(annot) => {
                if fix.contains(&annot) {
                    Ok(true)
                } else if learn.contains(&annot) {
                    Ok(false)
                } else {
                    Err(format!(
                        "unexpected parameter annotation: {annot:?}.\n\
                         For fixed parameters, use one of {fix:?}\n\
                         For learned parameters, one of   {learn:?}"
                    ))
                }
            }
        }
    }

    pub(crate) fn value(&self) -> Option<&T> {
        use ParameterInput as I;
        match self {
            I::Annotated(value, _) | I::OnlyValue(value) => Some(value),
            I::NoValue(_) => None,
        }
    }

    pub(crate) fn annotation(&self) -> Option<&str> {
        use ParameterInput as I;
        match self {
            I::Annotated(_, annot) | I::NoValue(annot) => Some(annot),
            I::OnlyValue(_) => None,
        }
    }
}

// Abstract away for reuse when checking mixtures of user-input + default parameters.
pub fn tau_values(tau_1: &Option<(Index, f64)>, tau_2: &Option<(Index, f64)>) -> Result<(), Error> {
    use Ordering as O;
    if let (Some((i1, tau_1)), Some((i2, tau_2))) = (tau_1, tau_2) {
        match tau_2.total_cmp(tau_1) {
            O::Less => {
                return err!(
                    ("the second coalescence time must be older than the first: \
                      here tau_1{i1} > tau_2{i2}: {tau_1} > {tau_2}.",)
                )
                .fail()
            }
            O::Equal => {
                return err!(("the two coalescence times cannot be identical: \
                              here tau_1{i1} == tau_2{i2} == {tau_1}.",))
                .fail()
            }
            O::Greater => {}
        };
    }
    Ok(())
}

// Combinations check (not scalable with the number of p_*).
#[allow(clippy::similar_names)]
pub fn probability_sums(probs: [&Option<(Index, f64)>; 3]) -> Result<(), Error> {
    let s: f64 = probs
        .iter()
        .filter_map(|p| p.as_ref().map(|(_, p)| p))
        .sum();
    if s > 1.0 {
        let names = ["p_ab", "p_ac", "p_ac"];
        let terms = names
            .iter()
            .zip(probs.iter())
            .filter_map(|(name, p)| p.as_ref().map(|(index, _)| (name, index)))
            .map(|(name, index)| format!("{name}{index}"))
            .collect::<Vec<_>>()
            .join(" + ");
        let vterms = probs
            .iter()
            .filter_map(|p| p.as_ref().map(|(_, p)| p.to_string()))
            .collect::<Vec<_>>()
            .join(" + ");
        err!(
            ("The sum of gene flow transfer probabilities must not be larger than 1. \
              Here {terms} = {vterms} = {s} > 1.")
        )
        .fail()?;
    }
    Ok(())
}

//--------------------------------------------------------------------------------------------------
// Check gene flow times.

impl TryFrom<&raw::GeneFlowTimes> for GeneFlowTimes<f64> {
    type Error = Error;
    fn try_from(raw: &raw::GeneFlowTimes) -> Result<Self, Self::Error> {
        if raw.is_empty() {
            err!(("No gene flow time specified."));
        }

        if raw.len() > MAX_N_GF_TIMES {
            err!((
                "Aphid does not support more than {MAX_N_GF_TIMES:?} gene flow times, \
                 but {} times were provided.",
                raw.len()
            ));
        }

        // Check values while sorting them.
        let mut gf_times = Self::new();
        for &time in raw {
            if time.is_infinite() || time.is_nan() || time < 0. {
                err!(("Invalid gene flow time: {time}."));
            }
            if time > 1.0 {
                err!(
                    ("Gene flow time cannot yet be specified earlier \
                      than latest species divergence, but {time} is greater than 1.")
                );
            }
            match gf_times
                .0
                .binary_search_by(|&t| t.total_cmp(&time).reverse())
            {
                Ok(_) => return err!(("Gene flow time '{time}' specified twice.")).fail(),
                Err(i) => gf_times.0.insert(i, time),
            }
        }
        Ok(gf_times)
    }
}

//--------------------------------------------------------------------------------------------------
impl raw::Taxa {
    fn expand(&self, interner: &mut Interner) -> Result<Taxa, Error> {
        // Check taxa consistency.
        let Self { triplet: raw_triplet, outgroup, other, .. } = self;
        let triplet: [&str; 3] = raw_triplet
            .try_into()
            .map_err(|mess| Error::Config { mess })?;
        if outgroup.is_empty() {
            err!(("No outgroup species specified."));
        }

        // No duplicates should be found among the taxa explicitly given.
        let mut designated_species = HashSet::new();
        let mut new_species = move |name, section| -> Result<_, Error> {
            ensure!(
                !designated_species.contains(&name),
                err!(
                    ("Species {name:?} found in '{section}' section \
                      is given twice within the [taxa] table.")
                )
            );
            let symbol = interner.get_or_intern(name);
            designated_species.insert(name);
            // Also record into the interner.
            Ok(symbol)
        };

        let [a, b, c] = triplet.map(|s| new_species(s, "triplet"));
        Ok(Taxa {
            triplet: SpeciesTriplet { a: a?, b: b?, c: c? },
            outgroup: outgroup
                .iter()
                .map(|s| new_species(s, "outgroup"))
                .collect::<Result<Vec<_>, _>>()?,
            other: other
                .iter()
                .map(|s| new_species(s, "other"))
                .collect::<Result<Vec<_>, _>>()?,
        })
    }
}

//--------------------------------------------------------------------------------------------------
// Check triplet and extract strings in canonical order.

impl<'t> TryFrom<&'t raw::Triplet> for [&'t str; 3] {
    type Error = String;

    // Work towards canonicalized ((a, b), c),
    // (x, y) refer to input nodes still undetermined.
    #[allow(clippy::many_single_char_names)]
    fn try_from(input: &'t raw::Triplet) -> Result<Self, Self::Error> {
        use raw::Triplet as T;
        use NodeInput as N;
        macro_rules! err {
            ($($message:tt)+) => {{
                return Err(format!$($message)+);
            }};
        }
        match input {
            T::Arrays(ars) => {
                let original = format!("{ars:?}");
                // Special-case one useful error.
                if ars.len() == 3 {
                    let mut species = ArrayVec::<&str, 3>::new();
                    for i in ars {
                        match i {
                            N::Leaf(sp) => species.push(sp),
                            N::Internal(_) => break,
                        }
                    }
                    if species.len() == 3 {
                        let [a, b, c] = species.into_inner().unwrap();
                        err!(
                            ("Ambiguous triplet:\n  {original}\nSpecify either\n  \
                              [[{a:?}, {b:?}], {c:?}]\nor\n   [{a:?}, [{b:?}, {c:?}]]")
                        );
                    }
                }
                // Fallback error for other invalid input types.
                macro_rules! invalid { () => {{
                    err!(
                        ("Invalid triplet specification. Expected 3 nested nodes under the form \
                         [[\"A\", \"B\"], \"C\"] or [\"C\", [\"A\", \"B\"]], got {original}.")
                    );
                }};}
                if ars.len() != 2 {
                    invalid!()
                }
                let mut it = ars.iter();
                let (x, y) = (it.next().unwrap(), it.next().unwrap());
                let ((N::Internal(ab), N::Leaf(c)) | (N::Leaf(c), N::Internal(ab))) = (x, y) else {
                    invalid!()
                };
                if ab.len() != 2 {
                    invalid!()
                }
                let mut it = ab.iter();
                let (a, b) = (it.next().unwrap(), it.next().unwrap());
                Ok([a, b, c])
            }
            T::Parenthesized(input) => {
                // A small tree with 3 expected nodes can be just regular-parsed.
                // Capture groups for trimmed leaf nodes.
                let specials = ",()"; // Forbidden in species names then.
                let mut it = "abc"
                    .chars()
                    .map(|x| format!(r"\s*(?<{x}>[^{specials}]+?)\s*"));
                let (a, b, c) = (it.next().unwrap(), it.next().unwrap(), it.next().unwrap());
                // Match internal node.
                let ab = format!(r"\s*\({a},{b}\)\s*");
                // There are two possible forms.
                let [alt1, alt2] = [format!("{ab},{c}"), format!("{c},{ab}")]
                    .map(|p| Regex::new(&format!(r"\s*\({p}\)\s*")).unwrap());
                // Match either for the whole triplet.
                let caps = if let Some(caps) = alt1.captures(input) {
                    caps
                } else if let Some(caps) = alt2.captures(input) {
                    caps
                } else {
                    err!(
                        ("Invalid triplet specification. Expected 3 nested nodes under the form \
                         \"((A, B), C)\" or \"(C, (A, B))\", got {input:?}.")
                    )
                };
                let extr = |x| caps.name(x).unwrap().as_str();
                Ok([extr("a"), extr("b"), extr("c")])
            }
        }
    }
}

impl SpeciesTriplet {
    pub fn as_array(&self) -> [SpeciesSymbol; 3] {
        [self.a, self.b, self.c]
    }

    pub fn iter(&self) -> impl Iterator<Item = SpeciesSymbol> {
        self.as_array().into_iter()
    }
}

//==================================================================================================
// Errors.

#[derive(Debug, Snafu)]
#[snafu(context(suffix(Err)))]
pub enum Error {
    #[snafu(transparent)]
    Io { source: io::Error },
    #[snafu(transparent)]
    Parse { source: raw::Error },
    #[snafu(display("Configuration: {mess}"))]
    Config { mess: String },
    #[snafu(transparent)]
    Optim { source: optim::Error },
    #[snafu(display("Could not find folder: {:?}", path.display()))]
    NoSuchFolder { path: PathBuf },
    #[snafu(display("In model parameter {name}{index}: {mess}"))]
    ParameterValue {
        name: String,
        index: Index,
        mess: String,
    },
}

// Referring to input parameter.
#[derive(Debug, Clone, Copy)]
pub struct Index {
    i: Option<usize>, // Lower on scalar user input (implicit table).
    auto: bool,       // Raise if calculated by aphid.
}

impl Index {
    pub fn auto(i: Option<usize>) -> Self {
        Self { i, auto: true }
    }
}

impl fmt::Display for Index {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if let Some(i) = self.i {
            write!(f, "[{}]", i + 1) // Report to humans, humans count from 1.
        } else if self.auto {
            write!(f, "[<auto>]")
        } else {
            Ok(())
        }
    }
}

//==================================================================================================
// Test.

#[cfg(test)]
mod tests {
    use serde::Deserialize;

    use super::*;

    #[test]
    #[allow(clippy::too_many_lines)]
    fn triplet_parsing() {
        #[derive(Deserialize)]
        struct Single {
            raw: raw::Triplet,
        }
        let success = |input, expected| {
            println!("Checking success for: {input}.");
            let raw: Single = toml::from_str(&format!("raw = {input}")).unwrap();
            let actual: [&str; 3] = (&raw.raw).try_into().unwrap();
            assert_eq!(expected, actual);
        };
        success(r#""((U, V), W)""#, ["U", "V", "W"]);
        success(r#""(U, (V, W))""#, ["V", "W", "U"]);
        success(
            r#"" ( ( with space , pretty much ) , everywhere ) ""#,
            ["with space", "pretty much", "everywhere"],
        );
        success(r#"[["U", "V"], "W"]"#, ["U", "V", "W"]);
        success(r#"["U", ["V", "W"]]"#, ["V", "W", "U"]);

        let failure = |input, expected: String| {
            println!("Checking failure for: {input}.");
            let raw = match toml::from_str::<Single>(&format!("raw = {input}")) {
                Ok(r) => r,
                Err(e) => {
                    let actual = format!("{e}");
                    assert_eq!(expected, actual, "Invalid error message.");
                    return;
                }
            };
            let res: [&str; 3] = match (&raw.raw).try_into() {
                Ok(r) => r,
                Err(e) => {
                    let actual = e.to_string();
                    assert_eq!(expected, actual, "Invalid error message.");
                    return;
                }
            };
            panic!("Unexpected success with\n  {input:?}\nObtained:\n  {res:?}");
        };
        failure(
            r#""(U, V, W)""#,
            concat!(
                r#"Invalid triplet specification. Expected 3 nested nodes under the form "#,
                r#""((A, B), C)" or "(C, (A, B))", got "(U, V, W)"."#
            )
            .into(),
        );
        failure(
            r#""(U, V, W, X)""#,
            concat!(
                r#"Invalid triplet specification. Expected 3 nested nodes under the form "#,
                r#""((A, B), C)" or "(C, (A, B))", got "(U, V, W, X)"."#,
            )
            .into(),
        );
        failure(
            r#""((U, V, W), X)""#,
            concat!(
                r#"Invalid triplet specification. Expected 3 nested nodes under the form "#,
                r#""((A, B), C)" or "(C, (A, B))", got "((U, V, W), X)"."#,
            )
            .into(),
        );
        failure(
            r#""((U, V), W, X)""#,
            concat!(
                r#"Invalid triplet specification. Expected 3 nested nodes under the form "#,
                r#""((A, B), C)" or "(C, (A, B))", got "((U, V), W, X)"."#,
            )
            .into(),
        );
        failure(
            r#""((U, V), W, X)""#,
            concat!(
                r#"Invalid triplet specification. Expected 3 nested nodes under the form "#,
                r#""((A, B), C)" or "(C, (A, B))", got "((U, V), W, X)"."#,
            )
            .into(),
        );
        // Special-cased error message.
        failure(
            r#"["U", "V", "W"]"#,
            concat!(
                r#"Ambiguous triplet:\n  ["U", "V", "W"]\n"#,
                r#"Specify either\n  [["U", "V"], "W"]\n"#,
                r#"or\n   ["U", ["V", "W"]]"#,
            )
            .replace(r"\n", "\n"),
        );
        failure(
            r#"["U", "V", "W", "X"]"#,
            concat!(
                r#"Invalid triplet specification. "#,
                r#"Expected 3 nested nodes under the form "#,
                r#"[["A", "B"], "C"] or ["C", ["A", "B"]], "#,
                r#"got ["U", "V", "W", "X"]."#
            )
            .into(),
        );
        failure(
            r#"[["U", "V"], "W", "X"]"#,
            concat!(
                r#"Invalid triplet specification. "#,
                r#"Expected 3 nested nodes under the form "#,
                r#"[["A", "B"], "C"] or ["C", ["A", "B"]], "#,
                r#"got [["U", "V"], "W", "X"]."#
            )
            .into(),
        );
        failure(
            r#"[["U", "V"], ["W", "X"]]"#,
            concat!(
                r#"Invalid triplet specification. "#,
                r#"Expected 3 nested nodes under the form "#,
                r#"[["A", "B"], "C"] or ["C", ["A", "B"]], "#,
                r#"got [["U", "V"], ["W", "X"]]."#
            )
            .into(),
        );
        failure(
            r"[]",
            concat!(
                r#"Invalid triplet specification. "#,
                r#"Expected 3 nested nodes under the form "#,
                r#"[["A", "B"], "C"] or ["C", ["A", "B"]], "#,
                r#"got []."#
            )
            .into(),
        );
        // Not a super error.. but go with it for now.
        failure(
            r#"[["U", ["V", "W"]]]"#,
            concat!(
                r#"TOML parse error at line 1, column 7\n"#,
                r#"  |\n"#,
                r#"1 | raw = [["U", ["V", "W"]]]\n"#,
                r#"  |       ^^^^^^^^^^^^^^^^^^^\n"#,
                r#"data did not match any variant of untagged enum Triplet\n"#,
            )
            .replace(r"\n", "\n"),
        );
    }
}
