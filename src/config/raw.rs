// Deserialize raw config from file.
// This type is a faithful representation
// what the user wrote in the config file,
// and it then needs to be checked
// into the eventual config type handed to user.
//
// Doctrings here are retrieved to build the final documentation.
// It seems that types need to be public to this end.

use std::{array, fmt, path::PathBuf};

use serde::Deserialize;
use serde_with::{serde_as, FromInto};
use snafu::{ResultExt, Snafu};

use crate::config::defaults;

//--------------------------------------------------------------------------------------------------
// Global config structure and defaults.

pub type GeneFlowTimes = Vec<f64>;

#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Config {
    pub taxa: Taxa,

    /// List the relative dates for possible gene flow events.
    /// At least one event must be specified,
    /// but no more than [`model::parameters::MAX_N_GF_TIMES`].
    /// Dates are relative to the divergence times of the `triplet` species.
    #[serde(default = "defaults::gf_times")]
    pub gf_times: GeneFlowTimes,

    /// If set, when the internal triplet branch length
    /// is inferior or equal to this value,
    /// consider that the topology is not resolved enough
    /// to exclude discordant scenarios.
    /// In this situation,
    /// every scenario contributes to the likelihood
    /// instead of only the ones with a concordant topology.
    /// The possible internal branch discordance
    /// between actual and expected length
    /// is neglected because the the actual length is small.
    /// For this reason, only values inferior
    /// to [`config::MAX_UNRESOLVED_LENGTH`] are accepted.
    /// The value is given in *mutation* units, so branch length × sequence length.
    pub unresolved_length: Option<f64>,

    /// These additional, optional configuration parameters
    /// control whether input trees are dismissed or kept for analysis.
    pub filters: Option<Filters>,

    #[serde(default = "defaults::init")]
    pub init: Init,

    /// This optional table can be set for additional control
    /// over the likelihood exploration.
    #[serde(default = "defaults::search")]
    pub search: Search,
}

impl Config {
    // Parse config file successfully
    // or abort the program with a useful error.
    pub(crate) fn parse(input: &str) -> Result<Self, Error> {
        toml::from_str(input).context(DeserializeErr {})
    }
}

//--------------------------------------------------------------------------------------------------
// Filters configuration.

#[serde_as]
#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Filters {
    /// If set, this parameter rejects trees whose imbalance is too strong
    /// with respect to their enclosing forest.
    /// Values need to be greater than 1
    /// because they quantify an "absolute" imbalance threshold: `max(q/Q, Q/q)`.
    pub max_clock_ratio: Option<f64>,

    /// When raised, filter out trees
    /// with at least one species from the 'other' section
    /// branching between LCA(outgroup) and the root,
    /// because it possibly should have been considered 'outgroup' instead.
    pub triplet_other_monophyly: Option<bool>,
}

//--------------------------------------------------------------------------------------------------
// Species data.

/// The major source of input to `aphid`
/// is specified under the `[taxa]` table.
#[serde_as]
#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Taxa {
    /// This path is where you specify the (possibly large) input file
    /// containing all gene trees for the species of interest.
    /// Every line in this file must have the following format:
    /// ```plain
    /// <tree> <TAB> <sequence_length> <TAB> <identifier>
    /// ```
    /// The `<tree>` part being a [Newick] representation of the gene tree,
    /// with branches lengths specified.
    ///
    /// [Newick]: https://en.wikipedia.org/wiki/Newick_format
    ///
    /// The path is either absolute or relative to the configuration file.
    pub trees: PathBuf,

    /// This parameter is where you specify
    /// the three species of interest and their phylogenetical topology.
    /// Either forms `["A", ["B", "C"]]` or `"(A, (B, C))"` are accepted,
    /// provided `A`, `B` and `C` match names within the provided `taxa.trees`.
    pub triplet: Triplet,

    /// Species listed in this parameter
    /// are used as the outgroup for the focal triplet.
    /// Either forms `["O", "P", "Q"]` or whitespace-separated `"O P Q"` are accepted,
    /// provided they are distinct from `taxa.triplet`.
    #[serde_as(as = "FromInto<ListOfStrings>")]
    pub outgroup: Vec<String>,

    /// Species listed in this parameter
    /// are used as extra leaves in the tree
    /// to estimate branch lengths properties.
    #[serde_as(as = "FromInto<ListOfStrings>")]
    pub other: Vec<String>,
}

// Guard against invalid inputs.
#[derive(Deserialize)]
#[serde(untagged)]
pub enum Triplet {
    Arrays(Vec<NodeInput>),
    Parenthesized(String),
}

#[derive(Deserialize)]
#[serde(untagged)]
pub enum NodeInput {
    Leaf(String),
    Internal(Vec<String>),
}

impl fmt::Debug for Triplet {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use Triplet as T;
        match self {
            T::Arrays(ars) => write!(f, "{ars:?}"),
            T::Parenthesized(s) => write!(f, "{s:?}"),
        }
    }
}

impl fmt::Debug for NodeInput {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use NodeInput as N;
        match self {
            N::Leaf(s) => write!(f, "{s:?}"),
            N::Internal(vec) => write!(f, "{vec:?}"),
        }
    }
}

//--------------------------------------------------------------------------------------------------
// Statistical model parameters.

#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Init {
    // Only the first field is necessary.
    #[serde(default = "defaults::init_thetas")]
    pub theta: LoS,

    // Others are optional.
    pub tau_1: OLoS,
    pub tau_2: OLoS,
    pub p_ab: OLoS,
    pub p_ac: OLoS,
    pub p_bc: OLoS,
    pub p_ancient_gf: OLoS,
}

type LoS = ListOrScalar<ParameterInput<f64>>;
type OLoS = Option<ListOrScalar<ParameterInput<f64>>>;

// Parameter are either input as:
//    0.5           => Start from 0.5 and learn the optimal value.
//    [0.5, "fix"]  => Start from 0.5 and force-fix this value.
//    "learn"       => Start from aphid's heuristic and learn.
//    "fix"         => Start from aphid's heuristic and fix.
#[derive(Deserialize, Clone)]
#[serde(untagged)]
pub enum ParameterInput<T> {
    Annotated(T, String),
    OnlyValue(T),
    NoValue(String),
}

//--------------------------------------------------------------------------------------------------
// Search configuration.

#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Search {
    #[serde(default)]
    pub bfgs: BfgsConfig,
}

#[derive(Deserialize)]
#[serde(deny_unknown_fields, default)]
pub struct BfgsConfig {
    pub max_iter: u64,
    pub wolfe: WolfeSearchConfig,
    pub step_size_threshold: f64,
    pub slope_tracking: Option<SlopeTrackingConfig>,
    pub record_trace: RecordTrace,
}

#[derive(Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum RecordTrace {
    No,
    Global,
    Detail,
}

#[derive(Deserialize)]
#[serde(deny_unknown_fields, default)]
pub struct WolfeSearchConfig {
    pub c1: f64,
    pub c2: f64,
    pub init_step_size: f64,
    pub step_decrease: f64,
    pub step_increase: f64,
    pub flat_gradient: f64,
    pub bisection_threshold: f64,
}

#[derive(Deserialize)]
#[serde(deny_unknown_fields, default)]
pub struct GdConfig {
    pub max_iter: u64,
    pub learning_rate: f64,
    pub slope_tracking: Option<SlopeTrackingConfig>,
}

#[derive(Deserialize)]
#[serde(deny_unknown_fields)]
pub struct SlopeTrackingConfig {
    pub sample_size: usize,
    pub threshold: f64,
    pub grain: u64,
}

//==================================================================================================
// Parsing utils.

// Allow either scalar or vector data input.
#[derive(Deserialize)]
#[serde(untagged)]
pub enum ListOrScalar<T> {
    Scalar(T),
    List(Vec<T>),
}

// Lists of strings can either be specified as regular arrays like in:
//    ["a", "b", "c", "d"]
// Or as whitespace-separated strings, like in:
//     "a b c d"
// The latter is shorter,
// but less flexible as it does not allow whitespace in strings.
#[derive(Deserialize)]
#[serde(untagged)]
enum ListOfStrings {
    Regular(Vec<String>),
    Compact(String),
}

impl From<ListOfStrings> for Vec<String> {
    fn from(lof: ListOfStrings) -> Self {
        use ListOfStrings as L;
        match lof {
            L::Regular(vec) => vec,
            L::Compact(input) => input.split_whitespace().map(ToOwned::to_owned).collect(),
        }
    }
}

impl<const N: usize> TryFrom<ListOfStrings> for [String; N] {
    type Error = String;

    fn try_from(lof: ListOfStrings) -> Result<Self, Self::Error> {
        use ListOfStrings as L;
        let vec = match lof {
            L::Regular(vec) => vec,
            L::Compact(input) => input.split_whitespace().map(ToOwned::to_owned).collect(),
        };
        if vec.len() == N {
            let mut it = vec.into_iter();
            Ok(array::from_fn(|_| it.next().unwrap()))
        } else {
            Err(format!(
                "Expected {N} items in list of strings, found {}.",
                vec.len()
            ))
        }
    }
}

//==================================================================================================
// Errors.

#[derive(Debug, Snafu)]
#[snafu(context(suffix(Err)))]
pub enum Error {
    #[snafu(display(
        "Error while reading config file:\n{}",
        complete_deserialize_error(source)
    ))]
    Deserialize { source: toml::de::Error },
}

// Best-attempt to improve over https://github.com/serde-rs/serde/issues/773.
fn complete_deserialize_error(e: &toml::de::Error) -> String {
    let mut message = format!("{e}");
    let pattern = "data did not match any variant of untagged enum ";
    if message.contains(pattern) {
        let name = message.trim().rsplit(' ').next().unwrap();
        let accepted_types = match name {
            // TODO: generate these with procedural macros?
            "ListOfStrings" => vec!["Array[String]", "String"],
            "ListOrScalar" => vec!["Array", "<single value>"],
            _ => panic!("Error in source code: enum {name} has not been handled."),
        };
        message = message.replace(
            &format!("{pattern}{name}"),
            &format!(
                "invalid type: expected one of [{}]",
                accepted_types.join(", ")
            ),
        );
    };
    message
}
