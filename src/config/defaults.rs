// Decisions for anything not user-provided.

use super::raw::{ListOrScalar, ParameterInput, RecordTrace};
use crate::{
    config::raw::{BfgsConfig, GdConfig, Init, Search, SlopeTrackingConfig, WolfeSearchConfig},
    Filters,
};

const DEFAULT_INIT_THETAS: &[f64] = &[1e-4, 1e-3, 1e-2, 1e-1];
pub(crate) fn init_thetas() -> ListOrScalar<ParameterInput<f64>> {
    ListOrScalar::List(
        DEFAULT_INIT_THETAS
            .iter()
            .map(|&th| ParameterInput::OnlyValue(th))
            .collect(),
    )
}
pub(crate) fn init() -> Init {
    Init {
        theta: init_thetas(),
        tau_1: None,
        tau_2: None,
        p_ab: None,
        p_ac: None,
        p_bc: None,
        p_ancient_gf: None,
    }
}

pub(crate) fn gf_times() -> Vec<f64> {
    vec![1.]
}

pub(crate) fn filters() -> Filters {
    Filters {
        max_clock_ratio: None,
        triplet_other_monophyly: false,
    }
}

pub(crate) fn search() -> Search {
    Search { bfgs: BfgsConfig::default() }
}

impl Default for BfgsConfig {
    fn default() -> Self {
        BfgsConfig {
            max_iter: 1_000,
            wolfe: WolfeSearchConfig::default(),
            step_size_threshold: 1e-9,
            slope_tracking: Some(SlopeTrackingConfig {
                sample_size: 20,
                threshold: 1e-3,
                grain: 5,
            }),
            record_trace: RecordTrace::No,
        }
    }
}

impl Default for WolfeSearchConfig {
    fn default() -> Self {
        // Taken from Nocedal and Wright 2006.
        let c1 = 1e-4;
        let c2 = 0.1;
        WolfeSearchConfig {
            c1,
            c2,
            init_step_size: 1.0,
            step_decrease: c2,
            step_increase: 10., // (custom addition)
            flat_gradient: 1e-20,
            bisection_threshold: 1e-1,
        }
    }
}

impl Default for GdConfig {
    fn default() -> Self {
        GdConfig {
            max_iter: 1_000,
            learning_rate: 1e-5,
            slope_tracking: Some(SlopeTrackingConfig {
                sample_size: 100,
                threshold: 1e-3,
                grain: 50,
            }),
        }
    }
}
