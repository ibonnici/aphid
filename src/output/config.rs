// Render config to json
// to trace every parameter used if needed.
// Serde does most of the job,
// but a reference to the interner is necessary
// to resolve species symbols into name strings.
use std::fmt;

use serde_json::{json, Value as JsValue, Value as Js};

use crate::{
    config::{SpeciesTriplet, Taxa},
    interner::{Interner, ResolvedSymbol},
    Config,
};

//==================================================================================================
// 'Resolver' types carry both a reference to their data
// and to the interner to resolve symbols within them.

pub struct Resolver<'i> {
    config: &'i Config,
    interner: &'i Interner,
}

struct TaxaResolver<'i> {
    taxa: &'i Taxa,
    interner: &'i Interner,
}

struct TripletResolver<'i> {
    triplet: &'i SpeciesTriplet,
    interner: &'i Interner,
}

impl Config {
    pub fn resolve<'d>(&'d self, interner: &'d Interner) -> Resolver<'d> {
        Resolver { config: self, interner }
    }
}

impl Taxa {
    fn resolve<'d>(&'d self, interner: &'d Interner) -> TaxaResolver<'d> {
        TaxaResolver { taxa: self, interner }
    }
}

impl SpeciesTriplet {
    fn resolve<'d>(&'d self, interner: &'d Interner) -> TripletResolver<'d> {
        TripletResolver { triplet: self, interner }
    }
}

//==================================================================================================
// Resolve as json values.

impl Resolver<'_> {
    pub fn json(&self) -> JsValue {
        let Resolver { config, interner } = self;
        let Config {
            trees,
            taxa,
            filters,
            unresolved_length,
            searches: search,
        } = config;
        json!({
            "trees": trees,
            "taxa": taxa.resolve(interner).json(),
            "filters": filters,
            "unresolved_length": unresolved_length,
            "search": search,
        })
    }
}

impl TaxaResolver<'_> {
    pub fn json(&self) -> JsValue {
        let TaxaResolver { taxa, interner } = self;
        let Taxa { triplet, outgroup, other } = taxa;
        let outgroup = Js::Array(
            outgroup
                .iter()
                .map(|&s| json! {interner.resolve(s).unwrap()})
                .collect(),
        );
        let other = Js::Array(
            other
                .iter()
                .map(|&s| json! {interner.resolve(s).unwrap()})
                .collect(),
        );
        json!({
            "triplet": triplet.resolve(interner).json(),
            "outgroup": outgroup,
            "other": other,
        })
    }
}

impl TripletResolver<'_> {
    pub fn json(&self) -> JsValue {
        let TripletResolver { triplet, interner } = self;
        let SpeciesTriplet { a, b, c } = triplet;
        let [a, b, c] = [a, b, c].map(|&s| interner.resolve(s).unwrap());
        json! {
            [[a, b], c]
        }
    }
}

//==================================================================================================
// Debug displays (same logic without json for informal use within the program).

impl fmt::Debug for Resolver<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let Config {
            trees,
            taxa,
            filters,
            unresolved_length,
            searches: search,
        } = self.config;
        f.debug_struct("Config")
            .field("trees", trees)
            .field("taxa", &taxa.resolve(self.interner))
            .field("filters", filters)
            .field("unresolved_length", unresolved_length)
            .field("search", search)
            .finish()
    }
}

impl fmt::Debug for TaxaResolver<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let Taxa { triplet, outgroup, other } = self.taxa;
        let resolve = |slice: &[_]| {
            slice
                .iter()
                .map(|&symbol| ResolvedSymbol::new(symbol, self.interner))
                .collect::<Vec<_>>()
        };
        f.debug_struct("Taxa")
            .field("triplet", &triplet.resolve(self.interner))
            .field("outgroup", &resolve(outgroup))
            .field("other", &resolve(other))
            .finish()
    }
}

impl fmt::Debug for TripletResolver<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let [a, b, c] = self
            .triplet
            .as_array()
            .map(|s| ResolvedSymbol::new(s, self.interner));
        let ab = [a, b];
        f.debug_list().entry(&ab).entry(&c).finish()
    }
}
