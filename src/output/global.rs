// Use this structure to collect all analysis/run/top-level information
// susceptible to interest user after an aphid run.
// Export as a json file.

use serde::Serialize;

use crate::{BranchLength, Parameters};

/// All user-facing information used or produced by aphid
/// regarding the gene forest analyzed.
#[derive(Serialize, Default)]
pub struct Global {
    /// The number of trees analyzed.
    pub n_trees: usize,

    /// Number of triplets rejected based on their topology
    /// (incomplete or paraphyletic).
    pub n_excluded_triplets_topologies: usize,

    /// Number of triplets considered unresolved.
    pub n_unresolved_triplets: usize,

    /// Number of outgroup rejected based on their topology
    /// (empty or paraphyletic).
    pub n_excluded_outgroup_topologies: usize,

    /// Number of tree tops rejected
    /// (non-root LCA(triplet, outgroup)).
    /// Trees already rejected based on their triplet or outgroup are not counted.
    pub n_excluded_treetops_topologies: usize,

    /// Total number of trees excluded based on their topology.
    pub n_excluded_topologies: usize,

    /// Mean branch length over the forest.
    pub mean_branch_length: BranchLength,

    /// Mean length of a triplet,
    /// calculated after the topology filter and before the geometry filter.
    pub mean_length_triplet: BranchLength,

    /// Mean length of non-triplet branches ('other' and 'outgroup')
    /// calculated after the topology filter and before the geometry filter.
    pub mean_length_outgroup_other: BranchLength,

    /// Average imbalance between triplet branches lengths
    /// and the outgroup + other sections branches lengths.
    /// Only calculated if a maximum clock ratio is set.
    pub imbalance: Option<f64>,

    /// True if the imbalance means that triplets are longer on average.
    /// False if the outgroup + other sections are longer on average.
    pub triplet_longer: Option<bool>,

    /// Overall ratio over the genes forest.
    pub shape: Option<f64>,

    /// Number of trees excluded based on their geometry.
    pub n_excluded_geometries: usize,

    /// Final number of trees kept for likelihood calculation.
    pub n_included_trees: usize,

    /// Best parameters values found to maximize likelihood.
    pub estimate: Estimate,
}

#[derive(Serialize, Default)]
pub struct Estimate {
    /// Best ln-likelihood value found.
    pub ln_likelihood: f64,

    /// Corresponding parameters values.
    pub parameters: Parameters<f64>,
}
