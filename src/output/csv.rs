// Extract all scalar data from the detailed output
// and fit it into a large csv table to ease parsing by biologists.

use serde::Serialize;

use super::detail::{Internal, MeanLengths, Outgroup, Top, Tree, Triplet, TripletAnalysis};
use crate::{
    gene_tree::{NbBases, TripletTopology},
    BranchLength,
};

// One record per tree.
#[derive(Serialize)]
#[allow(clippy::struct_excessive_bools)] // On purpose for CSV splatting.
pub struct Record<'i> {
    // Global tree information.
    tree_id: &'i str,
    n_bases: NbBases,
    n_nodes_raw: usize,
    n_nodes_pruned: usize,

    //----------------------------------------------------------------------------------------------
    // Topology-related data.

    // Triplet section.
    triplet_n_missing: usize,
    triplet_lca: Option<usize>,
    triplet_n_paraphyletic: usize,
    triplet_included: bool,
    triplet_topology: Option<TripletTopology>,
    triplet_branch_length_a: Option<NbBases>,
    triplet_branch_length_b: Option<NbBases>,
    triplet_branch_length_c: Option<NbBases>,
    triplet_branch_length_d: Option<NbBases>,
    triplet_resolved: Option<bool>,

    // Outgroup section.
    outgroup_n_missing: usize,
    outgroup_lca: Option<usize>,
    outgroup_n_paraphyletic: usize,
    outgroup_included: bool,

    // Tree top section.
    top_lca: Option<usize>,
    top_n_external: Option<usize>,
    top_n_internal_triplet: Option<usize>,
    top_n_internal_outgroup: Option<usize>,
    top_included: Option<bool>,

    topology_included: bool,

    //----------------------------------------------------------------------------------------------
    // Geometry-related data.
    mean_lengths_triplet: Option<BranchLength>,
    mean_lengths_outgroup_other: Option<BranchLength>,
    mean_lengths_total: Option<BranchLength>,

    local_shape: Option<BranchLength>,
    geometry_included: bool,

    //----------------------------------------------------------------------------------------------
    mutation_rate: Option<f64>,
    ln_likelihood: Option<f64>,
}

//==================================================================================================
// Trivial extraction / flattening from the detailed tree information.

impl<'i> From<&Tree<'i>> for Record<'i> {
    fn from(tree: &Tree<'i>) -> Self {
        // Destructure all.
        let &Tree {
            id: tree_id,
            n_bases,
            n_nodes_raw,
            n_nodes_pruned,
            triplet:
                Triplet {
                    lca: triplet_lca,
                    missing: ref triplet_missing,
                    paraphyletic: ref triplet_paraphyletic,
                    analysis: ref triplet_analysis,
                    included: triplet_included,
                },
            outgroup:
                Outgroup {
                    lca: outgroup_lca,
                    missing: ref outgroup_missing,
                    paraphyletic: ref outgroup_paraphyletic,
                    included: outgroup_included,
                },
            ref top,

            topology_included,
            mean_lengths:
                MeanLengths {
                    total: mean_lengths_total,
                    triplet: mean_lengths_triplet,
                    outgroup_other: mean_lengths_outgroup_other,
                },
            local_shape,
            geometry_included,
            mutation_rate,
            ln_likelihood,
        } = tree;

        // Deeper into (optional) triplet information destructuring.
        let (triplet_topology, tl, triplet_resolved) =
            if let Some(TripletAnalysis { topology, branches_lengths: ref bl, resolved }) =
                *triplet_analysis
            {
                (Some(topology), Some(bl), Some(resolved))
            } else {
                (None, None, None)
            };

        // Deeper into (optional) tree top information destructuring.
        let (top_lca, top_n_external, int, top_included) =
            if let Some(Top { lca, ref external, ref internal, included }) = *top {
                (
                    Some(lca),
                    Some(external.len()),
                    Some(internal),
                    Some(included),
                )
            } else {
                (None, None, None, None)
            };

        let (top_n_internal_triplet, top_n_internal_outgroup) =
            if let Some(Some(Internal { triplet, outgroup })) = int {
                (Some(triplet.len()), Some(outgroup.len()))
            } else {
                (None, None)
            };

        // All is eventually flattened.
        Record {
            tree_id,
            n_bases,
            n_nodes_raw,
            n_nodes_pruned,
            triplet_lca,
            triplet_n_missing: triplet_missing.len(),
            triplet_n_paraphyletic: triplet_paraphyletic.len(),
            triplet_included,
            triplet_topology,
            triplet_branch_length_a: tl.map(|l| l.a),
            triplet_branch_length_b: tl.map(|l| l.b),
            triplet_branch_length_c: tl.map(|l| l.c),
            triplet_branch_length_d: tl.map(|l| l.d),
            triplet_resolved,
            outgroup_lca,
            outgroup_n_missing: outgroup_missing.len(),
            outgroup_n_paraphyletic: outgroup_paraphyletic.len(),
            outgroup_included,
            top_lca,
            top_n_external,
            top_n_internal_triplet,
            top_n_internal_outgroup,
            top_included,
            topology_included,
            mean_lengths_triplet,
            mean_lengths_outgroup_other,
            mean_lengths_total,
            local_shape,
            geometry_included,
            mutation_rate,
            ln_likelihood,
        }
    }
}
