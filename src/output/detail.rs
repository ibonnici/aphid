// Use this structure to collect all per-tree information
// susceptible to interest user after an aphid run.
// Export as a json file.

use arrayvec::ArrayVec;
use serde::Serialize;

use crate::{
    config::Taxa,
    gene_tree::{NbBases, TripletTopology},
    interner::{Interner, SpeciesSymbol},
    topological_analysis::{SectionAnalysis, TreeTop},
    BranchLength, LocalGeneTriplet, TopologicalAnalysis,
};

/// All user-facing information used or produced by aphid
/// regarding each particular gene tree.
#[derive(Serialize, Default)]
pub struct Tree<'i> {
    // Borrow from species/trees identifiers.
    /// Tree identifier, as given in input.
    pub id: &'i str,
    /// Sequence length.
    pub n_bases: NbBases,
    /// Number of nodes in the input tree.
    pub n_nodes_raw: usize,

    /// Number of nodes after only species of interest have been kept.
    pub n_nodes_pruned: usize,

    /// Status of the focal triplet in this tree.
    pub triplet: Triplet<'i>,

    /// Status of the designated outgroup in this tree.
    pub outgroup: Outgroup<'i>,

    /// Status of this tree's most ancestral nodes.
    /// Undefined if either no triplet species or no outgroup species were found.
    pub top: Option<Top<'i>>,

    /// Raised if the tree passed the topology filter.
    pub topology_included: bool,

    /// Mean branches lengths,
    /// undefined if the none of the species set
    /// they are supposed to be calculated over is found within the tree.
    pub mean_lengths: MeanLengths,

    /// "Absolute" ratio of mean triplet length
    /// and mean length of designated 'outgroup' and 'other' species:
    /// always superior to 1 to measure 'imbalance',
    /// regardless which of the numerator or denominator is greater.
    pub local_shape: Option<f64>,

    /// Raised if the tree passed the geometry filter.
    pub geometry_included: bool,

    /// Estimated mutation rate for this tree.
    /// (No estimate if the tree was excluded from analysis.)
    pub mutation_rate: Option<f64>,

    /// Likelihood of this single tree, provided it was included in the analysis.
    pub ln_likelihood: Option<f64>,
}

#[derive(Serialize, Default)]
pub struct Triplet<'i> {
    /// LCA(triplet): the most recent ancestor
    /// of the focal triplet species found in this tree.
    /// Undefined if all triplet species were missing.
    pub lca: Option<usize>,
    /// Triplet species not found within this tree.
    pub missing: ArrayVec<&'i str, 3>,
    /// Paraphyletic species found within this tree:
    /// these descend from LCA(triplet) but don't belong to the focal triplet.
    pub paraphyletic: Vec<&'i str>,

    /// Further information calculated iif the triplet is complete and monophyletic.
    pub analysis: Option<TripletAnalysis>,

    /// Raise unless the tree should be excluded from analysis
    /// based on this triplet topology.
    pub included: bool,
}

#[derive(Serialize)]
pub struct TripletAnalysis {
    /// Topology within this tree,
    /// assuming the reference topology was 'ABC' ~ '((A, B), C)'.
    pub topology: TripletTopology,

    /// Estimate of the number of mutations
    /// along the triplet branches `[a, b, c, d]`.
    /// The internal branch length `d` either represent:
    ///       - `ab` in `((:a, :b):ab, :c)` for topology `ABC`.
    ///       - `ac` in `(:b, (:a, :c):ac)` for topology `ACB`.
    ///       - `bc` in `(:a, (:c, :b):bc)` for topology `BCA`.
    pub branches_lengths: TripletLengths,

    /// True if the topology is considered sufficiently resolved
    /// to exclude discordant scenarios from likelihood calculations.
    pub resolved: bool,
}

#[derive(Serialize)]
pub struct TripletLengths {
    pub a: NbBases,
    pub b: NbBases,
    pub c: NbBases,
    pub d: NbBases,
}

#[derive(Serialize, Default)]
pub struct Outgroup<'i> {
    /// LCA(outgroup): the ost recent ancestor
    /// of the designated outgroup species found in this tree.
    /// Undefined if all outgroup species were missing.
    pub lca: Option<usize>,
    /// Outgroup species not found within this tree.
    pub missing: Vec<&'i str>,
    /// Paraphyletic species found within this tree:
    /// these descend from LCA(outgroup) but don't belong to the designated outgroup.
    pub paraphyletic: Vec<&'i str>,

    /// Raise unless the tree should be excluded from analysis
    /// based on this outgroup topology.
    pub included: bool,
}

#[derive(Serialize)]
pub struct Top<'i> {
    /// LCA(top): the most recent ancestor
    /// of LCA(triplet) and LCA(outgroup) found in this tree.
    pub lca: usize,
    /// Species not descending from LCA(top).
    /// If any, then LCA(top) is not the root of the tree.
    pub external: Vec<&'i str>,
    /// Species descending from LCA(top),
    /// but neither from LCA(triplet) or LCA(outgroup).
    /// Only defined if there is no direct lineage between LCA(triplet) and LCA(outgroup).
    pub internal: Option<Internal<'i>>,

    /// Raise unless the tree should be excluded from analysis
    /// based on this tree top topology.
    pub included: bool,
}

#[derive(Serialize)]
pub struct Internal<'i> {
    /// Species branching between LCA(top) and LCA(triplet).
    pub triplet: Vec<&'i str>,
    /// Species branching between LCA(top) and LCA(outgroup).
    pub outgroup: Vec<&'i str>,
}

#[derive(Serialize, Default)]
pub struct MeanLengths {
    /// Calculated over all species of interest found in this tree.
    pub total: Option<BranchLength>,

    /// Calculated over the focal triplet species found.
    pub triplet: Option<BranchLength>,

    /// Calculated over the designated outgroup species found
    /// plus the species designated as 'other'.
    pub outgroup_other: Option<BranchLength>,
}

//==================================================================================================
// Ease construction from internal types.

pub fn resolve_topology<'i>(
    top: &TopologicalAnalysis,
    taxa: &Taxa,
    triplet_other_monophyly: bool,
    interner: &'i Interner,
) -> (Triplet<'i>, Outgroup<'i>, Option<Top<'i>>) {
    let TopologicalAnalysis { triplet, outgroup, top } = top;

    let triplet = Triplet::resolve(triplet, taxa.triplet.iter(), interner);
    let outgroup = Outgroup::resolve(outgroup, taxa.outgroup.iter(), interner);
    let top = top
        .as_ref()
        .map(|top| Top::resolve(top, triplet_other_monophyly, interner));

    (triplet, outgroup, top)
}

impl<'i> Triplet<'i> {
    pub fn resolve<'int: 'i>(
        sa: &SectionAnalysis,
        triplet: impl Iterator<Item = SpeciesSymbol>,
        interner: &'int Interner,
    ) -> Self {
        use SectionAnalysis as SA;
        let resolve = |s| interner.resolve(s).unwrap();
        let (lca, missing, paraphyletic) = match sa {
            SA::AllMissing => (None, triplet.map(resolve).collect(), Vec::new()),
            SA::MissingSome(missing, lca) => (
                Some(lca.id),
                missing.iter().copied().map(resolve).collect(),
                lca.paraphyletic.iter().copied().map(resolve).collect(),
            ),
            SA::AllFound(lca) => (
                Some(lca.id),
                ArrayVec::new(),
                lca.paraphyletic.iter().copied().map(resolve).collect(),
            ),
        };
        Self {
            lca,
            missing,
            paraphyletic,
            analysis: None,
            included: sa.is_included_as_triplet(),
        }
    }
}

impl<'i> Outgroup<'i> {
    pub fn resolve<'int: 'i, 's>(
        sa: &'s SectionAnalysis,
        outgroup: impl Iterator<Item = &'s SpeciesSymbol>,
        interner: &'int Interner,
    ) -> Self {
        use SectionAnalysis as SA;
        let resolve = |&s| interner.resolve(s).unwrap();
        let (lca, missing, paraphyletic) = match sa {
            SA::AllMissing => (None, outgroup.map(resolve).collect(), Vec::new()),
            SA::MissingSome(missing, lca) => (
                Some(lca.id),
                missing.iter().map(resolve).collect(),
                lca.paraphyletic.iter().map(resolve).collect(),
            ),
            SA::AllFound(lca) => (
                Some(lca.id),
                Vec::new(),
                lca.paraphyletic.iter().map(resolve).collect(),
            ),
        };
        Self {
            lca,
            missing,
            paraphyletic,
            included: sa.is_included_as_outgroup(),
        }
    }
}

impl<'i> Top<'i> {
    pub fn resolve<'int: 'i, 's>(
        top: &'s TreeTop,
        triplet_other_monophyly: bool,
        interner: &'int Interner,
    ) -> Self {
        let resolve = |&s| interner.resolve(s).unwrap();
        let TreeTop { lca, ref external, ref internal } = *top;
        Self {
            lca,
            external: external.iter().map(resolve).collect(),
            internal: internal.as_ref().map(|i| Internal {
                triplet: i.triplet_side().iter().map(resolve).collect(),
                outgroup: i.outgroup_side().iter().map(resolve).collect(),
            }),
            included: top.is_included(triplet_other_monophyly),
        }
    }
}

//--------------------------------------------------------------------------------------------------

impl TripletAnalysis {
    pub fn new(loc: &LocalGeneTriplet) -> Self {
        let LocalGeneTriplet {
            topology, branches_lengths: [a, b, c, d], resolved, ..
        } = *loc;
        Self {
            topology,
            branches_lengths: TripletLengths { a, b, c, d },
            resolved,
        }
    }
}
