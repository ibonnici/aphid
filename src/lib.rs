pub mod config;
mod gene_tree;
pub mod genes_forest;
pub mod interner;
pub mod io;
pub mod it_mean;
pub mod learn;
mod lexer;
pub mod model;
pub mod optim;
pub mod output;
mod tree;

pub use config::{Config, Filters};
pub use gene_tree::{
    extract_local_triplet,
    topological_analysis::{self, TopologicalAnalysis},
    triplet::GeneTriplet,
    BranchLength, GeneTree, LocalGeneTriplet,
};
pub use genes_forest::{filter::imbalance, GenesForest};
pub use learn::optimize_likelihood;
pub use model::{likelihood::ln_likelihood, Parameters};

pub const VERSION: &str = env!("CARGO_PKG_VERSION");
pub const NAME: &str = env!("CARGO_PKG_NAME");
