// Filter out trees based on topological analysis.
//
// Inclusion criteria:
//   - Monophyletic triplet (exactly 3 species).
//   - Monophyletic outgroup (at least 1 species).
//   - LCA(triplet, outgroup) = root.

use crate::{
    gene_tree::{topological_analysis::SectionAnalysis, BranchLength},
    topological_analysis::TreeTop,
    GeneTree, TopologicalAnalysis,
};

impl TopologicalAnalysis {
    pub fn is_included(&self, triplet_other_monophyly: bool) -> bool {
        if !self.triplet.is_included_as_triplet() {
            return false;
        }

        if !self.outgroup.is_included_as_outgroup() {
            return false;
        }

        let Some(top) = &self.top else {
            panic!("Treetop should exist if both triplet and outgroup are included.")
        };

        if !top.is_included(triplet_other_monophyly) {
            return false;
        }

        true
    }
}

impl SectionAnalysis {
    pub(crate) fn is_included_as_triplet(&self) -> bool {
        // Reject incomplete triplets.
        let Self::AllFound(lca) = &self else {
            return false;
        };

        // Reject non-monophyletic triplets.
        if !lca.paraphyletic.is_empty() {
            return false;
        }

        true
    }

    pub(crate) fn is_included_as_outgroup(&self) -> bool {
        // Reject empty outgroups.
        let Some(lca) = self.lca() else {
            return false;
        };

        // Reject non-monophyletic outgroup.
        if !lca.paraphyletic.is_empty() {
            return false;
        }

        true
    }
}

impl TreeTop {
    pub(crate) fn is_included(&self, triplet_other_monophyly: bool) -> bool {
        // Reject non-root LCA(triplet, outgroup).
        if !self.external.is_empty() {
            return false;
        }

        // If required, reject non-monophyletic (triplet, other).
        if triplet_other_monophyly {
            // Rejection occurs if and only if 'other' species
            // branch from the 'outgroup' side of LCA(triplet, outgroup) subtree.
            let Some(internal) = &self.internal else {
                return false; // Or if one is in direct lineage with the other.
            };
            if !internal.outgroup_side().is_empty() {
                return false;
            }
        }

        true
    }
}

// ASSUMING the above topology-based filter passed, filter further based on geometry.
impl GeneTree {
    // Calculate mean branch length for a given subtree,
    // including the trimmed length from the root to its LCA.
    fn section_branch_length(&self, start: usize) -> BranchLength {
        let above = self.trimmed_length_from_root(start); // No mean to calculate.
        let below = self.mean_branch_length(start); // Branches fork from this node on.
        above + below
    }

    // Calculate mean branch length for the triplet, and the 'rest' = 'outgroup' + 'other'.
    pub fn triplet_rest_mean_lengths(
        &self,
        topology: &TopologicalAnalysis,
    ) -> (BranchLength, BranchLength) {
        let i_triplet = topology.triplet.lca().unwrap().id;
        let mt = self.section_branch_length(i_triplet);
        let mr = self.mean_branch_length_excluding(i_triplet, 0);
        (mt, mr)
    }

    // Decide whether to include with respect to clock ratio filter.
    pub fn is_shape_included(
        &self,
        mean_triplet: BranchLength,
        mean_rest: BranchLength,
        global_shape: f64,
        max_clock_ratio: f64,
    ) -> (bool, f64) {
        let local_shape = mean_triplet / mean_rest;
        let imbalance = imbalance(local_shape, global_shape).1;
        (imbalance < max_clock_ratio, imbalance)
    }
}

// Calculate the "absolute" ratio (always ⩾ 1),
// and raise the associated flag if numerator is larger than denominator.
pub fn imbalance(a: f64, b: f64) -> (bool, f64) {
    if a > b {
        (true, a / b)
    } else {
        (false, b / a)
    }
}

#[cfg(test)]
mod tests {

    use float_eq::float_eq;
    use itertools::{EitherOrBoth, Itertools};

    use crate::{
        config::{Filters, SpeciesTriplet, Taxa},
        gene_tree::topological_analysis::{InternalSpecies, SectionAnalysis, SectionLca, TreeTop},
        interner::Interner,
        GeneTree, TopologicalAnalysis,
    };

    #[test]
    #[allow(clippy::too_many_lines)] // It does take vertical space to test many trees.
    fn analysis_then_filter() {
        use EitherOrBoth as EO;
        let mut interner = Interner::new();
        let mut g = |c| interner.get_or_intern(c);

        // Ease analysis "literals".
        macro_rules! analysis {
            ( triplet: $triplet:tt,
              outgroup: $outgroup:tt,
              top: $($top:tt)?,
            ) => {
                 TopologicalAnalysis {
                     triplet: analysis!(section: $triplet),
                     outgroup: analysis!(section: $outgroup),
                     top: analysis!(top: $($top)?),
                 }
            };
            (section: {None}) => { SectionAnalysis::AllMissing };
            (section: {lca: $lca:tt, missing: $miss:tt}) => {
                SectionAnalysis::MissingSome(vecs!$miss, analysis!(lca: $lca))
            };
            (section: {lca: $lca:tt}) => { SectionAnalysis::AllFound(analysis!(lca: $lca)) };
            (lca: {$id:literal}) => { SectionLca { id: $id, paraphyletic: Vec::new() } };
            (lca: {$id:literal, paraphyletic: $para:tt}) => {
                SectionLca { id: $id, paraphyletic: vecs!$para }
            };
            (top: { None }) => { None };
            (top: {lca: $id:literal, intern: $int:tt, extern: $ext:tt}) => { Some(
                    TreeTop { lca: $id, internal: analysis!(int: $int), external: vecs!$ext }
            )};
            (int: { None }) => { None };
            (int: { $sp:tt $sep:literal }) => { Some(
                InternalSpecies { species: vecs!$sp, first_outgroup: $sep }
            )};
        }
        macro_rules! vecs {
            ($($item:ident),*) => {
                vec![$(interner.get_or_intern(stringify!($item))),*]
            };
        }

        // Test analysis on a few trees:
        let taxa = Taxa {
            triplet: SpeciesTriplet { a: g("T"), b: g("U"), c: g("V") },
            outgroup: vec![g("O"), g("P"), g("Q")],
            other: vec![g("X"), g("Y"), g("Z")],
        };

        let default_filter = || Filters {
            max_clock_ratio: None,
            triplet_other_monophyly: false,
        };
        let mut filters = default_filter();
        let reset_filters = |filters: &mut Filters| {
            *filters = default_filter();
        };

        let analysis = |input, expected, interner: &mut _| {
            println!("Checking analysis for tree:\n  {input}");
            let input = format!("{input} 0"); // Add dummy sequence length.
            let gtree = GeneTree::from_input(&input, interner).unwrap();
            let actual = gtree.topological_analysis(&taxa);
            if expected != actual {
                // Display side by side to ease comparison.
                let exp = format!("{expected:#?}");
                let act = format!("{actual:#?}");
                let width = exp.lines().map(str::len).max().unwrap() + 3;
                let mut lines = Vec::new();
                lines.push(format!("{:-^width$} | {:-^width$}", "expected", "actual"));
                for lr in exp.lines().zip_longest(act.lines()) {
                    lines.push(match lr {
                        EO::Both(e, a) => format!("{e:<width$} | {a}"),
                        EO::Left(e) => format!("{e:<width$} |"),
                        EO::Right(a) => format!("{:<width$} | {a}", ""),
                    });
                }
                panic!("Unexpected analysis result:\n{}\n", lines.join("\n"));
            };
        };
        let included = |input, included, filters: &Filters, interner: &mut _| {
            println!("Checking inclusion for tree:\n  {input}\nwith filters:\n  {filters:?}");
            let input = format!("{input} 0"); // Add dummy sequence length.
            let gtree = GeneTree::from_input(&input, interner).unwrap();
            let analysis = gtree.topological_analysis(&taxa);
            assert_eq!(
                included,
                analysis.is_included(filters.triplet_other_monophyly),
                "Tree incorrectly filtered."
            );
        };

        // Minimal included tree.
        //
        //         │
        //     ┌───0────┐
        //     │        │
        //     │     ┌──2──┐
        //     │     │     │
        //     │   ┌─3─┐   │
        //     1   4   5   6
        //     O   T   U   V
        let input = "(O:1,((T:4,U:5):3,V:6):2):0;";
        analysis(
            input,
            analysis! {
                triplet: {lca: {2}},
                outgroup: {lca: {1}, missing: [P, Q]},
                top: {lca: 0, intern: { [] 0 }, extern: []},
            },
            &mut interner,
        );
        included(input, true, &filters, &mut interner);

        // Flesh outgroup.
        //
        //                │
        //          ┌─────0─────┐
        //          │           │
        //       ┌──1──┐     ┌──6──┐
        //       │     │     │     │
        //     ┌─2─┐   │   ┌─7─┐   │
        //     3   4   5   8   9   10
        //     O   P   Q   T   U   V
        let input = "(((O:3,P:4):2,Q:5):1,((T:8,U:9):7,V:10):6):0;";
        analysis(
            input,
            analysis! {
                triplet: {lca: {6}},
                outgroup: {lca: {1}},
                top: {lca: 0, intern: { [] 0 }, extern: []},
            },
            &mut interner,
        );
        included(input, true, &filters, &mut interner);

        // Introduce 'other' species. Maximal included tree with these included taxa.
        //
        //                   │
        //             ┌─────0───────┐
        //        ┌────1───┐         │
        //        │        │   ┌─────8──────┐
        //        │        │   │            │
        //     ┌──2──┐     │   │        ┌───10─┐
        //     │     │     │   │        │      │
        //     │   ┌─4─┐   │   │     ┌──11─┐   │
        //     │   │   │   │   │     │     │   │
        //     │   │   │   │   │   ┌─12┐   │   │
        //     3   5   6   7   9   13  14  15  16
        //     O   P   Q   X   Y   T   U   V   Z
        let input = "(((O:3,(P:5,Q:6):4):2,X:7):1,(Y:9,(((T:13,U:14):12,V:15):11,Z:16):10):8):0;";
        analysis(
            input,
            analysis! {
                triplet: {lca: {11}},
                outgroup: {lca: {2}},
                top: {lca: 0, intern: { [Y, Z, X] 2 }, extern: []},
            },
            &mut interner,
        );
        included(input, true, &filters, &mut interner);
        // Rejected if internal outgroup species are disallowed.
        filters.triplet_other_monophyly = true;
        included(input, false, &filters, &mut interner);
        reset_filters(&mut filters);

        // Flipped tree to check that internal species 'sides' are correctly identified.
        //
        //                     │
        //           ┌─────────0───────────┐
        //           │                ┌────10──┐
        //     ┌─────1──────┐         │        │
        //     │            │         │        │
        //     │        ┌───3──┐   ┌──11─┐     │
        //     │        │      │   │     │     │
        //     │     ┌──4──┐   │   │   ┌─13┐   │
        //     │     │     │   │   │   │   │   │
        //     │   ┌─5─┐   │   │   │   │   │   │
        //     2   6   7   8   9   12  14  15  16
        //     Y   T   U   V   Z   O   P   Q   X
        let input = "((Y:2,(((T:6,U:7):5,V:8):4,Z:9):3):1,((O:12,(P:14,Q:15):13):11,X:16):10):0;";
        analysis(
            input,
            analysis! {
                triplet: {lca: {4}},
                outgroup: {lca: {11}},
                top: {lca: 0, intern: { [Y, Z, X] 2 }, // <- Outgroup side is still collected last.
                              extern: []},
            },
            &mut interner,
        );
        included(input, true, &filters, &mut interner);
        filters.triplet_other_monophyly = true;
        included(input, false, &filters, &mut interner);
        reset_filters(&mut filters);

        // Included tree. One missing outgroup. Three other internal species.
        //
        //               │
        //        ┌──────0───────┐
        //        │              │
        //        │        ┌─────6──────┐
        //        │        │            │
        //     ┌──1──┐     │        ┌───8──┐
        //     │     │     │        │      │
        //     │   ┌─3─┐   │     ┌──9──┐   │
        //     │   │   │   │     │     │   │
        //     │   │   │   │   ┌─10┐   │   │
        //     2   4   5   7   11  12  13  14
        //     X   O   P   Y   T   U   V   Z
        let input = "((X:2,(O:4,P:5):3):1,(Y:7,(((T:11,U:12):10,V:13):9,Z:14):8):6);";
        analysis(
            input,
            analysis! {
                triplet: {lca: {9}},
                outgroup: {lca: {3}, missing: [Q]},
                top: {lca: 0, intern: { [Y, Z, X] 2 }, extern: []},
            },
            &mut interner,
        );
        included(input, true, &filters, &mut interner);
        filters.triplet_other_monophyly = true;
        included(input, false, &filters, &mut interner);
        reset_filters(&mut filters);

        // Similar, but all internal species stand on the outgroup side.
        //
        //                    │
        //           ┌────────0─────────┐
        //           │                  │
        //        ┌──1───────┐          │
        //        │          │          │
        //     ┌──2──┐     ┌─7─┐        │
        //     │     │     │   │        │
        //     │   ┌─4─┐   │   │     ┌──10─┐
        //     │   │   │   │   │     │     │
        //     │   │   │   │   │   ┌─11┐   │
        //     3   5   6   8   9   12  13  14
        //     X   O   P   Y   Z   T   U   V
        let input = "(((X:3,(O:5,P:6):4):2,(Y:8,Z:9):7):1,((T:12,U:13):11,V:14):10);";
        analysis(
            input,
            analysis! {
                triplet: {lca: {10}},
                outgroup: {lca: {4}, missing: [Q]},
                top: {lca: 0, intern: { [X, Y, Z] 0 }, extern: []},
            },
            &mut interner,
        );
        included(input, true, &filters, &mut interner);
        filters.triplet_other_monophyly = true;
        included(input, false, &filters, &mut interner);
        reset_filters(&mut filters);

        // Rejected tree: triplet is not monophyletic.
        //
        //                   │
        //             ┌─────0───────┐
        //        ┌────1───┐         │
        //        │        │   ┌─────8──────┐
        //        │        │   │            │
        //     ┌──2──┐     │   │        ┌───10─┐
        //     │     │     │   │        │      │
        //     │   ┌─4─┐   │   │     ┌──11─┐   │
        //     │   │   │   │   │     │     │   │
        //     │   │   │   │   │   ┌─12┐   │   │
        //     3   5   6   7   9   13  14  15  16
        //     O   P   Q   X   Y   T   U   Z   V
        let input = "(((O:3,(P:5,Q:6):4):2,X:7):1,(Y:9,(((T:13,U:14):12,Z:15):11,V:16):10):8):0;";
        analysis(
            input,
            analysis! {
                triplet: {lca: {10, paraphyletic: [Z]}},
                outgroup: {lca: {2}},
                top: {lca: 0, intern: { None }, extern: []},
            },
            &mut interner,
        );
        included(input, false, &filters, &mut interner);

        // Rejected tree: outgroup is not monophyletic.
        //
        //                   │
        //             ┌─────0───────┐
        //        ┌────1───┐         │
        //        │        │   ┌─────8──────┐
        //        │        │   │            │
        //     ┌──2──┐     │   │        ┌───10─┐
        //     │     │     │   │        │      │
        //     │   ┌─4─┐   │   │     ┌──11─┐   │
        //     │   │   │   │   │     │     │   │
        //     │   │   │   │   │   ┌─12┐   │   │
        //     3   5   6   7   9   13  14  15  16
        //     O   P   X   Q   Y   T   U   V   Z
        let input = "(((O:3,(P:5,X:6):4):2,Q:7):1,(Y:9,(((T:13,U:14):12,V:15):11,Z:16):10):8):0;";
        analysis(
            input,
            analysis! {
                triplet: {lca: {11}},
                outgroup: {lca: {1, paraphyletic: [X]}},
                top: {lca: 0, intern: { None }, extern: []},
            },
            &mut interner,
        );
        included(input, false, &filters, &mut interner);

        // Rejected tree: X is external.
        //
        //        │
        //     ┌──0──────┐
        //     │         │
        //     │   ┌─────2─────┐
        //     │   │           │
        //     │   │        ┌──4───┐
        //     │   │        │      │
        //     │   │     ┌──5──┐   │
        //     │   │     │     │   │
        //     │   │   ┌─6─┐   │   │
        //     1   3   7   8   9   10
        //     X   O   T   U   V   Y
        let input = "(X:1,(O:3,(((T:7,U:8):6,V:9):5,Y:10):4):2):0;";
        analysis(
            input,
            analysis! {
                triplet: {lca: {5}},
                outgroup: {lca: {3}, missing: [P, Q]},
                top: {lca: 2, intern: { [Y] 1 }, extern: [X]},
            },
            &mut interner,
        );
        included(input, false, &filters, &mut interner);

        // Rejected tree: X and Z are external.
        //
        //        ┌─────────0──────────┐
        //        │                    │
        //     ┌──1──────┐             │
        //     │         │             │
        //     │   ┌─────3─────┐       │
        //     │   │           │       │
        //     │   │        ┌──5───┐   │
        //     │   │        │      │   │
        //     │   │     ┌──6──┐   │   │
        //     │   │     │     │   │   │
        //     │   │   ┌─7─┐   │   │   │
        //     2   4   8   9   10  11  12
        //     X   O   T   U   V   Y   Z
        let input = "((X:2,(O:4,(((T:8,U:9):7,V:10):6,Y:11):5):3):1,Z:12):0;";
        analysis(
            input,
            analysis! {
                triplet: {lca: {6}},
                outgroup: {lca: {4}, missing: [P, Q]},
                top: {lca: 3, intern: { [Y] 1 }, extern: [X, Z]},
            },
            &mut interner,
        );
        included(input, false, &filters, &mut interner);

        // Same, with reversed external nodes (checks hygiene of `intern` vs. `extern` sorting)
        //
        //     ┌─────0────────┐
        //     │              │
        //     │         ┌────2───────┐
        //     │         │            │
        //     │   ┌─────3─────┐      │
        //     │   │           │      │
        //     │   │        ┌──5───┐  │
        //     │   │        │      │  │
        //     │   │     ┌──6──┐   │  │
        //     │   │     │     │   │  │
        //     │   │   ┌─7─┐   │   │  │
        //     1   4   8   9   10  11 12
        //     Z   O   T   U   V   Y  X
        let input = "(Z:1,((O:4,(((T:8,U:9):7,V:10):6,Y:11):5):3,X:12):2):0;";
        analysis(
            input,
            analysis! {
                triplet: {lca: {6}},
                outgroup: {lca: {4}, missing: [P, Q]},
                top: {
                    lca: 3,
                    intern: { [Y] 1 },
                    extern: [Z, X] // No leak from 'intern' to 'extern'.
                },
            },
            &mut interner,
        );
        included(input, false, &filters, &mut interner);

        // Rejected tree: both outgroup and triplet are not monophyletic.
        //
        //        │
        //     ┌──0──────┐
        //     │         │
        //     │   ┌─────2─────┐
        //     │   │           │
        //     │   │        ┌──4───┐
        //     │   │        │      │
        //     │   │     ┌──5──┐   │
        //     │   │     │     │   │
        //     │   │   ┌─6─┐   │   │
        //     1   3   7   8   9   10
        //     P   O   X   T   U   V
        let input = "(P:1,(O:3,(((X:7,T:8):6,U:9):5,V:10):4):2):0;";
        analysis(
            input,
            analysis! {
                triplet: {lca: {4, paraphyletic: [X]}},
                outgroup: {lca: {0, paraphyletic: [X, T, U, V]}, missing: [Q]},
                top: {lca: 0, intern: { None }, extern: []},
            },
            &mut interner,
        );
        included(input, false, &filters, &mut interner);

        // Rejected tree: no outgroup species.
        //
        //        │
        //     ┌──0──────┐
        //     │         │
        //     │   ┌─────2─────┐
        //     │   │           │
        //     │   │        ┌──4───┐
        //     │   │        │      │
        //     │   │     ┌──5──┐   │
        //     │   │     │     │   │
        //     │   │   ┌─6─┐   │   │
        //     1   3   7   8   9   10
        //     X   Y   T   U   V   Z
        let input = "(X:1,(Y:3,(((T:7,U:8):6,V:9):5,Z:10):4):2):0;";
        analysis(
            input,
            analysis! {
                triplet: {lca: {5}},
                outgroup: { None },
                top: { None }, // Undefined.
            },
            &mut interner,
        );
        included(input, false, &filters, &mut interner);

        // Rejected tree: incomplete triplet.
        //
        //        │
        //     ┌──0──────┐
        //     │         │
        //     │   ┌─────2─────┐
        //     │   │           │
        //     │   │        ┌──4───┐
        //     │   │        │      │
        //     │   │     ┌──5──┐   │
        //     │   │     │     │   │
        //     │   │   ┌─6─┐   │   │
        //     1   3   7   8   9   10
        //     O   X   T   U   Y   Z
        let input = "(O:1,(X:3,(((T:7,U:8):6,Y:9):5,Z:10):4):2):0;";
        analysis(
            input,
            analysis! {
                triplet: {lca: {6}, missing: [V]},
                outgroup: {lca: {1}, missing: [P, Q] },
                top: {lca: 0, intern: { [X, Y, Z] 3 }, extern: [] },
            },
            &mut interner,
        );
        included(input, false, &filters, &mut interner);

        // Rejected tree: no triplet species.
        //
        //        │
        //     ┌──0──────┐
        //     │         │
        //     │   ┌─────2─────┐
        //     │   │           │
        //     │   │        ┌──4───┐
        //     │   │        │      │
        //     │   │     ┌──5──┐   │
        //     │   │     │     │   │
        //     │   │   ┌─6─┐   │   │
        //     1   3   7   8   9   10
        //     O   X   Y   P   Q   Z
        let input = "(O:1,(X:3,(((Y:7,P:8):6,Q:9):5,Z:10):4):2):0;";
        analysis(
            input,
            analysis! {
                triplet: { None },
                outgroup: {lca: {0, paraphyletic: [X, Y, Z]}},
                top: { None }, // Undefined.
            },
            &mut interner,
        );
        included(input, false, &filters, &mut interner);
    }

    #[test]
    fn clock_ratio() {
        // Same as above.
        let mut interner = Interner::new();
        let mut g = |c| interner.get_or_intern(c);
        let taxa = Taxa {
            triplet: SpeciesTriplet { a: g("T"), b: g("U"), c: g("V") },
            outgroup: vec![g("O"), g("P"), g("Q")],
            other: vec![g("X"), g("Y"), g("Z")],
        };

        let aseq = |a: f64, b: f64| {
            assert!(
                float_eq!(a, b, ulps <= 1u64),
                "{a} differs from {b} by more than 1 ulp.",
            );
        };

        let mut check = |input: &str, expected_triplet_mean, expected_rest_mean| {
            println!("Checking clock ratio for tree:\n  {input}");
            let input = format!("{input} 0"); // Add dummy sequence length.
            let gtree = GeneTree::from_input(&input, &mut interner).unwrap();
            let top = gtree.topological_analysis(&taxa);
            let (actual_triplet_mean, actual_rest_mean) = gtree.triplet_rest_mean_lengths(&top);
            aseq(expected_triplet_mean, actual_triplet_mean);
            aseq(expected_rest_mean, actual_rest_mean);
        };

        //
        //                   │100       // <- Root length should not be included.
        //             ┌─────┴───────┐
        //             │1            │8
        //        ┌────┴───┐         │
        //        │2       │   ┌─────┴──────┐
        //        │        │   │            │10
        //     ┌──┴──┐     │   │        ┌───┴──┐
        //     │     │4    │7  │9       │11    │
        //     │   ┌─┴─┐   │   │     ┌──┴──┐   │
        //     │3  │5  │6  │   │     │12   │   │
        //     │   │   │   │   │   ┌─┴─┐   │   │
        //     │   │   │   │   │   │13 │14 │15 │16
        //     O   P   Q   X   Y   T   U   V   Z
        let input = "(((O:3,(P:5,Q:6):4):2,X:7):1,(Y:9,(((T:13,U:14):12,V:15):11,Z:16):10):8):100;";
        check(
            input,
            // Triplet mean.
            ((13. + 12.) + (14. + 12.) + (15.)) / 3. + (11. + 10. + 8.),
            // Rest mean.
            ((3. + 2. + 1.)       // O
            + (5. + 4. + 2. + 1.) // P
            + (6. + 4. + 2. + 1.) // Q
            + (7. + 1.)           // X
            + (9. + 8.)           // Y
            + (16. + 10. + 8.))   // Z
             / 6.,
        );
    }
}
