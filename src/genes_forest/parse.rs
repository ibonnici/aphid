// Parse input file into a gene trees forest.

use std::{collections::HashMap, path::Path};

use snafu::{ResultExt, Snafu};

use super::GenesForest;
use crate::{
    interner::Interner,
    io::{self, read_file},
    lexer::{Error as LexerError, Lexer},
};

impl GenesForest {
    pub fn from_file(path: &Path, interner: &mut Interner) -> Result<Self, Error> {
        Self::parse(&read_file(path)?, interner)
    }

    pub(crate) fn parse(input: &str, interner: &mut Interner) -> Result<Self, Error> {
        let mut lexer = Lexer::new(input);
        let mut trees = Vec::new();
        let mut ids = Vec::new();
        let mut index = HashMap::new();
        // Count lines to contextualize potential errors.
        let mut l = 1;
        while !lexer.trim_start().consumed() {
            let gtree = lexer.read_gene_tree(interner).context(LexErr { line: l })?;
            let name = lexer
                .trim_start_on_line()
                .read_block()
                .ok_or_else(|| Error::Parse {
                    line: l,
                    mess: "Unexpected end of line while reading gene tree name.".into(),
                })?;
            lexer
                .trim_start_on_line()
                .strip_char(['\n'])
                .ok_or_else(|| unexpected_data_on_line(l, &mut lexer))?;
            let symbol = interner.get_or_intern(name);
            if index.contains_key(&symbol) {
                return Err(duplicated_tree_name(l, name));
            }
            index.insert(symbol, trees.len());
            ids.push(symbol);
            trees.push(gtree);
            l += 1;
        }
        Ok(GenesForest { trees, ids, _index: index })
    }
}

fn duplicated_tree_name(line: usize, name: &str) -> Error {
    Error::Parse {
        line,
        mess: format!("Tree name {name:?} already given earlier in the file."),
    }
}

fn unexpected_data_on_line(line: usize, lexer: &mut Lexer) -> Error {
    Error::Parse {
        line,
        mess: format!(
            "Unexpected trailing data: {}.",
            lexer.read_until(['\n']).unwrap().0
        ),
    }
}

#[derive(Debug, Snafu)]
#[snafu(context(suffix(Err)))]
pub enum Error {
    #[snafu(display("Lexing error while parsing gene forest, line {line}:\n{source}"))]
    Lex { line: usize, source: LexerError },
    #[snafu(display("Error while parsing gene forest:\n{mess}"))]
    Parse { line: usize, mess: String },
    #[snafu(transparent)]
    Read { source: io::Error },
}
