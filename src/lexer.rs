// Wrap an input string and consume it into various meaningful bits or "lexed tokens",
// with a very loose definition the word "lexer".
// This value's list of methods is meant to be extended in the other modules
// so that the "meaningful bits" can be anything needed,
// even sophisticated structures and "Newick tree"s in particular.

pub(crate) struct Lexer<'i> {
    input: &'i str,
}

macro_rules! lexerr {
    ($($message:tt)*) => {
        crate::lexer::Error::new(format!($($message)*))
    };
}
macro_rules! errout {
    ($($message:tt)*) => {
        return Err(crate::lexer::lexerr!($($message)*));
    };
}
pub(crate) use errout;
pub(crate) use lexerr;
use snafu::Snafu;

// https://doc.rust-lang.org/reference/whitespace.html
const WHITESPACE: [char; 11] = [
    // Special-case newline, so WHITESPACE[1..] excludes it.
    '\u{000A}', // (line feed, '\n')
    '\u{0009}', // (horizontal tab, '\t')
    '\u{000B}', // (vertical tab)
    '\u{000C}', // (form feed)
    '\u{000D}', // (carriage return, '\r')
    '\u{0020}', // (space, ' ')
    '\u{0085}', // (next line)
    '\u{200E}', // (left-to-right mark)
    '\u{200F}', // (right-to-left mark)
    '\u{2028}', // (line separator)
    '\u{2029}', // (paragraph separator)
];

impl<'i> Lexer<'i> {
    pub(crate) fn new(input: &'i str) -> Self {
        Lexer { input }
    }

    // Have a peek what's next to consume.
    #[cfg(test)]
    pub(crate) fn rest(&self) -> &str {
        self.input
    }

    pub(crate) fn consumed(&self) -> bool {
        self.input.is_empty()
    }

    // If input starts with one of the given chars, consume it and return it.
    // Assumes the remaining input is still invalid utf8.
    // Return None if there was no such leading char.
    pub(crate) fn strip_char<const N: usize>(&mut self, chars: [char; N]) -> Option<char> {
        for c in chars {
            if let Some(rest) = self.input.strip_prefix(c) {
                self.input = rest;
                return Some(c);
            }
        }
        None
    }

    // Consume input until the first given character (included).
    // Return both the read and the actual stop character found.
    // Return None and don't consume anything if no stop character was found.
    pub(crate) fn read_until<const N: usize>(
        &mut self,
        stops: [char; N],
    ) -> Option<(&'i str, char)> {
        self.input.find(stops).map(|i_stop| {
            let (read, rest) = self.input.split_at(i_stop);
            let (stop, rest) = rest.split_at(1);
            self.input = rest;
            (read, stop.chars().next().unwrap())
        })
    }

    // Consume input until the first given character (excluded).
    // Return both the read and the actual stop character found.
    // Return None and don't consume anything if no stop character was found.
    fn read_before<const N: usize>(&mut self, stops: [char; N]) -> Option<(&'i str, char)> {
        self.input.find(stops).map(|i_stop| {
            let (read, rest) = self.input.split_at(i_stop);
            self.input = rest;
            (read, rest.chars().next().unwrap())
        })
    }

    // Consume all leading whitespace.
    pub(crate) fn trim_start(&mut self) -> &mut Self {
        self.input = self.input.trim_start();
        self
    }

    // Consume all leading whitespace except newline.
    pub(crate) fn trim_start_on_line(&mut self) -> &mut Self {
        self.input = self.input.trim_matches(&WHITESPACE[1..]);
        self
    }

    // Read a whitespace-separated token, stopping before either whitespace or EOI.
    // Return None instead of an empty read.
    pub(crate) fn read_block(&mut self) -> Option<&'i str> {
        match self.read_before(WHITESPACE) {
            Some(("", _)) => None,
            Some((read, _)) => Some(read),
            None => {
                // This block was the last in input.
                let read = self.input;
                self.input = "";
                Some(read)
            }
        }
    }
}

//--------------------------------------------------------------------------------------------------
#[cfg(debug_assertions)]
impl Lexer<'_> {
    // Debug util to see the next few chars about to be consumed by the lexer.
    #[allow(dead_code)]
    fn display_peek(&self, n: usize) {
        println!(
            "Lexer: {:?}",
            self.input.chars().take(n).collect::<String>()
        );
    }
}

#[derive(Debug, Snafu)]
#[snafu(display("{message}"))]
pub struct Error {
    message: String,
}

impl Error {
    pub(crate) fn new(message: String) -> Self {
        Self { message }
    }
}

//==================================================================================================
#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn basic_lexing() {
        let input = "-+aa, bbb, c; rest";
        let mut lex = Lexer::new(input);
        assert_eq!(lex.strip_char(['-', '+']), Some('-'));
        assert_eq!(lex.strip_char(['-', '+']), Some('+'));
        assert_eq!(lex.strip_char(['-', '+']), None);
        assert_eq!(lex.read_until([',']), Some(("aa", ',')));
        lex.trim_start();
        assert_eq!(lex.read_before([',']), Some(("bbb", ',')));
        assert_eq!(lex.strip_char([',']), Some(','));
        lex.trim_start();
        assert_eq!(lex.read_until([',', ';']), Some(("c", ';')));
        // Empty read is ok.
        assert_eq!(lex.read_until([' ']), Some(("", ' ')));
        assert_eq!(lex.rest(), "rest");
    }
}
