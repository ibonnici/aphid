// Reading from / writing to files on disk.

use std::{
    borrow::Cow,
    env,
    fs::File,
    io::{self, Read},
    path::{Path, PathBuf},
    string::FromUtf8Error,
};

use serde::Serialize;
use snafu::{ResultExt, Snafu};

use crate::output;

// Read into owned data, emiting dedicated crate errors on failure.
pub(crate) fn read_file(path: &Path) -> Result<String, Error> {
    let tobuf = || -> PathBuf { path.into() };
    let mut file = File::open(path).context(IoErr { path: tobuf() })?;
    let mut buf = Vec::new();
    file.read_to_end(&mut buf)
        .context(IoErr { path: tobuf() })?;
    String::from_utf8(buf).context(Utf8Err { path: tobuf() })
}

// Wrap to integrate into aphid error system.
pub fn canonicalize(path: &Path) -> Result<PathBuf, Error> {
    path.canonicalize()
        .with_context(|_| CanonicalizeErr { path })
}

pub fn create_file(path: &Path, name: &str) -> Result<File, Error> {
    File::create(path).with_context(|_| AtErr { ctx: format!("creating {name} file"), path })
}

// Create, write and flush with harmonized error handling.
#[macro_export]
macro_rules! write_file {
    ($path:expr, $name:expr, $($content:tt)+) => {{ || -> Result<(), $crate::io::Error> {
        let (path, name) = ($path, $name);
        let mut file = $crate::io::create_file(path, name)?;
        write!(file, $($content)+)
            .with_context(|_| $crate::io::AtErr { ctx: format!("writing to {name} file"), path })?;
        file.flush().with_context(
            |_| $crate::io::AtErr { ctx: format!("flushing {name} file"), path},
        )?;
        Ok(())
     }()}};
}
pub use write_file;

// Display canonicalized paths relatively to current directory.
pub trait ToRelative {
    fn to_relative(&self) -> Result<Cow<Path>, Error>;
}
impl ToRelative for Path {
    fn to_relative(&self) -> Result<Cow<Path>, Error> {
        let cwd = env::current_dir().with_context(|_| CurDirErr)?;
        Ok(if let Some(diff) = pathdiff::diff_paths(self, cwd) {
            Cow::Owned(diff)
        } else {
            Cow::Borrowed(self)
        })
    }
}

#[derive(Debug, Snafu, Serialize)]
#[snafu(context(suffix(Err)))]
pub enum Error {
    #[snafu(display("IO error while reading file {}:\n{source}", path.display()))]
    Io {
        path: PathBuf,
        #[serde(serialize_with = "output::ser_display")]
        source: io::Error,
    },
    #[snafu(display("UTF-8 encoding error in file {}:\n{source}", path.display()))]
    Utf8 {
        path: PathBuf,
        #[serde(serialize_with = "output::ser_display")]
        source: FromUtf8Error,
    },
    #[snafu(display("Could not canonicalize path: {:?}:\n{source}", path.display()))]
    Canonicalize {
        #[serde(serialize_with = "output::ser_display")]
        source: std::io::Error,
        path: PathBuf,
    },
    #[snafu(display("Could not locate current directory: {source}"))]
    CurDir {
        #[serde(serialize_with = "output::ser_display")]
        source: std::io::Error,
    },
    #[snafu(display("Error while {ctx} at {}: {source}", path.display()))]
    #[snafu(visibility(pub))]
    At {
        ctx: String,
        path: PathBuf,
        #[serde(serialize_with = "output::ser_display")]
        source: std::io::Error,
    },
}
