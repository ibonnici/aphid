// The main input of the program is a large set of gene trees,
// read from input file in Newick notation, and associated with metadata.
// Assumed file structure: one tree per line.
//   <gene_tree> <sequence length> <tree/locus id>
//   <gene_tree> <sequence length> <tree/locus id>
//   ...
// The structure preserves the file ordering.

use std::collections::HashMap;

use crate::{interner::TreeSymbol, GeneTree};

// Move special methods implementations in dedicated modules.
pub(crate) mod filter;
pub mod parse;

pub struct GenesForest {
    trees: Vec<GeneTree>,
    ids: Vec<TreeSymbol>,
    _index: HashMap<TreeSymbol, usize>,
}

impl GenesForest {
    pub fn len(&self) -> usize {
        self.trees.len()
    }
    pub fn is_empty(&self) -> bool {
        self.trees.is_empty()
    }
    pub fn trees(&self) -> &[GeneTree] {
        &self.trees
    }
    pub fn ids(&self) -> &[TreeSymbol] {
        &self.ids
    }
}
