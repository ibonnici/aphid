// Species names and tree identifiers are interned to ease comparison,
// temper heap allocation and release input file resources.
//
// They are interned within the same structure,
// but provide convenience aliases to distinguish the symbols.

use core::fmt;

use string_interner::{backend::BufferBackend, symbol::SymbolUsize, StringInterner};

pub type Symbol = SymbolUsize;
pub type SpeciesSymbol = Symbol;
pub type TreeSymbol = Symbol;
pub type Interner = StringInterner<BufferBackend<Symbol>>;

// Prepend `:` when displaying resolved symbols
// to not mistake them for actual strings.
// TODO: This assumes that `:<symbol>` will not be ambiguous
// because identifiers names are supposed to contain few special characters,
// but this is not enforced in any way yet,
// and species name like `injection:, :attack`
// would make the output hard or impossible to read.
pub struct ResolvedSymbol<'i>(&'i str);

impl fmt::Debug for ResolvedSymbol<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_fmt(format_args!(":{}", self.0))
    }
}

impl<'i> ResolvedSymbol<'i> {
    pub fn new(symbol: Symbol, interner: &'i Interner) -> Self {
        ResolvedSymbol(interner.resolve(symbol).unwrap_or_else(|| {
            panic!(
                "Unresolved symbol: {symbol:?}. \
                 This likely results from an error in the source code: \
                 have several interners been used and their symbols mixed up?"
            )
        }))
    }
}
