// Specialize the binary tree
// into something with more dedicated meaning for gene trees.
// Branch data correspond to the node "length":
// length of the branch leading from its direct parent to itself.
// The root branch length is typically null,
// but it *may not be* as allowed by the Newick format,
// or when "length scars" are left on pruned trees.

use std::collections::HashMap;

use crate::{
    interner::SpeciesSymbol,
    it_mean::SummedMean,
    tree::{TerminalNode, Tree},
};

pub(crate) mod parse;
pub mod topological_analysis;
pub(crate) mod triplet;

pub use triplet::{
    extract_local as extract_local_triplet, LocalGeneTriplet, Topology as TripletTopology,
};

// Alias numeric types to use semantic "units".

// Estimate number of mutation per base.
pub type BranchLength = f64;

// Number of bases, to measure sequence length or number of mutations.
// Restrict to 32 bits to not silently loose precision when casting to f64.
// This should make the program crash with sequences lengths larger than 2^32.
pub(crate) type NbBases = u32;

// Product of the previous two.
pub(crate) type MeanNbBases = f64;
pub fn nb_mutations(branch_length: BranchLength, sequence_length: NbBases) -> MeanNbBases {
    branch_length * f64::from(sequence_length)
}

pub struct GeneTree {
    pub(crate) tree: Tree<BranchLength, Option<SpeciesSymbol>, SpeciesSymbol>,
    // Direct index with named nodes.
    pub(crate) index: HashMap<SpeciesSymbol, usize>,
    pub(crate) sequence_length: NbBases,
}

impl GeneTree {
    pub fn len(&self) -> usize {
        self.tree.len()
    }

    pub fn root(&self) -> usize {
        self.tree.root()
    }

    pub fn is_empty(&self) -> bool {
        self.tree.is_empty()
    }

    pub fn sequence_length(&self) -> NbBases {
        self.sequence_length
    }

    pub fn species_symbols(&self) -> impl Iterator<Item = SpeciesSymbol> + '_ {
        self.index.keys().copied()
    }

    // Get a pruned version of the tree,
    // with only the matching leaves kept.
    // External pruned nodes leave "lengths scars" on the root branch length.
    pub fn pruned(
        &self,
        pattern: impl Fn(usize, &TerminalNode<BranchLength, SpeciesSymbol>) -> bool,
    ) -> Self {
        // Adapt the pattern so it also produces an up-to-date index.
        let mut index = HashMap::new();
        let pruned = self.tree.pruned(
            pattern,
            |a, b| a + b,
            &mut index,
            |i, &iname, index| {
                if let Some(name) = iname {
                    index.insert(name, i);
                }
                iname
            },
            |i, &tname, index| {
                index.insert(tname, i);
                tname
            },
        );
        GeneTree { tree: pruned, index, ..*self }
    }

    // Iterate over terminal nodes names
    // and their corresponding branches length from the given node.
    // The length is "trimmed" because the starting branch length is *not* taken into account.
    fn trimmed_branches_lengths(
        &self,
        start: usize,
    ) -> impl Iterator<Item = (usize, (SpeciesSymbol, BranchLength))> + '_ {
        // Accumulate "distance" with branches lengths from root to tips.
        self.tree.node_to_leaves(
            start,
            0.,
            move |distance, (i, inode, _)| {
                if i == start {
                    distance
                } else {
                    distance + inode.branch
                }
            },
            move |distance, (i, tnode, _)| {
                let total = if i == start { distance } else { distance + tnode.branch };
                (tnode.payload, total)
            },
        )
    }

    // Same, but filter out one subtree given its LCA node.
    fn trimmed_branches_lengths_excluding(
        &self,
        excluded: usize,
        start: usize,
    ) -> impl Iterator<Item = (usize, (SpeciesSymbol, BranchLength))> + '_ {
        self.tree.filter_node_to_leaves(
            start,
            0.,
            move |distance, (i, inode, _)| {
                if i == excluded {
                    None
                } else if i == start {
                    Some(distance)
                } else {
                    Some(distance + inode.branch)
                }
            },
            move |distance, (i, tnode, _)| {
                (i != excluded).then(|| {
                    let total = if i == start { distance } else { distance + tnode.branch };
                    (tnode.payload, total)
                })
            },
        )
    }

    // Calculate mean branch lengths from the given node downwards.
    pub fn mean_branch_length(&self, start: usize) -> BranchLength {
        self.trimmed_branches_lengths(start)
            .map(|(_, (_, length))| length)
            .summed_mean::<usize>()
    }

    // Same, but exclude one subtree given its LCA node.
    pub fn mean_branch_length_excluding(&self, excluded: usize, start: usize) -> BranchLength {
        self.trimmed_branches_lengths_excluding(excluded, start)
            .map(|(_, (_, length))| length)
            .summed_mean::<usize>()
    }

    // Calculate length from the given node to the root, excluding root length.
    pub(crate) fn trimmed_length_from_root(&self, inode: usize) -> BranchLength {
        self.tree
            .to_root_from(inode)
            .map(|i| if self.tree.is_root(i) { 0.0 } else { *self.tree.nodes[i].branch() })
            .sum()
    }
}

#[cfg(test)]
mod tests {
    use crate::{interner::Interner, lexer::Lexer};

    #[test]
    fn trimmed_lengths() {
        let mut interner = Interner::new();

        // Ignore the root branch length.
        let input = "(A:1,(B:2,(((C:3,D:4):5,E:6):7,F:8):9):10):11; 12";
        //
        //    (branches lengths)          (nodes indexes)
        //           1
        //   ┌───────────── A            ┌────────────1 A
        // 11│         2                 │
        // ──┤    ┌──────── B           ─0    ┌───────3 B
        //   │    │       3              │    │
        //   │ 10 │    5┌── C            │    │     ┌─7 C
        //   └────┤  7┌─┤ 4              └────2   ┌─6
        //        │ ┌─┤ └── D                 │ ┌─5 └─8 D
        //        │ │ │ 6                     │ │ │
        //        │9│ └──── E                 │ │ └───9 E
        //        └─┤  8                      └─4
        //          └────── F                   └────10 F
        //
        let tree = Lexer::new(input).read_gene_tree(&mut interner).unwrap();
        let mut some =
            |id, name, length| Some((id, (interner.get_or_intern(name), f64::from(length))));

        macro_rules! check_iterators {
            ($($input:expr => {$($expected:tt)*})+) => {$(
                let mut it = $input;
                $(
                  assert_eq!(it.next(), some$expected);
                )*
                assert_eq!(it.next(), None);
            )+};
        }

        check_iterators! {

          // From the root.
          tree.trimmed_branches_lengths(0) => {
            (1, "A", 1)
            (3, "B", 10 + 2)
            (7, "C", 10 + 9 + 7 + 5 + 3)
            (8, "D", 10 + 9 + 7 + 5 + 4)
            (9, "E", 10 + 9 + 7 + 6)
            (10, "F", 10 + 9 + 8)
          }

          // From a subnode.
          tree.trimmed_branches_lengths(5) => {
            (7, "C", 5 + 3)
            (8, "D", 5 + 4)
            (9, "E", 6)
          }

          // Excluding subtrees.
          tree.trimmed_branches_lengths_excluding(7, 0) => {
            (1, "A", 1)
            (3, "B", 10 + 2)
            (8, "D", 10 + 9 + 7 + 5 + 4)
            (9, "E", 10 + 9 + 7 + 6)
            (10, "F", 10 + 9 + 8)
          }

          tree.trimmed_branches_lengths_excluding(6, 0) => {
            (1, "A", 1)
            (3, "B", 10 + 2)
            (9, "E", 10 + 9 + 7 + 6)
            (10, "F", 10 + 9 + 8)
          }

          tree.trimmed_branches_lengths_excluding(5, 0) => {
            (1, "A", 1)
            (3, "B", 10 + 2)
            (10, "F", 10 + 9 + 8)
          }

          tree.trimmed_branches_lengths_excluding(1, 0) => {
            (3, "B", 10 + 2)
            (7, "C", 10 + 9 + 7 + 5 + 3)
            (8, "D", 10 + 9 + 7 + 5 + 4)
            (9, "E", 10 + 9 + 7 + 6)
            (10, "F", 10 + 9 + 8)
          }

          tree.trimmed_branches_lengths_excluding(0, 0) => {}

          tree.trimmed_branches_lengths_excluding(5, 5) => {}

          tree.trimmed_branches_lengths_excluding(6, 5) => {
            (9, "E", 6)
          }

        }
    }
}
