// Disatisfied with tch-rs optimisers,
// here is a collection of homebrew optimisers procedures
// built on tch-rs Tensors and autograd.
//
// These don't share a common 'Optim' trait (anymore),
// but they implement some `minimize(f, init, ..)` method
// to minimize the given scalar function y = f(x),
// assuming that the tensor 'x'
// contains all and only the gradient-tracking leaves of it.

pub(crate) mod bfgs;
pub(crate) mod gd;
pub(crate) mod io;
pub(crate) mod tensor;
pub(crate) mod wolfe_search;

use std::num::NonZeroUsize;

use paste::paste;
use serde::Serialize;
use snafu::{ensure, Snafu};
use tch::Tensor;
use tensor::Loggable;
pub(crate) use tensor::OptimTensor;

// Abstact over the possible optimisations results.
pub(crate) trait OptimResult {
    fn best_vars(&self) -> &Tensor;
    fn best_loss(&self) -> f64;
    fn n_eval(&self) -> u64;
    fn n_diff(&self) -> u64;
}

// Ease implementation of the above for struct with explicit field names.
macro_rules! simple_optim_result_impl {
    ($Name:ident) => {
        impl crate::optim::OptimResult for $Name {
            fn best_vars(&self) -> &Tensor {
                &self.vars
            }
            fn best_loss(&self) -> f64 {
                self.loss
            }
            fn n_eval(&self) -> u64 {
                self.n_eval
            }
            fn n_diff(&self) -> u64 {
                self.n_diff
            }
        }
    };
}
use simple_optim_result_impl;

//==================================================================================================
// Keep track of the best candidate found.

pub(crate) struct Best {
    loss: f64,
    vars: Tensor,
}

impl Best {
    fn new(loss: f64, vars: &Tensor) -> Self {
        Self { loss, vars: vars.detach().copy() }
    }

    fn update(&mut self, loss: f64, vars: &Tensor) {
        if loss <= self.loss {
            self.loss = loss;
            self.vars = vars.detach().copy();
        }
    }
}

//==================================================================================================
// Keep track of latest search history.

struct History {
    slope: Box<dyn Fn(&Tensor) -> f64>, // Calculate slope from ordered trace data.
    // Rotating log.
    trace: Vec<f64>,
    size: usize,
    next_up: usize, // Rotates around the trace.
    full: bool,     // Raise on first wrapping.
}

impl History {
    fn new(size: NonZeroUsize, (kind, device): (tch::Kind, tch::Device)) -> Self {
        let size: usize = size.into();
        let n: i64 = size.try_into().unwrap_or_else(|e| {
            panic!(
                "Too much history required \
                 to fit in tensor length integer type: {size}:\n{e}"
            )
        });
        let x = Tensor::linspace(0, n - 1, n, (kind, device));
        #[allow(clippy::cast_precision_loss)]
        let mean_x = 0.5 * (n as f64 - 1.0);
        let dev_x = x - mean_x;
        let dev_x_square = dev_x.square().sum(kind).to_double();
        Self {
            slope: Box::new(move |y| {
                let mean_y = y.mean(kind);
                let dev_y = y - mean_y;
                let slope = dev_x.dot(&dev_y) / dev_x_square;
                slope.to_double()
            }),
            trace: vec![0.0; size],
            size,
            next_up: 0,
            full: false,
        }
    }

    fn update(&mut self, loss: f64) {
        self.trace[self.next_up] = loss;
        self.next_up += 1;
        if self.next_up == self.size {
            self.next_up = 0;
            self.full = true;
        }
    }

    // Calculate mean slope over the history.
    // Don't return if history hasn't been filled yet.
    fn slope(&self) -> Option<f64> {
        self.full.then(|| {
            // Reform a new ordered tensor from the rotated one.
            let trace = &self.trace;
            let i = self.next_up;
            let (latest, oldest) = trace.split_at(i);
            let [latest, oldest] = [latest, oldest].map(Tensor::from_slice);
            let y = Tensor::cat(&[oldest, latest], 0);

            (*self.slope)(&y)
        })
    }
}

// Useful to optimizers using history
// to periodically check slope and stop when it reaches some threshold.
#[derive(Debug, Serialize)]
pub(crate) struct SlopeTrackingConfig {
    history_size: NonZeroUsize,
    threshold: f64, // Positive.
    grain: u64,     // Wait this number of steps before checking slope (0 to never check)
}

impl SlopeTrackingConfig {
    pub(crate) fn new(history_size: usize, threshold: f64, grain: u64) -> Result<Self, Error> {
        ensure!(
            history_size > 1,
            cerr!(
                ("Two history points at least are required to calculate slope. \
                  Received {history_size}.")
            )
        );
        let history_size = NonZeroUsize::new(history_size).unwrap();
        ensure!(
            threshold >= 0.0,
            cerr!(("Slope threshold must be positive. Received: {threshold}.")),
        );
        Ok(Self { history_size, threshold, grain })
    }
}

struct SlopeTracker<'c> {
    config: &'c SlopeTrackingConfig,
    history: History,
}

impl<'c> SlopeTracker<'c> {
    fn new(config: &'c SlopeTrackingConfig, kindev: (tch::Kind, tch::Device)) -> Self {
        Self {
            history: History::new(config.history_size, kindev),
            config,
        }
    }

    // Return slope value if its magnitude is low enough.
    fn low_slope(&mut self, loss: f64, n_iter: u64) -> Option<f64> {
        self.history.update(loss);
        let sg = self.config.grain;
        if sg > 0 && n_iter % sg == 0 {
            if let Some(slope) = self.history.slope() {
                if slope.abs() < self.config.threshold {
                    return Some(slope);
                }
            }
        }
        None
    }
}

//==================================================================================================
// Errors.

// Not exactly sure how to best handle errors polymorphism among optimizers?
// Here, an explicit list of implementors is required.
#[derive(Debug, Snafu, Serialize)]
#[snafu(context(suffix(Err)))]
pub enum Error {
    #[snafu(display("Configuration:\n{mess}"))]
    Config {
        #[serde(flatten)]
        mess: String,
    },
    #[snafu(display("Gradient descent failure:\n{source}"))]
    Gd {
        #[serde(flatten)]
        source: gd::Error,
    },
    #[snafu(display("Failure of BFGS algorithm:\n{source}"))]
    Bfgs {
        #[serde(flatten)]
        source: bfgs::Error,
    },
    #[snafu(transparent)]
    Io {
        #[serde(flatten)]
        source: io::Error,
    },
}

macro_rules! cerr {
    ($fmt:tt) => {
        crate::optim::ConfigErr { mess: format!$fmt }
    };
}
use cerr;

macro_rules! error_convert {
    ($($mod:ident),+$(,)?) => {$(
        paste! {
            impl From<$mod::Error> for Error {
                fn from(e: $mod::Error) -> Self {
                    Error::[< $mod:camel >] { source: e }
                }
            }
        }
    )+};
}
error_convert! {
    gd,
    bfgs,
}

//==================================================================================================
// Tests.

#[cfg(test)]
mod tests {
    use std::{fs::File, io::Write, num::NonZero, path::PathBuf};

    use float_eq::float_eq;
    use rand::{
        distr::{Distribution, Uniform},
        rngs::StdRng,
        SeedableRng,
    };
    use rand_distr::StandardNormal;
    use tch::{Device, Kind, Tensor};

    use super::{bfgs::Log, wolfe_search::Config as WolfeSearchConfig};
    use crate::optim::{
        bfgs::Config as BfgsConfig, gd::Config as GdConfig, History, OptimResult, OptimTensor,
        SlopeTrackingConfig,
    };

    // Generate files to debug simple optimisation tests.
    const SAMPLE_GRID: bool = false;
    fn export_2d_loss_landscape(
        loss: impl Fn(&Tensor) -> Tensor,
        [a_min, a_max]: [f64; 2],
        [b_min, b_max]: [f64; 2],
        samples: i64,
        filename: &str,
    ) {
        if !SAMPLE_GRID {
            return;
        }
        println!("Dense evaluation on 2D grid..");
        let (kind, device) = (Kind::Double, Device::Cpu);

        // Export loss function grid.
        let a_grid = Tensor::linspace(a_min, a_max, samples, (kind, device));
        let b_grid = Tensor::linspace(b_min, b_max, samples, (kind, device));
        let mut file = File::create(filename).unwrap();
        for a in a_grid.iter::<f64>().unwrap() {
            for b in b_grid.iter::<f64>().unwrap() {
                let y = loss(&Tensor::from_slice(&[a, b])).to_double();
                writeln!(file, "{a:?}\t{b:?}\t{y:?}").unwrap();
            }
        }
    }

    fn bfgs_config() -> BfgsConfig {
        BfgsConfig {
            max_iter: 1_000,
            wolfe: WolfeSearchConfig {
                c1: 1e-4,
                c2: 0.1,
                init_step: 1.0,
                step_decrease: 0.1,
                step_increase: 10., // (custom addition)
                flat_gradient: 1e-20,
                bisection_threshold: 1e-1,
            },
            small_step: 1e-9,
            slope_tracking: Some(SlopeTrackingConfig {
                history_size: NonZero::new(20).unwrap(),
                threshold: 1e-3,
                grain: 5,
            }),
            log: Some(Log {
                id: "test".into(),
                folder: "./target".into(),
                main: Some(PathBuf::from("./target/bfgs.csv")),
                linsearch: Some(PathBuf::from("./target/bfgs_linsearch.csv")),
                variable_names: None,
            }),
        }
    }

    #[test]
    fn linear_regression() {
        // Test bfgs on an easy linear regression case.
        let mut rng = StdRng::seed_from_u64(12);
        let norm = StandardNormal;
        let (kind, device) = (Kind::Double, Device::Cpu);

        // Generate noisy data.
        let (a, b) = (5.0, 8.0);
        let n_data = 10;
        let x = Tensor::linspace(0, 100, n_data, (kind, device));
        let y = a * &x + b;
        let err = (0..n_data)
            .map(|_| norm.sample(&mut rng))
            .collect::<Vec<f64>>();
        let err = 0.05 * Tensor::from_slice(&err);
        let y = y + err;

        // Start from naive estimate.
        let init = Tensor::from_slice(&[0., 0.]);

        // Wrap model and data into a single 'loss' function to minimize.
        let loss = |p: &Tensor| {
            let (a, b) = (p.get(0), p.get(1));
            let prediction = a * &x + b;
            // Least squares error.
            let errors = &y - prediction;
            errors.square().sum(None)
        };
        export_2d_loss_landscape(loss, [-10., 10.], [-10., 10.], 1024, "./target/grid.csv");

        // Fit to data.
        let bfgs = bfgs_config();
        let bfgs = bfgs.minimize(loss, &init).unwrap();

        // Check that we get close enough.
        let [oa, ob] = [0, 1].map(|i| bfgs.best_vars().get(i).to_double());
        println!("Found with BFGS: ({oa}, {ob}).");
        assert!(
            float_eq!(oa, a, abs <= 5e-2),
            "{oa} != {a} (𝛥 = {:e})",
            a - oa
        );
        assert!(
            float_eq!(ob, b, abs <= 5e-2),
            "{ob} != {b} (𝛥 = {:e})",
            b - ob
        );

        // Seek similar results with simple gradient descent.
        // Use less naive initial point.
        let init = bfgs.best_vars() + 1;
        let sl = SlopeTrackingConfig::new(100, 1e-5, 50).unwrap();
        let gd = GdConfig {
            max_iter: 20_000,
            step_size: 2e-5,
            slope_tracking: Some(sl),
        };
        let gd = gd.minimize(loss, &init, 1).unwrap();
        let [oa, ob] = [0, 1].map(|i| gd.best_vars().get(i).to_double());
        println!("Found with gradient descent: ({oa}, {ob}).");
        assert!(
            float_eq!(oa, a, abs <= 1e-2), // (les strict)
            "{oa} != {a} (𝛥 = {:e})",
            a - oa
        );
        assert!(
            float_eq!(ob, b, abs <= 2e-1), // (even less strict)
            "{ob} != {b} (𝛥 = {:e})",
            b - ob
        );

        // Hard-test evaluation counts just to trigger attention whenever the algorithm changes.
        assert_eq!((bfgs.n_eval(), bfgs.n_diff()), (25, 25)); // With BFGS instead of DFP.
        assert_eq!((gd.n_eval(), gd.n_diff()), (18102, 18101)); // One-off from weak slope stop.
        assert!(bfgs.best_loss() < gd.best_loss());
    }

    // A few test cases taken from:
    // https://en.wikipedia.org/wiki/Test_functions_for_optimization
    fn sphere(kind: Kind) -> impl Fn(&Tensor) -> Tensor {
        move |x: &Tensor| (x.square()).sum(kind)
    }

    fn rosenbrock(kind: Kind) -> impl Fn(&Tensor) -> Tensor {
        move |x: &Tensor| {
            let n = x.size1().unwrap();
            let base = x.slice(0, 0, n - 1, 1);
            let shift = x.slice(0, 1, n, 1);
            let lhs: Tensor = 100. * (shift - base.square());
            let rhs: Tensor = 1. - base;
            (lhs.square() + rhs.square()).sum(kind)
        }
    }

    fn beale() -> impl Fn(&Tensor) -> Tensor {
        move |x: &Tensor| {
            let (x, y) = (&x.get(0), &x.get(1));
            let aa: Tensor = 1.5 - x + x * y;
            let bb: Tensor = 2.25 - x + x * y.square();
            let cc: Tensor = 2.625 - x + x * y * y.square();
            aa.square() + bb.square() + cc.square()
        }
    }

    fn goldstein_price() -> impl Fn(&Tensor) -> Tensor {
        move |x: &Tensor| {
            let (x, y) = (&x.get(0), &x.get(1));
            let xs = &x.square();
            let ys = &y.square();
            let xy = &(x * y);
            let left: Tensor = 1.
                + (x + y + 1.).square() * (19. - 14. * x + 3. * xs - 14. * y + 6. * xy + 3. * ys);
            let right = 30.
                + (x * 2. - y * 3.).square()
                    * (18. - 32. * x + 12. * xs + 48. * y - 36. * xy + 27. * ys);
            left * right
        }
    }

    fn levi() -> impl Fn(&Tensor) -> Tensor {
        move |x: &Tensor| {
            let (x, y) = (&x.get(0), &x.get(1));
            let pi = std::f64::consts::PI;
            (3. * pi * x).sin().square()
                + (x - 1.).square() * (1. + (3. * pi * y).sin().square())
                + (y - 1.).square() * (1. + (2. * pi * y).sin().square())
        }
    }

    fn ackley() -> impl Fn(&Tensor) -> Tensor {
        move |x: &Tensor| {
            let (x, y) = (&x.get(0), &x.get(1));
            let tau = std::f64::consts::TAU;
            let e = std::f64::consts::E;
            (((x.square() + y.square()) * 0.5).sqrt() * -0.2).exp() * -20.
                - (((tau * x).cos() + (tau * y).cos()) * 0.5).exp()
                + e
                + 20.
        }
    }

    fn test_2d(
        loss: impl Fn(&Tensor) -> Tensor,
        (opt_x, opt_y): ([f64; 2], f64),
        init: [f64; 2],
        [x_range, y_range]: [[f64; 2]; 2],
        [n_eval, n_diff]: [u64; 2],
    ) -> impl OptimResult {
        // Possibly export landscape to debug.
        export_2d_loss_landscape(&loss, x_range, y_range, 1024, "./target/grid.csv");

        let init = Tensor::from_slice(&init);
        let bfgs = bfgs_config();
        let bfgs = bfgs.minimize(&loss, &init).unwrap();

        // Extract optimum found.
        for (actual, expected) in (0..=1)
            .map(|i| bfgs.best_vars().get(i).to_double())
            .zip(opt_x)
        {
            assert!(
                float_eq!(expected, actual, abs <= 1e-6),
                "{actual} != {expected} (𝛥 = {:e})",
                actual - expected
            );
        }
        let bl = bfgs.best_loss();
        assert!(float_eq!(opt_y, bl, abs <= 1e-6), "{bl:e} != {opt_y}");
        println!(
            "Optimisation success: {} evals and {} diffs.",
            bfgs.n_eval(),
            bfgs.n_diff()
        );
        // Also hard-test these numbers to trigger attention on algorithm change.
        assert_eq!(
            [bfgs.n_eval(), bfgs.n_diff()],
            [n_eval, n_diff],
            "New number of evaluations/diffs?"
        );
        bfgs
    }

    #[test]
    fn sphere_2d() {
        test_2d(
            sphere(Kind::Double),
            ([0., 0.], 0.),
            [1., 1.5],
            [[-2., 2.], [-2., 2.]],
            [4, 4],
        );
    }

    #[test]
    fn rosenbrock_2d() {
        test_2d(
            rosenbrock(Kind::Double),
            ([1., 1.], 0.),
            [-1., 2.5],
            [[-2., 2.], [-1., 3.]],
            [330, 330],
        );
    }

    #[test]
    fn beale_2d() {
        test_2d(
            beale(),
            ([3., 0.5], 0.),
            [-1., -2.],
            [[-4.5, 4.5], [-4.5, 4.5]],
            [62, 62],
        );
    }

    #[test]
    fn goldstein_price_2d() {
        test_2d(
            goldstein_price(),
            ([0., -1.], 3.),
            [-1., -2.],
            [[-2., 2.], [-3., 1.]],
            [99, 99],
        );
    }

    #[test]
    fn levi_2d() {
        let tau = std::f64::consts::TAU;
        test_2d(
            levi(),
            ([1., 1.], 0.),
            [-4., -3.],
            [[-tau, tau], [-tau, tau]],
            [13, 13],
        );
    }

    #[test]
    fn ackley_2d() {
        let tau = std::f64::consts::TAU;
        test_2d(
            ackley(),
            ([0., 0.], 0.),
            [-4.5, -3.],
            [[-tau, tau], [-tau, tau]],
            [107, 107],
        );
    }

    #[test]
    fn rosenbrock_20d() {
        let mut rng = StdRng::seed_from_u64(12);
        let unif = Uniform::new(-5., 5.).unwrap();
        let kind = Kind::Double;
        let loss = rosenbrock(kind);
        let init = (0..20).map(|_| unif.sample(&mut rng)).collect::<Vec<_>>();
        let init = Tensor::from_slice(&init);
        let bfgs = bfgs_config();
        let bfgs = bfgs.minimize(&loss, &init).unwrap();
        for i in 0..20 {
            let oi = bfgs.best_vars().get(i).to_double();
            assert!(
                float_eq!(oi, 1., abs <= 1e-3),
                "variable {i}: {oi} != 1 (𝛥 = {:e})",
                1. - oi
            );
        }
        assert!(float_eq!(bfgs.best_loss(), 0., abs <= 1e-6));
    }

    #[test]
    fn slope_calculation() {
        let kindev = (Kind::Double, Device::Cpu);
        let (a, b) = (5., 8.);
        let n_data: usize = 100;
        #[allow(clippy::cast_precision_loss)]
        let x = Tensor::linspace(0., n_data as f64 - 1., n_data.try_into().unwrap(), kindev);
        let y: Tensor = a * x + b;
        let mut history = History::new(n_data.try_into().unwrap(), kindev);
        for value in y.iter::<f64>().unwrap() {
            history.update(value);
        }
        let estimate = history.slope().unwrap();
        assert!(float_eq!(estimate, a, ulps <= 1));
    }
}
