// Implementation of BFGS algorithm with tch-rs,
// taken from Nocedal & Wright (2006).
// Stops on its own when the mean slope of the history
// becomes flat, provided the history is full.

use std::{cell::LazyCell, io::Write, path::PathBuf};

use color_print::{cformat, cprintln};
use serde::Serialize;
use snafu::{ensure, ResultExt, Snafu};
use tch::Tensor;

use crate::{
    optim::{
        self,
        io::{self, TraceFile},
        simple_optim_result_impl, tensor,
        wolfe_search::{
            self, Config as WolfeSearchConfig, Error as WolfeSearchError, LastStep, Location,
            Summary as WolfeSearchSummary,
        },
        Best, Error as OptimError, OptimTensor, SlopeTracker, SlopeTrackingConfig,
    },
    output,
};

// The exposed config.
#[derive(Debug, Serialize)]
pub(crate) struct Config {
    pub(crate) max_iter: u64,
    pub(crate) wolfe: WolfeSearchConfig,
    // If required.
    pub(crate) slope_tracking: Option<SlopeTrackingConfig>,
    // When gradient from one step to the next happen to be equal,
    // the hessian approximation breaks with a division by zero.
    // In this situation, consider that the search is over
    // if the step norm was shorter than this threshold value.
    pub(crate) small_step: f64,
    // Where and whether to log search traces.
    pub(crate) log: Option<Log>,
}

#[derive(Debug, Serialize)]
pub(crate) struct Log {
    pub(crate) id: String, // Useful for console output in parallel execution context.
    pub(crate) folder: PathBuf,
    pub(crate) main: Option<PathBuf>,
    pub(crate) linsearch: Option<PathBuf>,
    pub(crate) variable_names: Option<Vec<String>>,
}

#[derive(Debug)]
pub(crate) struct BfgsResult {
    loss: f64,
    vars: Tensor,
    n_eval: u64,
    n_diff: u64,
}
simple_optim_result_impl!(BfgsResult);

// The actual search state.
struct BfgsSearch<'c, F: Fn(&Tensor) -> Tensor> {
    // Search configuration.
    cf: &'c Config,

    // Objective function,
    fun: F,
    n_vars: usize, // Cached: expensive to query.

    // Current variables values.
    vars: Tensor,
    // Current loss.
    loss: f64,
    // Current grad.
    grad: Tensor,

    // Running estimate of the *inverse* hessian matrix.
    hess: Tensor,
    // (initial value + recycled constant)
    eye: Tensor,

    // Current search direction.
    dir: Tensor,

    // Iteration number.
    n_steps: u64,
    // Keep track of total number of function evaluation and differentiation.
    n_eval: u64,
    n_diff: u64,

    // Keep track of the best point found so far.
    best: Best,

    // If desired.
    slope_tracker: Option<SlopeTracker<'c>>,

    // Log files to write to.
    main_file: Option<TraceFile<'c>>, // Main search steps.
    lin_file: Option<TraceFile<'c>>,  // Linear search on every step.
}

impl Config {
    pub fn minimize(
        &self,
        fun: impl Fn(&Tensor) -> Tensor,
        init: &Tensor,
    ) -> Result<BfgsResult, OptimError> {
        let n_parms = init.size1().unwrap_or_else(|e| {
            panic!(
                "Parameters must be single-dimensional. Received dimensions {:?} instead:\n{e}",
                init.size(),
            )
        });
        let (kind, device) = (init.kind(), init.device());

        // Evaluate initial gradient value.
        let mut vars = init.set_requires_grad(true);
        vars.zero_grad();
        let loss_t = fun(&vars);
        let loss = loss_t.to_double();
        ensure!(loss.is_finite(), NonFiniteLossErr { loss, vars });

        loss_t.backward();
        let grad = vars.grad().copy();
        ensure!(grad.is_all_finite(), NonFiniteGradErr { grad, vars, loss });

        let best = Best::new(loss, &vars);

        // Initial hessian estimate is plain identity.
        let eye = Tensor::eye(n_parms, (kind, device));
        let hess = eye.copy();

        // First direction to search.
        let dir = (-&hess).mv(&grad);

        // Will only be differentiated as `x + alpha * p` during linear searches.
        vars = vars.detach();

        // Initialize slope tracking if desired.
        let slope_tracker = self.slope_tracking.as_ref().map(|s| {
            let mut t = SlopeTracker::new(s, (kind, device));
            assert!(t.low_slope(loss, 0).is_none());
            t
        });

        macro_rules! trace_file {
            ($file:ident) => {
                self.log
                    .as_ref()
                    .map(|log| log.$file.as_ref().map(|path| TraceFile::new(path)))
                    .flatten()
                    .transpose()?
            };
        }

        // Spin actual search steps.
        let mut search = BfgsSearch {
            fun,
            n_vars: vars.size1().unwrap().try_into().unwrap(),
            vars,
            loss,
            grad,
            hess,
            eye,
            dir,
            n_steps: 0,
            n_eval: 1,
            n_diff: 1,
            main_file: trace_file!(main),
            lin_file: trace_file!(linsearch),
            cf: self,
            slope_tracker,
            best,
        };

        Ok(search.run_search()?)
    }
}

impl<F: Fn(&Tensor) -> Tensor> BfgsSearch<'_, F> {
    fn run_search(&mut self) -> Result<BfgsResult, Error> {
        let cf = self.cf;
        let prefix = LazyCell::new(|| {
            if let Some(log) = &self.cf.log {
                cformat!("<s>{}::(BFGS)</> ", log.id)
            } else {
                String::new()
            }
        });
        if cf.max_iter == 0 {
            cprintln!("<k>{}No optimisation step asked for.</>", *prefix);
            return Ok(self.result());
        }

        self.write_header()?;
        if let Some(lin) = &mut self.lin_file {
            wolfe_search::write_header(
                lin,
                cf.log
                    .as_ref()
                    .and_then(|log| log.variable_names.as_ref())
                    .ok_or(self.n_vars),
            )?;
        };

        loop {
            // Pick a search direction.
            self.dir = (-&self.hess).mv(&self.grad);

            // Linear-search for an acceptable step size.
            let WolfeSearchSummary { last_step: res, n_eval, n_diff } = wolfe_search::search(
                Location::new(&self.fun, &self.dir, &self.vars, self.loss, &self.grad),
                &self.cf.wolfe,
                &mut self.best,
                (
                    cf.log.as_ref().map(|l| l.id.as_str()),
                    self.lin_file.as_mut(),
                    self.n_steps,
                ),
            )?;
            self.n_eval += n_eval;
            self.n_diff += n_diff;
            let &mut Self { n_steps, n_eval, n_diff, .. } = self;
            let Some(LastStep {
                step_size,
                step,
                vars_after_step,
                loss_after_step,
                grad_after_step,
            }) = res
            else {
                cprintln!(
                    "<k>{}Reached null step size after {n_steps} steps: \
                     {n_eval} evaluations and {n_diff} differentiations.</>",
                    *prefix,
                );
                break Ok(self.result());
            };

            // Check step norm.
            let norm = step.dot(&step).to_double();
            if norm <= 0.0 {
                cprintln!(
                    "<k>{}Reach silent step after {n_steps} steps: \
                     {n_eval} evaluations and {n_diff} differentiations.</>",
                    *prefix,
                );
                break Ok(self.result());
            }
            if norm < self.cf.small_step && self.grad == grad_after_step {
                cprintln!(
                    "<k>{}Reach silent grad step after {n_steps} steps: \
                     {n_eval} evaluations and {n_diff} differentiations.</>",
                    *prefix,
                );
                break Ok(self.result());
            }

            // Accept step.
            self.vars = vars_after_step;
            self.n_steps += 1;
            let n_steps = self.n_steps;

            // Check slope.
            if let Some(ref mut tracker) = self.slope_tracker {
                if let Some(lowslope) = tracker.low_slope(self.loss, n_steps) {
                    cprintln!(
                        "<k>{}Weak loss slope ({lowslope:e}) on iteration {n_steps}: stopping.</>",
                        *prefix
                    );
                    break Ok(self.result());
                }
            }

            // Update hessian approximation (eq. 6.17).
            let Self { ref grad, ref hess, .. } = *self;
            let diff = &grad_after_step - grad;
            let rho = 1. / &diff.dot(&step);
            let left = &self.eye - &rho * step.outer(&diff);
            let right = &self.eye - &rho * diff.outer(&step);
            self.hess = left.mm(&hess.mm(&right)) + rho * step.outer(&step);

            self.loss = loss_after_step;
            self.grad = grad_after_step;

            self.log(step_size)?;

            if n_steps >= cf.max_iter {
                cprintln!("<k>{}Max iteration reached: {n_steps}.</>", *prefix);
                break Ok(self.result());
            }
        }
    }

    fn result(&self) -> BfgsResult {
        let &Self { ref best, n_eval, n_diff, .. } = self;
        BfgsResult {
            vars: best.vars.detach().copy(),
            loss: best.loss,
            n_eval,
            n_diff,
        }
    }

    // Traces.
    fn write_header(&mut self) -> Result<(), Error> {
        let Some(TraceFile { path, ref mut file }) = self.main_file else {
            return Ok(());
        };
        let Some(log) = &self.cf.log else {
            return Ok(());
        };
        // Main headers.
        let mut header = ["step", "loss", "step_size"]
            .into_iter()
            .map(ToString::to_string)
            .collect::<Vec<_>>();
        for vec in ["var", "grad", "dir"] {
            if let Some(varnames) = &log.variable_names {
                for name in varnames {
                    header.push(format!("{vec}_{name}"));
                }
            } else {
                for i in 0..self.n_vars {
                    header.push(format!("{vec}_{i}"));
                }
            }
        }
        writeln!(file, "{}", header.join(","))
            .with_context(|_| io::AtErr { ctx: "writing header line", path })?;

        Ok(())
    }

    fn log(&mut self, step_size: f64) -> Result<(), Error> {
        let Some(TraceFile { path, ref mut file }) = self.main_file else {
            return Ok(());
        };
        let mut write = || -> Result<(), std::io::Error> {
            write!(file, "{}", self.n_steps)?;
            write!(file, ",{}", self.loss)?;
            write!(file, ",{step_size}")?;
            for t in [&self.vars, &self.grad, &self.dir] {
                for f in t.iter::<f64>().unwrap() {
                    write!(file, ",{f}")?;
                }
            }
            writeln!(file)
        };
        Ok(write().with_context(|_| io::AtErr { ctx: "writing trace file", path })?)
    }
}

#[derive(Debug, Snafu, Serialize)]
#[snafu(context(suffix(Err)))]
pub enum Error {
    #[snafu(display(
        "Obtained non-finite loss ({loss}) \
         before first step with these variables:\n{vars:?}"
    ))]
    NonFiniteLoss {
        loss: f64,
        #[serde(serialize_with = "tensor::ser")]
        vars: Tensor,
    },
    #[snafu(display(
        "Obtained non-finite gradient for finite loss ({loss}):\
         \ngradient: {grad:?}\nvariables: {vars:?}"
    ))]
    NonFiniteGrad {
        #[serde(serialize_with = "tensor::ser")]
        grad: Tensor,
        #[serde(serialize_with = "tensor::ser")]
        vars: Tensor,
        loss: f64,
    },
    #[snafu(transparent)]
    WolfeSearch {
        #[serde(flatten)]
        source: WolfeSearchError,
    },
    #[snafu(transparent)]
    Io {
        #[serde(serialize_with = "output::ser_display")]
        #[serde(flatten)]
        source: optim::io::Error,
    },
}
