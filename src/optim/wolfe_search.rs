// Given a local evaluation (y, grad) = (f(x), ∇f(x)) and a search direction p,
// perform a linear search to figure
// an acceptable positive step size 'alpha' in this direction,
// with respect to the strong Wolfe criteria.
// The algorithm implemented here is taken from Nocedal & Wright (2006),
// and the code uses their notations.
//
// There is no predefined value for alpha_max,
// but we ASSUME that all values of phi(alpha) are finite and non-NaN
// up to some horizon value h = alpha_max,
// and that acceptable step values do exist within this horizon.
//
//                           NaN and/or infinite values
//                           v v v
//               0          h
//    a = alpha  |----------|🗙-🗙-🗙-> ∞
//                 ^^  ^^^
//                 acceptable steps
//
// If the tested value for alpha is non-finite,
// it is reduced by a constant factor until we find a < h.
// If the bracketing phase requires increasing alpha again,
// we perform a binary search to find a < a_next < h.

use std::{cell::LazyCell, io::Write, ops::ControlFlow};

use color_print::{cformat, cprintln};
use serde::Serialize;
use snafu::{ensure, ResultExt, Snafu};
use tch::Tensor;

use crate::optim::{
    io::{self, TraceFile},
    tensor::{self, OptimTensor},
    Best,
};

#[derive(Debug, Serialize)]
pub(crate) struct Config {
    // 0 < c1 < c2 < 1
    pub(crate) c1: f64,
    pub(crate) c2: f64,

    // Starting point.
    pub(crate) init_step: f64, // Above 0.

    // Scaling factor to reduce alpha in search for a < h.
    pub(crate) step_decrease: f64, // (0 < · < 1)

    // Scaling factor to increase alpha during the bracketing phase,
    // unless some h < a was already found and we use binary search instead.
    pub(crate) step_increase: f64, // (1 < ·)

    // When f and f' evaluate to the same values
    // at both ends of the zooming interval
    // or in the sampled step size within it,
    // stop the search if the gradient is under this threshold.
    pub(crate) flat_gradient: f64, // (0 ⩽ ·)

    // If the next zoom candidate issued from cubic interpolation
    // is too close from the zoom interval
    // in terms of this fraction of the interval,
    // then use trivial bisection instead.
    pub(crate) bisection_threshold: f64, // (0 ⩽ · ⩽ 1/2)
}

pub(crate) struct Search<'s, 'c: 's, F: Fn(&Tensor) -> Tensor> {
    cf: &'c Config,

    // Borrow situation from the calling search step.
    loc: Location<'s, F>,

    // Evolvable search state.
    best: &'s mut Best, // Keep track of the best loss found so far.

    // Keep track of number of evaluations + differentiations performed.
    n_eval: u64,
    n_diff: u64,

    // Log search trace, given a search step number as first column.
    trace: Option<&'s mut TraceFile<'c>>,
    step_id: u64,
    n_vars: usize,
}

// Starting point of the linear search, supposed immutable in this context.
pub(crate) struct Location<'s, F: Fn(&Tensor) -> Tensor> {
    pub(crate) f: &'s F,      // Objective function.
    pub(crate) p: &'s Tensor, // Search direction.
    pub(crate) x: &'s Tensor, // Search starting point.
    pub(crate) y: f64,        // Search starting loss value = phi(0).
    pub(crate) phigrad: f64,  // Derivative = phi'(0) = p . ∇f(x).
}

// Pack search variables in various types depending on the search phase.
// Field names:
//    alpha = current tested step size.
//    step = alpha * p.
//    x_stepped = xs = x + step.
//    y_stepped = ys = phis = f(xs) = phi(alpha).
//    grad_stepped = gs = ∇f(xs).
//    phigrad_stepped = phigs = phi'(alpha) = p . ∇f(xs).

// Initial phase: evaluate phi(alpha), lowering alpha untill a finite value is found.
struct SearchFinite {
    alpha: f64,                    // To be evaluated.
    lowest_nonfinite: Option<f64>, // Update to the lowest 'h < a' found, if any.
}
// First iteration into the bracketing loop.
struct Bracketing {
    alpha: f64,
    sample: Sample,
    previous_step: Step,           // Initially set for alpha=0.
    lowest_nonfinite: Option<f64>, // Inherited/updated from previous state.
}
// The 'zoom' phase.
struct Zoom {
    lo: Step,
    hi: Step,
    last: LastStep, // Useful when terminating.
}

// Bundle the values obtained after 'sampling' one value of 'alpha'.
struct Sample {
    step: Tensor, // Intermediate computation = alpha * p.
    x: Tensor,    // The one gradient-tracked = x + step.
    y: Tensor,    // The one to invoke backpropagation on = f(xs) = phi(alpha).
    phi: f64,     // Local copy of the above.
}

// Bundle useful values from a 'previous' or 'current' search step.
struct Step {
    alpha: f64, // alpha
    phi: f64,   // phi(alpha)
    grad: f64,  // phi'(alpha)
}

impl<'s, F: Fn(&Tensor) -> Tensor> Location<'s, F> {
    pub(crate) fn new(f: &'s F, p: &'s Tensor, x: &'s Tensor, y: f64, grad: &'s Tensor) -> Self {
        Self { f, p, x, y, phigrad: p.dot(grad).to_double() }
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
enum BinaryStepDirection {
    Up,
    Down,
}

impl<F: Fn(&Tensor) -> Tensor> Search<'_, '_, F> {
    // Specify Wolfe criteria, assuming loss is finite.

    // Armijo's rule: check that the current step candidate
    // would "decrease f sufficiently".
    fn sufficient_decrease(&self, alpha: f64, phi: f64) -> bool {
        let &Self {
            loc: Location { y: phi_zero, phigrad: phigrad_zero, .. },
            cf: Config { c1, .. },
            ..
        } = self;
        phi <= phi_zero + c1 * alpha * phigrad_zero
    }

    // Strong curvature condition: check that the current step candidate
    // would "reduce slope sufficiently".
    fn weak_slope(&self, phigrad: f64) -> bool {
        let Self {
            loc: Location { phigrad: phigrad_zero, .. },
            cf: Config { c2, .. },
            ..
        } = self;
        phigrad.abs() <= c2 * phigrad_zero.abs()
    }

    fn run_search(mut self) -> Result<Summary, Error> {
        use ControlFlow as C;

        let bracket = self.search_first_finite(&SearchFinite {
            alpha: self.cf.init_step,
            lowest_nonfinite: None,
        })?;

        match self.bracket(bracket)? {
            C::Break(summary) => Ok(summary),
            C::Continue(zoom) => self.zoom(zoom),
        }
    }

    fn search_first_finite(&mut self, search: &SearchFinite) -> Result<Bracketing, Error> {
        use Error as E;
        let SearchFinite { mut alpha, mut lowest_nonfinite } = search;
        loop {
            match self.sample(alpha) {
                Err(E::NonFiniteLoss { loss, vars }) => {
                    // Try again with a lower value for alpha.
                    self.log(alpha, loss, None, &vars, None)?;
                    let lower = alpha * self.cf.step_decrease;
                    ensure!(
                        lower > 0.,
                        NoStepSizeYieldingFiniteLossErr {
                            x: self.loc.x.detach().copy(),
                            p: self.loc.p.detach().copy(),
                        }
                    );
                    lowest_nonfinite = Some(alpha);
                    alpha = lower;
                    continue;
                }
                Ok(sample) => {
                    return Ok(Bracketing {
                        alpha,
                        sample,
                        previous_step: Step {
                            alpha: 0.0,
                            phi: self.loc.y,
                            grad: self.loc.phigrad,
                        },
                        lowest_nonfinite,
                    })
                }
                Err(e) => return Err(e),
            }
        }
    }

    fn bracket(&mut self, mut bracket: Bracketing) -> Result<ControlFlow<Summary, Zoom>, Error> {
        use BinaryStepDirection as D;
        use ControlFlow as C;
        use Error as E;
        let mut first = true;
        'bracketing: loop {
            let Bracketing {
                alpha,
                sample: Sample { step, x, y, phi },
                previous_step,
                lowest_nonfinite,
            } = bracket;
            let (grad, phigrad) = self.differentiate(&x, &y, phi)?;
            let this_step = || Step { alpha, phi, grad: phigrad };
            self.log(alpha, phi, Some(phigrad), &x, Some(&grad))?;
            let last = || Self::last(alpha, step, &x, phi, &grad);

            if !self.sufficient_decrease(alpha, phi) || (!first && phi >= previous_step.phi) {
                return Ok(C::Continue(Zoom {
                    lo: previous_step,
                    hi: this_step(),
                    last: last(),
                }));
            }
            if self.weak_slope(phigrad) {
                return Ok(C::Break(self.summary(last())));
            }
            if phigrad >= 0.0 {
                return Ok(C::Continue(Zoom {
                    lo: this_step(),
                    hi: previous_step,
                    last: last(),
                }));
            }
            // Unfruitful: pick a larger candidate.
            let over_horizon = if let Some(ln) = lowest_nonfinite {
                ln
            } else {
                // No non-finite loss has been observed yet: increase candidate naively.
                let new = alpha * self.cf.step_increase;
                match self.sample(new) {
                    Ok(sample) => {
                        // Success: keep bracketing.
                        first = false;
                        bracket = Bracketing {
                            alpha: new,
                            sample,
                            previous_step: this_step(),
                            lowest_nonfinite,
                        };
                        continue 'bracketing;
                    }
                    // Or a new candidate above horizon has been found.
                    Err(E::NonFiniteLoss { loss, vars }) => {
                        self.log(new, loss, None, &vars, None)?;
                        new
                    }
                    Err(e) => return Err(e),
                }
            };
            // Watch the horizon to not pick a candidate yielding non-finite loss.
            // Bisect up first, then bisect down again if we overshoot.
            let lo = alpha; // The largest finite candidate found so far.
            let mut hi = over_horizon; // Reduce as we search.
            let mut dir = D::Up; // Flip if we overshoot.
            let mut current = alpha;
            'binsearch: loop {
                let new = lo + 0.5 * (hi - lo);
                // The binary search dies against floating point precision
                // if no progress has been made.
                ensure!(
                    if dir == D::Up {
                        current < new && new < hi
                    } else {
                        lo < new && new < current
                    },
                    DeadBinarySearchErr { alpha: new, summary: self.summary(last()) }
                );
                match self.sample(new) {
                    Ok(sample) => {
                        // Increase succeeded.
                        first = false;
                        bracket = Bracketing {
                            alpha: new,
                            sample,
                            previous_step,
                            lowest_nonfinite: Some(hi),
                        };
                        continue 'bracketing;
                    }
                    Err(E::NonFiniteLoss { loss, vars }) => {
                        // This candidate was an overshoot, bisect down from now on.
                        dir = D::Down;
                        hi = new;
                        current = new;
                        self.log(new, loss, None, &vars, None)?;
                        continue 'binsearch;
                    }
                    Err(e) => return Err(e),
                }
            }
        }
    }

    fn zoom(&mut self, zoom: Zoom) -> Result<Summary, Error> {
        let Zoom { mut lo, mut hi, mut last } = zoom;
        loop {
            // Cubic interpolation from eq (3.59).
            // The minimizer of the cubic is proven to exist within the interval
            // containing lo and hi, or it is either endpoint.
            let d1 = lo.grad + hi.grad - 3.0 * (lo.phi - hi.phi) / (lo.alpha - hi.alpha);
            let d2 = (hi.alpha - lo.alpha).signum() * (d1 * d1 - lo.grad * hi.grad).sqrt();
            let mut a = hi.alpha
                - (hi.alpha - lo.alpha) * (hi.grad + d2 - d1) / (hi.grad - lo.grad + 2.0 * d2);

            // Check that this did fall within the range.
            let (l, h) = (lo.alpha, hi.alpha);
            let (l, h) = if l < h { (l, h) } else { (h, l) };
            ensure!(l <= a && a <= h, BrokenZoomErr { alpha: a, lo: l, hi: h });

            // If it falls too close from a boundary, bisect instead.
            let d = f64::min(a - l, h - a);
            if d / (h - l) <= self.cf.bisection_threshold {
                a = l + 0.5 * (h - l);
            }
            ensure!(
                l < a && a < h,
                DeadZoomErr { alpha: a, summary: self.summary(last) }
            );

            // Evaluate.
            let alpha = a;
            let Sample { step, x, y, phi } = self.sample(alpha)?;
            let (ygrad, grad) = self.differentiate(&x, &y, phi)?;
            self.log(alpha, phi, Some(grad), &x, Some(&ygrad))?;
            last = Self::last(alpha, step, &x, phi, &ygrad);

            // Catch degeneration.
            #[allow(clippy::float_cmp)]
            if grad <= self.cf.flat_gradient
                && grad == hi.grad
                && grad == lo.grad
                && hi.phi == lo.phi
                && lo.phi == phi
            {
                return FlatZoomErr { alpha, grad, summary: self.summary(last) }.fail();
            }

            // Decide where to zoom next if necessary.
            if phi >= lo.phi || !self.sufficient_decrease(alpha, phi) {
                hi = Step { alpha, phi, grad };
                continue;
            }
            if self.weak_slope(grad) {
                return Ok(self.summary(last));
            }
            if grad * (hi.alpha - lo.alpha) >= 0.0 {
                hi = Step { ..lo };
            }
            lo = Step { alpha, phi, grad };
        }
    }

    // Sample the objective function at distance 'alpha' in direction 'p'.
    // Return all relevant tensors.
    // And in particular, the tensor on which to query the backprop
    // to possibly get 'phigrad_stepped' later.
    // Errors if the sampled loss is non-finite.
    fn sample(&mut self, alpha: f64) -> Result<Sample, Error> {
        let &mut Self { loc: Location { f, x: x_zero, p, .. }, .. } = self;

        let step = alpha * p;
        let x = (x_zero + &step).set_requires_grad(true);
        let y = f(&x);
        self.n_eval += 1;
        let phi = y.to_double();

        ensure!(
            phi.is_finite(),
            NonFiniteLossErr { loss: phi, vars: x.detach().copy() },
        );

        self.best.update(phi, &x);

        Ok(Sample { step, x, y, phi })
    }

    // Differentiate the objective function at distande 'alpha' in direction 'p',
    // assuming the object function has just been evaluated on x_stepped.
    // Return 'grad_stepped' and 'phigrad_stepped'.
    // Error if the gradient is non-finite.
    fn differentiate(
        &mut self,
        x: &Tensor,
        y: &Tensor,
        phi: f64, // Just to avoid extracting it again from y.
    ) -> Result<(Tensor, f64), Error> {
        let Self { loc: Location { p, .. }, .. } = self;
        y.backward();
        self.n_diff += 1;
        let grad = x.grad();
        ensure!(
            grad.is_all_finite(),
            NonFiniteGradErr {
                grad: grad.detach().copy(),
                vars: x.detach().copy(),
                loss: phi,
            }
        );
        let phigrad = p.dot(&grad).to_double(); // ( phi'(alpha) = p.∇f )
        Ok((grad, phigrad))
    }

    fn last(alpha: f64, step: Tensor, x: &Tensor, phi: f64, grad: &Tensor) -> LastStep {
        LastStep {
            step_size: alpha,
            step,
            vars_after_step: x.detach(), // Detach to not use it again in subsequent grad calc.
            loss_after_step: phi,
            grad_after_step: grad.detach(),
        }
    }

    fn summary(&self, last: LastStep) -> Summary {
        Summary {
            last_step: Some(last),
            n_eval: self.n_eval,
            n_diff: self.n_diff,
        }
    }

    // Log one line on every candidate evaluated,
    // typically before getting ready to either change it or yield it.
    fn log(
        &mut self,
        alpha: f64,
        phi: f64,              // = phi(alpha)
        phigs: Option<f64>,    // = phi'(alpha)
        x: &Tensor,            // short for xs = x + alpha * p
        grad: Option<&Tensor>, // = ∇f(xs)
    ) -> Result<(), Error> {
        let Some(&mut TraceFile { path, ref mut file }) = self.trace else {
            return Ok(());
        };
        let mut write = || -> Result<(), std::io::Error> {
            macro_rules! w {
                ($fmt:literal, $value:expr) => {
                    write!(file, $fmt, $value)?;
                };
                ($fmt:literal if $opt:expr; $alt:literal) => {
                    if let Some(value) = $opt {
                        write!(file, $fmt, value)?;
                    } else {
                        write!(file, $alt)?;
                    }
                };
            }
            w!("{}", self.step_id);
            w!(",{}", alpha);
            w!(",{}", phi);
            w!(",{}" if phigs; ",");
            for t in [Some(x), grad] {
                if let Some(t) = t {
                    for f in t.iter::<f64>().unwrap() {
                        w!(",{}", f);
                    }
                } else {
                    for _ in 0..self.n_vars {
                        write!(file, ",")?;
                    }
                }
            }
            writeln!(file)
        };
        Ok(write().with_context(|_| io::AtErr { ctx: "writing to trace file", path })?)
    }
}

pub(crate) fn write_header(
    trace: &mut TraceFile,
    varnames: Result<&Vec<String>, usize>, // Get at least number if there are no names.
) -> Result<(), Error> {
    let mut header = ["id", "alpha", "loss", "phigrad"]
        .into_iter()
        .map(ToString::to_string)
        .collect::<Vec<_>>();
    for vec in ["var", "grad"] {
        match varnames {
            Ok(names) => {
                for name in names {
                    header.push(format!("{vec}_{name}"));
                }
            }
            Err(n_vars) => {
                for i in 0..n_vars {
                    header.push(format!("{vec}_{i}"));
                }
            }
        }
    }
    writeln!(trace.file, "{}", header.join(","))
        .with_context(|_| io::AtErr { ctx: "writing trace header", path: trace.path })?;
    Ok(())
}

// Perform the seach.
// Return step size and step vector meeting the criteria
// along with the evaluation + gradient value at the point stepped to
// and the number of evaluations/differentiations used to find the result.
// Don't return values at the point stepped to
// if the best step size found is null.
pub(crate) fn search<'s, 'c: 's, F: Fn(&Tensor) -> Tensor>(
    loc: Location<'s, F>,
    cf: &'c Config,
    best: &'s mut Best,
    (run_id, trace, step_id): (Option<&'s str>, Option<&'s mut TraceFile<'c>>, u64),
) -> Result<Summary, Error> {
    use Error as E;
    // Initialize.
    let search = Search {
        n_vars: loc.p.size1().unwrap().try_into().unwrap(),
        loc,
        best,
        cf,
        n_eval: 0,
        n_diff: 0,
        // Not yet calculated.
        trace,
        step_id,
    };
    let prefix = LazyCell::new(|| {
        if let Some(id) = run_id {
            cformat!("<s>{id}::(Wolfe)</> ")
        } else {
            String::new()
        }
    });
    match search.run_search() {
        Err(E::DeadBinarySearch { alpha, summary } | E::DeadZoom { alpha, summary }) => {
            if alpha <= 0. {
                cprintln!("<k>{}Null step size reached: {alpha}.</>", *prefix);
                Ok(summary)
            } else {
                cprintln!(
                    "<k>{}(step {step_id}) Wolfe range too small for ulps around {alpha}?</>",
                    *prefix
                );
                Ok(summary)
            }
        }
        Err(e @ E::FlatZoom { .. }) => {
            cprintln!("<k>{}{e}</>", *prefix);
            let E::FlatZoom { summary, .. } = e else {
                unreachable!() // (just so we can both print the error *and* extract summary)
            };
            Ok(summary)
        }
        res => res,
    }
}

#[derive(Debug, Serialize)]
pub struct Summary {
    pub(crate) last_step: Option<LastStep>,
    pub(crate) n_eval: u64,
    pub(crate) n_diff: u64,
}

#[derive(Debug, Serialize)]
pub(crate) struct LastStep {
    pub(crate) step_size: f64,
    #[serde(serialize_with = "tensor::ser")]
    pub(crate) step: Tensor,
    #[serde(serialize_with = "tensor::ser")]
    pub(crate) vars_after_step: Tensor,
    pub(crate) loss_after_step: f64,
    #[serde(serialize_with = "tensor::ser")]
    pub(crate) grad_after_step: Tensor,
}

#[derive(Debug, Snafu, Serialize)]
#[snafu(context(suffix(Err)))]
pub enum Error {
    #[snafu(display(
        "Could not find a step size yielding a finite loss value, \
         starting from point:\n{x}\nin direction:\n{p}"
    ))]
    NoStepSizeYieldingFiniteLoss {
        #[serde(serialize_with = "tensor::ser")]
        x: Tensor,
        #[serde(serialize_with = "tensor::ser")]
        p: Tensor,
    },
    #[snafu(display(
        "Could not find a step size meeting Wolfe lower bound, \
         starting from point:\n{x}\nin direction:\n{p}"
    ))]
    NoStepLargeEnough {
        #[serde(serialize_with = "tensor::ser")]
        x: Tensor,
        #[serde(serialize_with = "tensor::ser")]
        p: Tensor,
    },
    #[snafu(display("Obtained non-finite loss ({loss}) with these variables:\n{vars:?}"))]
    NonFiniteLoss {
        loss: f64,
        #[serde(serialize_with = "tensor::ser")]
        vars: Tensor,
    },
    #[snafu(display(
        "Obtained non-finite gradient for loss ({loss}):\ngradient: {grad:?}\nvariables: {vars:?}"
    ))]
    NonFiniteGrad {
        #[serde(serialize_with = "tensor::ser")]
        grad: Tensor,
        #[serde(serialize_with = "tensor::ser")]
        vars: Tensor,
        loss: f64,
    },
    #[snafu(display("Binary search could not reduce the search range further around {alpha}."))]
    DeadBinarySearch { alpha: f64, summary: Summary },
    #[snafu(display(
        "Cubic interpolation yielded a minimizer outside the zooming range:\
         (lo): {lo} :: {alpha} :: {hi} (hi)"
    ))]
    BrokenZoom { alpha: f64, hi: f64, lo: f64 },
    #[snafu(display("Zoom phase could not reduce the search range further around {alpha}."))]
    DeadZoom { alpha: f64, summary: Summary },
    #[snafu(display(
        "The zoom step length interval has possibly become \
         numerically flat around {alpha} (gradient: {grad:e})."
    ))]
    FlatZoom {
        alpha: f64,
        grad: f64,
        summary: Summary,
    },
    #[snafu(transparent)]
    Io { source: io::Error },
}
