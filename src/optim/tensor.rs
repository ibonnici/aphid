// Convenience trait for tensors, as used in this optimisation module.

use serde::{ser::SerializeSeq, Serializer};
use tch::Tensor;

//--------------------------------------------------------------------------------------------------
// Pulls value back from device.

pub(crate) trait OptimTensor {
    fn to_double(&self) -> f64;
    fn is_all_finite(&self) -> bool;
}

impl OptimTensor for Tensor {
    fn to_double(&self) -> f64 {
        self.double_value(&[])
    }
    fn is_all_finite(&self) -> bool {
        self.isfinite().all().to_double() > 0.5
    }
}

//--------------------------------------------------------------------------------------------------
// Debug logging features, formatting values so they (mostly) fit into columns.

pub(super) trait Loggable {
    fn format(&self) -> String;
}

impl Loggable for f64 {
    fn format(&self) -> String {
        let prec = 16; // Large to get fine precision displayed.
        let mut width = 23; // Large to have logs correctly align.
        let leading_space = if *self >= 0. {
            " "
        } else {
            width += 1;
            ""
        };
        format!("{leading_space}{self:<width$.prec$e}")
    }
}

impl Loggable for u64 {
    fn format(&self) -> String {
        let width = 23; // For consistency with floats.
        format!(" {self:<width$}")
    }
}

impl Loggable for Option<f64> {
    fn format(&self) -> String {
        let width = 23;
        if let Some(v) = self {
            v.format()
        } else {
            format!("{:<width$}", "")
        }
    }
}

impl Loggable for Tensor {
    fn format(&self) -> String {
        let mut res = String::new();
        res.push('[');
        let mut it = self.iter::<f64>().unwrap();
        if let Some(mut i) = it.next() {
            loop {
                res.push_str(&i.format());
                if let Some(n) = it.next() {
                    res.push_str(", ");
                    i = n;
                } else {
                    break;
                }
            }
        }
        res.push(']');
        res
    }
}

//--------------------------------------------------------------------------------------------------
// Serde.

pub(crate) fn ser<S>(t: &Tensor, s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    assert!(
        t.size().len() == 1,
        "Only 1-dimensional tensors can be serialized yet."
    );
    let mut seq = s.serialize_seq(Some(
        usize::try_from(t.size()[0]).expect("Serializing negative tensor size?"),
    ))?;
    for ref e in t.iter::<f64>().unwrap() {
        seq.serialize_element(e)?;
    }
    seq.end()
}
