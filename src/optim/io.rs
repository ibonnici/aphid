//! Anything io-related, but contained within this autonomous 'optim' module.
use std::{
    fs::File,
    path::{Path, PathBuf},
};

use serde::Serialize;
use snafu::{ResultExt, Snafu};

/// Pack open file and its path together,
/// useful for trace files.
pub(crate) struct TraceFile<'p> {
    pub(crate) path: &'p Path,
    pub(crate) file: File,
}

impl<'p> TraceFile<'p> {
    pub(crate) fn new(path: &'p Path) -> Result<Self, Error> {
        Ok(Self {
            file: File::create(path).with_context(|_| AtErr { ctx: "creating file", path })?,
            path,
        })
    }
}

#[derive(Debug, Snafu, Serialize)]
#[snafu(context(suffix(Err)))]
pub enum Error {
    #[snafu(display("Error while {ctx} at {}: {source}", path.display()))]
    #[snafu(visibility(pub(crate)))]
    At {
        ctx: String,
        path: PathBuf,
        #[serde(serialize_with = "crate::output::ser_display")]
        source: std::io::Error,
    },
}
