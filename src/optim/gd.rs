// Implementation of simple, naive gradient descent.
// Keeps a history of the last few loss values visited
// to return only the best one found so far (latest in case of ex-aequo).
// Stops on its own when the mean slope of the history
// becomes flat, provided the history is full.

use color_print::cprintln;
use serde::Serialize;
use snafu::Snafu;
use tch::Tensor;

use crate::optim::{
    simple_optim_result_impl, tensor, Best, Error as OptimError, Loggable, OptimTensor,
    SlopeTracker, SlopeTrackingConfig,
};

#[derive(Debug, Serialize)]
pub(crate) struct Config {
    pub(crate) max_iter: u64,
    pub(crate) step_size: f64, // Above 0.
    pub(crate) slope_tracking: Option<SlopeTrackingConfig>,
}

#[derive(Debug)]
pub(crate) struct GdResult {
    loss: f64,
    vars: Tensor,
    n_eval: u64,
    n_diff: u64,
}
simple_optim_result_impl!(GdResult);

impl Config {
    pub fn minimize(
        &self,
        fun: impl Fn(&Tensor) -> Tensor,
        init: &Tensor,
        log: u64,
    ) -> Result<GdResult, OptimError> {
        let &Self { max_iter, step_size, ref slope_tracking } = self;
        let kindev = (init.kind(), init.device());
        let mut n_eval = 0;
        let mut n_diff = 0;

        // Factorize common checks.
        macro_rules! check_finite_loss {
            ($loss:ident, $vars:ident, $step:expr) => {
                snafu::ensure!(
                    $loss.is_finite(),
                    NonFiniteLossErr {
                        loss: $loss,
                        vars: $vars.detach().copy(),
                        step: $step
                    }
                );
            };
        }
        macro_rules! check_finite_grad {
            ($grad:ident, $loss:ident, $vars:ident, $step:expr) => {
                snafu::ensure!(
                    $grad.is_all_finite(),
                    NonFiniteGradErr {
                        grad: $grad.copy(),
                        loss: $loss,
                        vars: $vars.detach().copy(),
                        step: $step,
                    }
                );
            };
        }

        // Evaluate initial loss value.
        let mut x = init.set_requires_grad(true);
        x.zero_grad();
        let mut y_t = fun(&x);
        let mut y = y_t.to_double();
        n_eval += 1;
        check_finite_loss!(y, x, 0u64);

        // Keep track of the best (loss, parameters) pair found so far.
        let mut best = Best::new(y_t.to_double(), init);

        // Initialize slope tracking if desired.
        let mut slope_tracker = slope_tracking.as_ref().map(|s| {
            let mut t = SlopeTracker::new(s, kindev);
            assert!(t.low_slope(y, 0).is_none());
            t
        });

        // Evaluate intial gradient value.
        y_t.backward();
        let mut grad = x.grad().copy();
        n_diff += 1;
        check_finite_grad!(grad, y, x, 0u64);

        if max_iter == 0 {
            cprintln!("<k>No optimisation step asked for.</>");
            return Ok(GdResult { vars: best.vars, loss: best.loss, n_eval, n_diff });
        }

        let mut n_steps = 0;
        loop {
            // Update.
            let step = -step_size * &grad;
            tch::no_grad(|| {
                let _ = x.g_add_(&step);
            });

            // Re-evaluate loss in new location.
            x.zero_grad();
            y_t = fun(&x);
            y = y_t.to_double();
            n_eval += 1;
            check_finite_loss!(y, x, n_steps);
            best.update(y, &x);

            // Check slope.
            if let Some(ref mut tracker) = slope_tracker {
                if let Some(lowslope) = tracker.low_slope(y, n_steps) {
                    cprintln!(
                        "<k>Weak loss slope ({lowslope:e}) on iteration {n_steps}: stopping.</>"
                    );
                    break Ok(GdResult { loss: best.loss, vars: best.vars, n_eval, n_diff });
                }
            }

            // Re-evaluate gradient in new location.
            y_t.backward();
            grad = x.grad().copy();
            n_diff += 1;
            check_finite_grad!(grad, y, x, n_steps);

            if log > 0 && n_steps % log == 0 {
                println!("step {n_steps:<3} y: {} x: {}", y.format(), x.format());
                println!("            {} x': {}", None.format(), grad.format());
            }

            // Move on to next step.
            n_steps += 1;
            if n_steps >= max_iter {
                if let Some(slope_threshold) = slope_tracking.as_ref().map(|s| s.threshold) {
                    cprintln!(
                        "<k>Max iteration reached ({n_steps}) \
                         without finding a loss slope magnitude \
                         lower than {slope_threshold:e}.</>"
                    );
                } else {
                    cprintln!("<k>Max iteration reached ({n_steps}).</>");
                }
                break Ok(GdResult { loss: best.loss, vars: best.vars, n_eval, n_diff });
            }
        }
    }
}

#[derive(Debug, Serialize, Snafu)]
#[snafu(context(suffix(Err)))]
pub enum Error {
    #[snafu(display(
        "Obtained non-finite loss ({loss}) \
         with these variables on step {step}:\n{vars:?}"
    ))]
    NonFiniteLoss {
        loss: f64,
        #[serde(serialize_with = "tensor::ser")]
        vars: Tensor,
        step: u64,
    },
    #[snafu(display(
        "Obtained non-finite gradient for loss ({loss}) on step {step}:\
         \ngradient: {grad:?}\nvariables: {vars:?}"
    ))]
    NonFiniteGrad {
        #[serde(serialize_with = "tensor::ser")]
        grad: Tensor,
        #[serde(serialize_with = "tensor::ser")]
        vars: Tensor,
        loss: f64,
        step: u64,
    },
}
