// Specify the content of the config file,
// and its representation within the program.
// Use TOML format for the file.
//
// The "raw" config data matches the content of the file.
// Once parsed, it is checked for consistency
// and transformed into the actual, useful config data.

use std::path::PathBuf;

use serde::Serialize;

use crate::{
    gene_tree::MeanNbBases, interner::SpeciesSymbol, optim::bfgs::Config as BfgsConfig,
    BranchLength, Parameters,
};

pub mod defaults;
pub mod expand;
pub mod raw;

/// The final configuration value handed out to user.
///
/// Contains all the information required to specify
/// how the program is supposed to run.
/// Refer to the documentation for the detailed
/// meaning of the various options.
pub struct Config {
    // Path to the gene trees data file.
    pub trees: PathBuf,

    // Names of the species of interest.
    pub taxa: Taxa,

    // Parametrize the filters.
    pub filters: Filters,

    pub unresolved_length: Option<MeanNbBases>,

    // Parametrize the searches.
    pub searches: Vec<Search>,
}
pub const MAX_UNRESOLVED_LENGTH: f64 = 0.5;

#[derive(Debug, Serialize)]
pub struct Filters {
    pub triplet_other_monophyly: bool,
    pub max_clock_ratio: Option<BranchLength>,
}

// Contains only non-duplicated interned strings.
// Every section contains distinct species.
pub struct Taxa {
    pub triplet: SpeciesTriplet,
    pub outgroup: Vec<SpeciesSymbol>,
    pub other: Vec<SpeciesSymbol>,
}

// The triplet is importantly *oriented*: ((A, B), C).
// In a way that corresponds to user input topology:
//    triplet = "((U, V), W)" # A: U, B: V, C: W
//    triplet = "(U, (V, W))" # A: V, B: W, C: U
pub struct SpeciesTriplet {
    pub a: SpeciesSymbol,
    pub b: SpeciesSymbol,
    pub c: SpeciesSymbol,
}

#[derive(Debug, Serialize)]
pub struct Search {
    // Starting point in the search space.
    // Missing values will be inferred from aphid init heuristic.
    // Raise associated flag to force-fix parameters.
    pub init_parms: Parameters<ParmSpec>,

    // (starting heuristics config:
    //  initial simple gradient descent + data reduction used to be here)

    // Main learning phase configuration.
    pub(crate) bfgs: BfgsConfig,
}

#[derive(Debug, Serialize)]
pub struct ParmSpec {
    // Don't specify to use aphid's default starting point.
    pub value: Option<f64>,
    // Raise if force-fixed.
    pub fixed: bool,
    #[serde(skip_serializing)]
    pub index: Option<usize>, // Useful for reporting (lower if scalar).
}
