// Attempt to leverage `rustdoc_types` crate
// to extract information from this project's sources
// and have them automatically inserted within the documentation mdbook.

use std::{collections::HashMap, fs, path::PathBuf, process};

use aphid::NAME;
use clap::{Parser, ValueEnum};
use paste::paste;
use regex::Regex;
use rustdoc_types::{Crate, Id, Item, ItemEnum, Module, StructKind};
use snafu::{ensure, OptionExt, ResultExt, Snafu};

const ROOT: &str = env!("CARGO_MANIFEST_DIR");

/// Extract pieces of information from the json output of rustdoc.
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// What to retrieve.
    #[clap(long, short='x', value_enum, default_value_t = Extract::Doc)]
    extract: Extract,

    /// Path to the item.
    path: String,
}

#[derive(ValueEnum, Clone, Debug)]
enum Extract {
    /// Extract item docstring.
    Doc,
    /// Extract constant value.
    Value,
}

fn main() {
    if let Err(e) = run() {
        // Considering that the following is unsolved.
        //   https://github.com/FauconFan/mdbook-cmdrun/issues/18
        // Display instead something LARGE to not miss it when parsing the generated docs.
        println!("!!!!!!! DOC EXTRACTION ERROR !!!!!!!!!  ");
        println!("{e}  ");
        println!("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  ");
        process::exit(1);
    }
}

macro_rules! errout {
    ($errname:ident, $mess:tt) => {{
        paste! {
            return [<$errname Err>] {e : format!$mess }.fail();
        }
    }};
}

// Extract string from the docs.
fn run() -> Result<(), Error> {
    let args = Args::parse();

    // Find the documentation file (assuming it has been produced by rustdoc).
    let mut file = PathBuf::from(ROOT);
    file.push("target");
    file.push("doc");
    file.push(format!("{NAME}.json"));

    // Read and parse.
    let json = fs::read_to_string(&file).with_context(|_| IoErr { file })?;
    let aphid: Crate = serde_json::from_str(&json).with_context(|_| ParseErr)?;
    // Now the whole crate information is available as 'aphid'.

    // Extract root module.
    let index = &aphid.index;
    let root = index
        .get(&aphid.root)
        .with_context(|| InconsistentErr { e: "unindexed root?" })?;
    ensure!(
        root.name == Some(NAME.into()),
        InconsistentErr { e: "root item name differs from crate name?" }
    );
    let ItemEnum::Module(root) = &root.inner else {
        return InconsistentErr { e: "root item is not a module? " }.fail();
    };

    let res = extract(&args.extract, &args.path, root, index)?;
    println!("{res}");

    Ok(())
}

fn extract(
    typ: &Extract,
    raw_path: &str,
    module: &Module,
    index: &HashMap<Id, Item>,
) -> Result<String, Error> {
    use ItemEnum as I;

    // Fetch item through modules first.
    let (item, path) = fetch(raw_path, module, index)?;

    let item = if let Some(path) = path {
        // Need to go fetch futher within the item.
        match &item.inner {
            I::Struct(str) => 's: {
                // The final path bit must be a field name.
                let StructKind::Plain { fields, .. } = &str.kind else {
                    errout!(NotFound, ("{raw_path:?} within {str:#?}."));
                };
                // Linear-search for it.
                for i in fields {
                    let field = index.get(i).unwrap();
                    let Some(name) = &field.name else {
                        continue;
                    };
                    if name != path {
                        continue;
                    }
                    break 's field;
                }
                errout!(
                    NotFound,
                    ("field named {path:?} within struct {raw_path:?}.")
                );
            }
            _ => {
                errout!(Unimplemented, ("fetch further within {item:#?}."))
            }
        }
    } else {
        item
    };

    let extract = match typ {
        Extract::Doc => {
            let Some(doc) = &item.docs else {
                errout!(NotFound, ("documentation for {raw_path}"));
            };
            doc.to_string()
        }
        Extract::Value => extract_value(item)?,
    };

    post_process(&extract, module, index)
}

fn extract_value(item: &Item) -> Result<String, Error> {
    let ItemEnum::Constant { const_: cst, .. } = &item.inner else {
        errout!(Unimplemented, ("extract a 'value' from {item:#?}."));
    };
    Ok(cst.expr.to_string())
}

fn post_process(input: &str, module: &Module, index: &HashMap<Id, Item>) -> Result<String, Error> {
    // Interpolate constant values within the resulting string.
    let re = Regex::new(r"\[`([a-zA-Z_0-9:]*?)`\]").unwrap();
    let mut res = String::new();
    let mut remaining = input;
    while let Some(c) = re.captures(remaining) {
        let whole = c.get(0).unwrap();
        let path = c.get(1).unwrap().as_str();
        let val = extract(&Extract::Value, path, module, index)?;
        let (before, _) = remaining.split_at(whole.start());
        let (_, rest) = remaining.split_at(whole.end());
        res.push_str(before);
        res.push_str(&val);
        remaining = rest;
    }
    res.push_str(remaining);
    Ok(res)
}

// Consume the given path to fetch the target item
// until either the path is consumed or the item found is not a module.
fn fetch<'p, 'm>(
    path: &'p str,
    module: &'m Module,
    index: &'m HashMap<Id, Item>,
) -> Result<(&'m Item, Option<&'p str>), Error> {
    // Extract next step from the path, using either possible separators.
    let (step, rest) =
        if let Some((a, b)) = path.split_once("::") { (a, Some(b)) } else { (path, None) };

    // Linear-search for the item name.
    for i in &module.items {
        let sub_item = &index.get(i).unwrap();
        let Some(name) = &sub_item.name else { continue };
        if name != step {
            continue;
        }

        let Some(rest) = rest else {
            // Path consumed.
            return Ok((sub_item, None));
        };

        let ItemEnum::Module(submodule) = &sub_item.inner else {
            // Not a module anymore.
            return Ok((sub_item, Some(rest)));
        };

        // Recurse into submodule.
        return fetch(rest, submodule, index);
    }

    errout!(NotFound, ("{step:?} within module."));
}

#[derive(Debug, Snafu)]
#[snafu(context(suffix(Err)))]
enum Error {
    #[snafu(display("IO error with file {:?}: {source}", file.display()))]
    Io {
        file: PathBuf,
        source: std::io::Error,
    },
    #[snafu(display("Parse error: {source}"))]
    Parse { source: serde_json::Error },
    #[snafu(display("Inconsistent program behaviour: {e}"))]
    Inconsistent { e: String },
    #[snafu(display("Not found: {e}"))]
    NotFound { e: String },
    #[snafu(display("Unimplemented: {e}"))]
    Unimplemented { e: String },
}
