use std::path::PathBuf;

use clap::Parser;

/// Aphid: distinguishing gene flow from incomplete lineage sorting.
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub(crate) struct Args {
    /// Path to the config file.
    pub(crate) config: PathBuf,

    /// Path to the output folder, created if missing.
    pub(crate) output: PathBuf,

    /// Raise to overwrite previous output folder.
    #[clap(short, long)]
    pub(crate) force: bool,
}
