use std::{
    cell::LazyCell,
    collections::HashSet,
    fs::{self},
    io::Write as IoWrite,
    path::{self, Path, PathBuf},
    process,
    time::Instant,
};

use aphid::{
    config::{
        expand::{self, Index},
        ParmSpec,
    },
    extract_local_triplet, imbalance,
    interner::{Interner, ResolvedSymbol, SpeciesSymbol},
    io::{self, ToRelative},
    it_mean::SummedMean,
    learn::heuristic_starting_point,
    optimize_likelihood,
    output::{self, detail, file},
    Config, GeneTree, GeneTriplet, GenesForest, LocalGeneTriplet, Parameters, VERSION,
};
use clap::Parser;
use color_print::{ceprintln, cformat, cprintln};
use float_eq::float_eq;
use serde_json::json;
use snafu::{ensure, ResultExt, Snafu};

mod args;

// Run and terminate error bubbling if any.
fn main() {
    let tick = Instant::now();
    match run() {
        Ok(()) => {
            let duration = tick.elapsed().as_secs_f64();
            cprintln!("Done in <s>{duration:.03} s</>. <bold,green>✓</>");
        }
        Err(e) => {
            ceprintln!("<bold,red>🗙 Error:</> {e}");
            process::exit(1);
        }
    }
}

fn run() -> Result<(), Error> {
    // Parse command-line arguments.
    let args = args::Args::parse();
    println!("Running Aphid v{VERSION}.");

    // Initialize string interning optimisation.
    let mut interner = Interner::new();
    let interner = &mut interner;

    // Canonicalize files.
    let output = path::absolute(&args.output)
        .with_context(|_| io::AtErr { ctx: "searching absolute path", path: &args.output })?;

    // Read data from the various files.
    let (config, subfolders, forest) = read_inputs(&args, &output, interner)?;

    // Check overall consistency.
    // Extract the set of designated species = 'triplet' + 'outgroup' + 'other'.
    let designated_species = check_input_consistency(&forest, &config, interner)?;

    // Prepare output folder.
    prepare_output(&output, &subfolders, &args)?;

    // Output full configuration detail.
    write_config(&output, &config, interner)?;

    // Prepare output.
    let mut global = init_global(&forest);
    let mut details = init_detail(&forest, interner);

    // Remove all non-designated species from the gene trees.
    let pruned = prune(&forest, &mut details, &designated_species);

    // Topology-filtering pass.
    // Use this pass to calculate branches lengths means
    // and prepare the triplet information for later likelihood calculation.
    let (topology_included, (triplet_means_sum, rest_means_sum), local_triplets) =
        topology_filter(&pruned, &mut global, &mut details, &config, interner);

    // Geometry-filtering pass.
    let included = geometry_filter(
        &pruned,
        &mut global,
        &mut details,
        topology_included,
        (triplet_means_sum, rest_means_sum),
        &config,
    );

    display_summary(&included, &details, &config);

    // Extract the only information needed for likelihood calculation.
    let triplets = final_tree_selection(included, local_triplets, &mut global, &mut details);

    // Fit the model.
    let res = learn(&triplets, &mut global, &config, &mut details);

    // Output full per-tree detail *even* if learning failed.
    write_detail(&output, &details)?;
    write_csv(&output, &details)?;
    write_global(&output, &global)?;

    res
}

fn read_inputs(
    args: &args::Args,
    output_folder: &Path,
    interner: &mut Interner,
) -> Result<(Config, Vec<PathBuf>, GenesForest), Error> {
    let path = io::canonicalize(&args.config)?;
    cprintln!("Read config from <b>{}</>.", path.display());
    let (config, output_subfolders) = Config::from_file(&path, output_folder, interner)?;

    let path = &config.trees;
    cprintln!("Read gene trees from <b>{}</>:", path.display());
    let forest = GenesForest::from_file(path, interner)?;
    cprintln!("  Found <g>{}</> gene trees.", forest.len());

    Ok((config, output_subfolders, forest))
}

fn check_input_consistency(
    forest: &GenesForest,
    config: &Config,
    interner: &mut Interner,
) -> Result<HashSet<SpeciesSymbol>, Error> {
    println!("Check input consistency.");

    // Check that all designated species are actually found in the forest,
    // or they may be mispelled.
    let designated_species = config.designated_species().collect::<HashSet<_>>();
    let mut not_found = designated_species.clone();
    for gtree in forest.trees() {
        for sp in gtree.species_symbols() {
            not_found.remove(&sp);
        }
    }

    ensure!(
        not_found.is_empty(),
        InputConsistencyErr {
            mess: format!(
                "Species {1:?} cannot be found in input gene trees. {0} mispelled?",
                if not_found.len() == 1 { "Is it" } else { "Are they" },
                not_found
                    .iter()
                    .map(|&sp| ResolvedSymbol::new(sp, interner))
                    .collect::<Vec<_>>(),
            )
        }
    );

    Ok(designated_species)
}

fn prepare_output(output: &Path, subfolders: &[PathBuf], args: &args::Args) -> Result<(), Error> {
    println!("Prepare output folder:");

    match (output.exists(), args.force) {
        (true, true) => {
            cprintln!(
                "  <y>Replacing</> existing folder: <b>{}</>.",
                output.to_relative()?.display()
            );
            fs::remove_dir_all(output)
                .with_context(|_| io::AtErr { ctx: "removing folder", path: output })?;
        }
        (true, false) => OutputExistsErr { path: &output }.fail()?,
        (false, _) => {
            cprintln!("  Creating empty folder: <b>{}</>.", output.display());
        }
    };
    fs::create_dir_all(output)
        .with_context(|_| io::AtErr { ctx: "creating output folder", path: output })?;

    for path in subfolders {
        fs::create_dir_all(path)
            .with_context(|_| io::AtErr { ctx: "creating output subfolder", path })?;
    }

    Ok(())
}

fn write_config(output: &Path, config: &Config, interner: &Interner) -> Result<(), Error> {
    let path = output.join(file::CONFIG);
    cprintln!(
        "  Write full configuration to <b>{}</>.",
        path.to_relative()?.display()
    );
    io::write_file!(&path, "config", "{:#}", config.resolve(interner).json())?;
    Ok(())
}

fn init_global(forest: &GenesForest) -> output::Global {
    output::Global {
        n_trees: forest.len(),
        ..Default::default() // Fill further information later.
    }
}

fn init_detail<'i>(forest: &GenesForest, interner: &'i Interner) -> Vec<detail::Tree<'i>> {
    forest
        .ids()
        .iter()
        .zip(forest.trees().iter())
        .map(|(&id, tree)| detail::Tree {
            id: interner.resolve(id).unwrap(),
            n_bases: tree.sequence_length(),
            n_nodes_raw: tree.len(),
            ..Default::default() // Fill further information later.
        })
        .collect()
}

fn prune(
    forest: &GenesForest,
    details: &mut [detail::Tree],
    designated_species: &HashSet<SpeciesSymbol>,
) -> Vec<aphid::GeneTree> {
    println!("Prune trees so they only contain designated species.");
    forest
        .trees()
        .iter()
        .zip(details.iter_mut())
        .map(|(raw, detail)| {
            let pruned = raw.pruned(|_, leaf| designated_species.contains(&leaf.payload));
            detail.n_nodes_pruned = pruned.len();
            pruned
        })
        .collect()
}

fn topology_filter<'i>(
    pruned: &[GeneTree],
    global: &mut output::Global,
    details: &mut [detail::Tree<'i>],
    config: &Config,
    interner: &'i Interner,
) -> (Vec<usize>, (f64, f64), Vec<Option<aphid::LocalGeneTriplet>>) {
    println!("Filter trees based on topology:");
    let mut included = Vec::new();
    let mut n_excluded = 0;
    let mut n_unresolved = 0; // Not counting excluded ones.

    // Use this pass over trees to prepare next upcoming *geometry* filter.
    let mut triplet_means_sum = 0.;
    let mut rest_means_sum = 0.;

    let taxa = &config.taxa;
    let tom = config.filters.triplet_other_monophyly;
    let unrl = config.unresolved_length;

    let mut local_triplets = Vec::new();
    for (i, (tree, detail)) in pruned.iter().zip(details.iter_mut()).enumerate() {
        // Decide first filter based on topology.
        let topology = tree.topological_analysis(taxa);

        // Collect detail, accumulate global.
        let (triplet, outgroup, treetop) = detail::resolve_topology(&topology, taxa, tom, interner);
        global.n_excluded_triplets_topologies += usize::from(!triplet.included);
        global.n_excluded_outgroup_topologies += usize::from(!outgroup.included);
        global.n_excluded_treetops_topologies +=
            usize::from(!treetop.as_ref().is_none_or(|t| t.included));
        detail.triplet = triplet;
        detail.outgroup = outgroup;
        detail.top = treetop;

        // Synthetize triplet information for later likelihood calculation.
        let loc = extract_local_triplet(tree, &topology, &taxa.triplet, unrl);
        detail.triplet.analysis = loc.as_ref().map(detail::TripletAnalysis::new);

        if topology.is_included(tom) {
            let mean_length = tree.mean_branch_length(tree.root());
            let (mean_triplet, mean_rest) = tree.triplet_rest_mean_lengths(&topology);

            triplet_means_sum += mean_triplet;
            rest_means_sum += mean_rest;

            detail.topology_included = true;
            detail.mean_lengths = detail::MeanLengths {
                total: Some(mean_length),
                triplet: Some(mean_triplet),
                outgroup_other: Some(mean_rest),
            };

            n_unresolved += usize::from(!loc.as_ref().unwrap().resolved);
            included.push(i);
        } else {
            n_excluded += 1;
        }

        local_triplets.push(loc);
    }

    // Report.
    cprintln!(
        "  <g>{n_excluded}</> tree{s_were} excluded from analysis based on their topology.",
        s_were = if n_excluded > 1 { "s were" } else { " was" },
    );

    if let Some(min) = unrl {
        cprintln!(
            "  <g>{n_unresolved}</> included triplet{s_were} unresolved (internal length ⩽ {min}).",
            s_were = if n_unresolved > 1 { "s were" } else { " was" },
        );
    }

    // Record global information.
    #[allow(clippy::cast_precision_loss)]
    let nt = global.n_trees as f64;
    global.n_excluded_topologies = n_excluded;
    global.n_unresolved_triplets = n_unresolved;
    global.mean_length_triplet = triplet_means_sum / nt;
    global.mean_length_outgroup_other = rest_means_sum / nt;

    (
        included,
        (triplet_means_sum, rest_means_sum),
        local_triplets,
    )
}

fn geometry_filter(
    pruned: &[GeneTree],
    global: &mut output::Global,
    details: &mut [detail::Tree],
    topology_included: Vec<usize>,
    (triplet_means_sum, rest_means_sum): (f64, f64),
    config: &Config,
) -> Vec<usize> {
    println!("Filter trees based on geometry.");

    let (geometry_included, n_excluded) = if let Some(max) = config.filters.max_clock_ratio {
        let mut included = Vec::new();
        let mut n_excluded = 0;

        // Calculate global mean tree shape.
        let (triplet_longer, imbalance) = imbalance(triplet_means_sum, rest_means_sum);
        let (t, r) = ("'triplet' section", "'outgroup' and 'other' section");
        let (small, large) = if triplet_longer { (t, r) } else { (r, t) };
        cprintln!(
            "  Branch lengths in {large} are on average <g>{imbalance}</> times longer \
               than in {small}."
        );
        let global_shape = triplet_means_sum / rest_means_sum;
        cprintln!("  Mean tree shape: <g>{global_shape}</>");

        global.imbalance = Some(imbalance);
        global.triplet_longer = Some(triplet_longer);
        global.shape = Some(global_shape);

        for i in topology_included {
            let tree = &pruned[i];
            let detail = &mut details[i];

            let detail::MeanLengths {
                triplet: Some(mean_triplet),
                outgroup_other: Some(mean_rest),
                ..
            } = detail.mean_lengths
            else {
                unreachable!()
            };
            let (pass, shape_ratio) =
                tree.is_shape_included(mean_triplet, mean_rest, global_shape, max);
            detail.local_shape = Some(shape_ratio);
            if pass {
                detail.geometry_included = pass;
                included.push(i);
            } else {
                n_excluded += 1;
            }
        }

        (included, n_excluded)
    } else {
        (topology_included, 0)
    };

    // Report.
    if n_excluded > 0 {
        cprintln!(
            "  --> <g>{n_excluded}</> tree{s_were} excluded from analysis based on their geometry.",
            s_were = if n_excluded > 1 { "s were" } else { " was" },
        );
    } else {
        println!("  --> All trees kept.");
    }

    global.n_excluded_geometries = n_excluded;
    global.n_included_trees = geometry_included.len();

    geometry_included
}

fn final_tree_selection(
    included: Vec<usize>,
    mut local_triplets: Vec<Option<LocalGeneTriplet>>,
    global: &mut output::Global,
    details: &mut [detail::Tree],
) -> Vec<GeneTriplet> {
    println!("Estimate relative mutation rates.");
    let global_mean = included
        .iter()
        .map(|&i| details[i].mean_lengths.total.unwrap())
        .summed_mean::<usize>();
    cprintln!("  mean branch length accross trees: l = <g>{global_mean}</>");
    global.mean_branch_length = global_mean;

    let mut triplets = Vec::new();
    for i in included {
        let local = local_triplets[i].take().unwrap(); // Exists since included.
        let detail = &mut details[i];
        let local_mean = detail.mean_lengths.total.unwrap();
        let relative_mutation_rate = local_mean / global_mean;
        detail.mutation_rate = Some(relative_mutation_rate);
        triplets.push(GeneTriplet { local, relative_mutation_rate });
    }

    triplets
}

fn display_summary(included: &[usize], details: &[detail::Tree], config: &Config) {
    println!("\nSummary:");

    let s = |n| if n > 1 { "s" } else { "" };
    let n = details.iter().map(|d| u64::from(!d.triplet.included)).sum();
    cprintln!(
        "  - <g>{n}</> triplet{} rejected (incomplete or non-monophyletic).",
        s(n),
    );

    if let Some(min) = config.unresolved_length {
        let n = details
            .iter()
            .filter_map(|d| d.triplet.analysis.as_ref().map(|a| u64::from(!a.resolved)))
            .sum();
        cprintln!(
            "  - <g>{n}</> triplet{} considered unresolved (internal branch length ⩽ {min}).",
            s(n),
        );
    }

    let n = details
        .iter()
        .map(|d| u64::from(!d.outgroup.included))
        .sum();
    cprintln!(
        "  - <g>{n}</> outgroup{} rejected (empty or non-monophyletic).",
        s(n)
    );

    let n = details
        .iter()
        .map(|d| d.top.as_ref().map_or(1, |top| (!top.included).into()))
        .sum::<u64>();
    cprintln!(
        "  - <g>{n}</> tree top{} rejected (non-root LCA(triplet, outgroup){}).",
        s(n),
        if config.filters.triplet_other_monophyly {
            " or non-monophyletic (triplet, other)"
        } else {
            ""
        }
    );

    if let Some(max) = config.filters.max_clock_ratio {
        let n = details
            .iter()
            .map(|d| u64::from(!d.geometry_included))
            .sum();
        cprintln!(
            "  - <g>{n}</> tree shape{} rejected \
             (triplet/outgroup imbalance larger than {max} times the average).",
            s(n)
        );
    }

    let n = included.len() as u64;
    cprintln!("==> <s,g>{n}</> tree{} kept for analysis.", s(n));
}

#[allow(clippy::similar_names)] // (ab, ac, bc)
fn fill_init_parms(
    config: &Config,
    triplets: &[GeneTriplet],
) -> Result<Vec<Parameters<(f64, bool)>>, Error> {
    println!("Prepare starting points.");
    let mut res = Vec::new();
    let n_searches = config.searches.len();
    for (i, search) in config.searches.iter().enumerate() {
        let input = &search.init_parms;
        // Some parameters are reliably input.
        let here = |ps: &ParmSpec, name| {
            (
                ps.value.unwrap_or_else(|| {
                    panic!(
                        "Input {} cannot contain missing {name}.",
                        if n_searches > 1 { format!("[{i}]") } else { String::new() }
                    )
                }),
                ps.fixed,
                ps.index,
            )
        };
        let theta = here(&input.theta, "theta");
        let gf_times = input.gf_times.map(|gft| here(gft, "GF time"));

        // Others may need to be calculated from the default.
        let default = LazyCell::new(|| {
            heuristic_starting_point(triplets, theta.0, &gf_times.map(|(t, _, _)| *t))
        });
        let tau_1 = unwrap_or_default(&input.tau_1, || default.tau_1);
        let tau_2 = unwrap_or_default(&input.tau_2, || default.tau_2);
        let p_ab = unwrap_or_default(&input.p_ab, || default.p_ab);
        let p_ac = unwrap_or_default(&input.p_ac, || default.p_ac);
        let p_bc = unwrap_or_default(&input.p_bc, || default.p_bc);
        let p_ancient = unwrap_or_default(&input.p_ancient, || default.p_ancient);

        // Late transversal checks.
        let indexed = |(v, _, i): (f64, _, _)| Some((Index::auto(i), v));
        expand::tau_values(&indexed(tau_1), &indexed(tau_2))?;
        expand::probability_sums([&indexed(p_ab), &indexed(p_ac), &indexed(p_bc)])?;

        res.push(
            Parameters {
                theta,
                tau_1,
                tau_2,
                p_ab,
                p_ac,
                p_bc,
                p_ancient,
                gf_times,
            }
            .map(|&(val, fixed, _)| (val, fixed)),
        );
    }
    Ok(res)
}
fn unwrap_or_default(ps: &ParmSpec, default: impl Fn() -> f64) -> (f64, bool, Option<usize>) {
    let ParmSpec { value, fixed, index } = ps;
    (
        value.unwrap_or_else(default),
        *fixed,
        index.as_ref().copied(),
    )
}

fn learn(
    triplets: &[GeneTriplet],
    global: &mut output::Global,
    config: &Config,
    detail: &mut [detail::Tree],
) -> Result<(), Error> {
    println!("\nLearn:");

    // Spin several parallell searches.
    let parms = fill_init_parms(config, triplets)?;
    let results = optimize_likelihood(triplets, &parms, &config.searches)?;

    println!("\nPick best result among heuristics:");
    let best = results
        .into_iter()
        .filter_map(Result::ok)
        .max_by(|a, b| a.ln_likelihood.total_cmp(&b.ln_likelihood))
        .unwrap();
    cprintln!("Optimized ln-likelihood: <g>{}</>", best.ln_likelihood);
    println!("Optimized parameters: {}\n", best.parameters.colored());

    // Recalculate ln-likelihood per tree for the detailed output.
    let mut detail = detail.iter_mut().filter(|gt| gt.geometry_included);
    let mut sum = 0.;
    for gtrip in triplets {
        let lnl = gtrip.likelihood(&best.parameters).ln();
        sum += lnl;
        detail.next().unwrap().ln_likelihood = Some(lnl);
    }
    assert!(detail.next().is_none());

    // Smoke test:
    // TODO: turn into actual testing by including 'official' example data.
    let t = best.ln_likelihood;
    assert!(
        float_eq!(t, sum, abs <= 1e-6),
        "Discrepancy between regular and tensor likelihood calculations?\n\
         regular: {sum}\n\
         tensors: {t}\n"
    );

    global.estimate.ln_likelihood = best.ln_likelihood;
    global.estimate.parameters = best.parameters;
    Ok(())
}

fn write_global(output: &Path, global: &output::Global) -> Result<(), Error> {
    let path = output.join(file::GLOBAL);
    cprintln!(
        "Write global analysis results detail to <b>{}</>.",
        path.to_relative()?.display()
    );
    io::write_file!(&path, "global results", "{:#}", json!(global))?;
    Ok(())
}

fn write_detail(output: &Path, details: &[detail::Tree]) -> Result<(), Error> {
    let path = output.join(file::DETAIL);
    cprintln!(
        "Write full trees analysis/filtering detail to <b>{}</>.",
        path.to_relative()?.display()
    );
    io::write_file!(&path, "detailed results", "{:#}", json!(details))?;
    Ok(())
}

fn write_csv(output: &Path, details: &[detail::Tree]) -> Result<(), Error> {
    let path = output.join(file::CSV);
    cprintln!(
        "Summarize scalar values to <b>{}</>.",
        path.to_relative()?.display()
    );
    let file = io::create_file(&path, "csv summary")?;
    let mut wtr = csv::Writer::from_writer(file);
    for detail in details {
        let record: output::csv::Record = detail.into();
        wtr.serialize(record).context(CsvErr)?;
    }
    wtr.flush()
        .with_context(|_| io::AtErr { ctx: "flushing csv summary", path })?;
    Ok(())
}

#[derive(Debug, Snafu)]
#[snafu(context(suffix(Err)))]
enum Error {
    #[snafu(transparent)]
    Check {
        source: aphid::config::expand::Error,
    },
    // TODO: have color_print feature this without the need to allocate an intermediate string?
    #[snafu(display("Output folder already exists: {}.", cformat!("<b>{}</>", path.display())))]
    OutputExists { path: PathBuf },
    #[snafu(transparent)]
    ForestParse {
        source: aphid::genes_forest::parse::Error,
    },
    #[snafu(display("Inconsistent input:\n{mess}"))]
    InputConsistency { mess: String },
    #[snafu(transparent)]
    Learn { source: aphid::learn::Error },
    #[snafu(display("Could not serialize record to CSV:\n{source}"))]
    Csv { source: csv::Error },
    #[snafu(transparent)]
    AphidIo { source: aphid::io::Error },
    #[snafu(transparent)]
    Optim { source: aphid::optim::Error },
}
