// The parameters to optimize are constrained.
// To avoid this becoming a problem for the optimizer,
// translate them into a set of "scores".
// Every score ranges over the whole real line,
// but there is a strict bijection between parameters and scores.

use std::{
    borrow::Cow,
    fmt::{self, Display},
};

use arrayvec::ArrayVec;
use serde::{ser::SerializeStruct, Serialize, Serializer};
use tch::Tensor;

use crate::{
    model::parameters::{GeneFlowTimes, Num},
    Parameters,
};

#[derive(Debug, PartialEq, Eq)]
pub struct Scores<F> {
    pub theta: F,     // Encodes theta.
    pub tau_1: F,     // Encodes tau_1.
    pub delta_tau: F, // Encodes positive difference between tau_2 and tau_1.
    pub gf: F,        // Encodes p_ab + p_ac + p_bc.
    // (implicit) ab = 1,  // Weigths p_ab.
    pub ac: F,      // Weights p_ac.
    pub bc: F,      // Weights p_bc.
    pub ancient: F, // Encodes p_ancient.
    // (implicit) max_gft = 1,
    pub gf_times: GeneFlowTimes<F>, // Encode fraction of max_gft or previous greater gft.
}
// Implicit constants.
const SCORE_AB: f64 = 1.0;
const MAX_GFT: f64 = 1.0;

//--------------------------------------------------------------------------------------------------
// Check whether all constraints are enforced.

#[cfg(test)]
impl Parameters<f64> {
    fn valid(&self) -> bool {
        let &Parameters {
            theta,
            tau_1,
            tau_2,
            p_ab,
            p_ac,
            p_bc,
            p_ancient,
            ref gf_times,
        } = self;
        let unit = |x| (0. ..=1.).contains(&x);
        (0. <= theta)
            && (0. ..tau_2).contains(&tau_1)
            && (unit(p_ab) && unit(p_ac) && unit(p_bc) && unit(p_ancient))
            && unit(p_ab + p_ac + p_bc)
            && gf_times.0.windows(2).all(|gft| gft[0] >= gft[1])
            && gf_times.0[0] <= MAX_GFT
    }
}

//==================================================================================================
// Convert.

// Sigmoid: cast ℝ → [0, 1].
//          reverse [0, 1] ← ℝ.
trait Sigmoid {
    type Output;
    fn sigmoid(self) -> Self::Output;
    fn rev_sigmoid(self) -> Self::Output;
}
impl Sigmoid for f64 {
    type Output = f64;
    fn sigmoid(self) -> f64 {
        1. / (1. + (-self).exp())
    }
    fn rev_sigmoid(self) -> f64 {
        (self / (1. - self)).ln()
    }
}
impl Sigmoid for &Tensor {
    type Output = Tensor;
    fn sigmoid(self) -> Tensor {
        1. / (1. + (-self).exp())
    }
    fn rev_sigmoid(self) -> Tensor {
        Tensor::logit(self, None)
    }
}

//--------------------------------------------------------------------------------------------------
// Calculate parameters from the scores.

macro_rules! scores_to_parms {
    ($score:expr, $Ty:ident) => {{
        let s = $score;
        let theta = s.theta.exp();
        let p_ancient = s.ancient.sigmoid();

        // Taus.
        let tau_1 = s.tau_1.exp();
        let delta_tau = s.delta_tau.exp();
        let tau_2 = &tau_1 + &delta_tau;

        // GF probabilities from weights.
        let pgf = s.gf.sigmoid(); // = p_ab + p_ac + p_bc.
        let w_ab = SCORE_AB.exp();
        let w_ac = s.ac.exp();
        let w_bc = s.bc.exp();
        let w_tot = w_ab + &w_ac + &w_bc;
        let p_ab = &pgf * w_ab / &w_tot;
        let p_ac = &pgf * &w_ac / &w_tot;
        let p_bc = &pgf * &w_bc / &w_tot;

        // Decreasing GF times, each a fraction of the previous one.
        let mut gf_times = ArrayVec::new();
        for (i, sgf) in s.gf_times.0.iter().enumerate() {
            let gft =
                if i == 0 { MAX_GFT * sgf.sigmoid() } else { &gf_times[i - 1] * sgf.sigmoid() };
            gf_times.push(gft);
        }
        let gf_times = GeneFlowTimes(gf_times);

        Parameters {
            theta,
            tau_1,
            tau_2,
            p_ab,
            p_ac,
            p_bc,
            p_ancient,
            gf_times,
        }
    }};
}

impl Scores<f64> {
    pub fn to_parameters(&self) -> Parameters<f64> {
        scores_to_parms!(self, f64)
    }
}

impl Scores<Tensor> {
    pub fn to_parameters(&self) -> Parameters<Tensor> {
        scores_to_parms!(self, f64)
    }
}

//--------------------------------------------------------------------------------------------------
// Calculate scores from parameters.

macro_rules! parms_to_score {
    ($parms:expr, $ln:ident) => {{
        let p = $parms;
        let Parameters { p_ab, p_ac, p_bc, p_ancient, tau_1, tau_2, .. } = p;

        let theta = p.theta.$ln();
        let ancient = p_ancient.rev_sigmoid();

        // Taus.
        let delta_tau = (tau_2 - tau_1).$ln();
        let tau_1 = p.tau_1.$ln();

        // GF probabilities to weights.
        let pgf = p_ab + p_ac + p_bc;
        let gf = pgf.rev_sigmoid();
        let w_ab = SCORE_AB.exp();
        let w_tot = w_ab / p_ab;
        let w_ac = &w_tot * p_ac;
        let w_bc = &w_tot * p_bc;
        let ac = w_ac.$ln();
        let bc = w_bc.$ln();

        // Decreasing GF scores, each expanding from the previous one.
        let mut gf_times = ArrayVec::new();
        for (i, gft) in p.gf_times.0.iter().enumerate() {
            let ratio = if i == 0 { (gft / MAX_GFT) } else { (gft / &p.gf_times.0[i - 1]) };
            let sgf = ratio.rev_sigmoid();
            gf_times.push(sgf);
        }
        let gf_times = GeneFlowTimes(gf_times);

        Scores {
            theta,
            tau_1,
            delta_tau,
            gf,
            ac,
            bc,
            ancient,
            gf_times,
        }
    }};
}

impl Parameters<f64> {
    pub fn to_scores(&self) -> Scores<f64> {
        parms_to_score!(self, ln)
    }
}

impl Parameters<Tensor> {
    pub fn to_scores(&self) -> Scores<Tensor> {
        parms_to_score!(self, log)
    }
}

impl Scores<Tensor> {
    pub fn to_floats(&self) -> Scores<f64> {
        self.map(|t| t.double_value(&[]))
    }
}

//--------------------------------------------------------------------------------------------------
// Convenience iteration among scores.

impl<I> Scores<I> {
    /// Iterate names in canonical fields order.
    pub fn names_from_n(n_gf_times: usize) -> impl Iterator<Item = Cow<'static, str>> {
        // /!\ Order here must match the fields!
        ["theta", "tau_1", "delta_tau", "gf", "ac", "bc", "ancient"]
            .into_iter()
            .map(Cow::Borrowed)
            .chain(GeneFlowTimes::<I>::names_from_n(n_gf_times).map(Cow::Owned))
    }
    /// Iterate names in canonical fields order.
    pub fn names(&self) -> impl Iterator<Item = Cow<'static, str>> {
        Self::names_from_n(self.gf_times.len())
    }

    /// Iterate over fields in canonical order.
    pub fn iter(&self) -> impl Iterator<Item = &I> {
        let Self {
            theta,
            tau_1,
            delta_tau,
            gf,
            ac,
            bc,
            ancient,
            gf_times,
        } = self;
        // /!\ Order must match fields!
        [theta, tau_1, delta_tau, gf, ac, bc, ancient]
            .into_iter()
            .chain(gf_times.iter())
    }

    // /!\ ASSUMING the iterator yields values in canonical order.
    pub fn from_iter(mut it: impl Iterator<Item = I>) -> Self {
        let mut next = || it.next().expect("Missing field in init operator.");
        Self {
            theta: next(),
            tau_1: next(),
            delta_tau: next(),
            gf: next(),
            ac: next(),
            bc: next(),
            ancient: next(),
            gf_times: GeneFlowTimes::from_iter(it),
        }
    }

    /// Iterate and transform in canonical fields order.
    pub fn map<O>(&self, f: impl FnMut(&I) -> O) -> Scores<O> {
        Scores::from_iter(self.iter().map(f))
    }

    /// Iterate and transform in order with canonical field names in canonical order.
    pub fn named_map<O>(&self, mut f: impl FnMut(Cow<'static, str>, &I) -> O) -> Scores<O> {
        let mut names = self.names();
        self.map(|v| f(names.next().unwrap(), v))
    }
}

//==================================================================================================
// Display.

impl<F: Num + Display + fmt::Debug> fmt::Display for Scores<F> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let Scores {
            theta,
            tau_1,
            delta_tau,
            gf,
            ac,
            bc,
            ancient,
            gf_times,
        } = self;
        f.debug_struct("Scores")
            .field("theta", theta)
            .field("tau_1", tau_1)
            .field("delta_tau", delta_tau)
            .field("gf", gf)
            .field("ac", ac)
            .field("bc", bc)
            .field("ancient", ancient)
            .field("gf_times", &gf_times.0)
            .finish()
    }
}

//==================================================================================================
// Serde.

pub(crate) fn ser<S: Serializer, F: Serialize>(
    scores: &Scores<F>,
    ser: S,
) -> Result<S::Ok, S::Error> {
    let Scores {
        theta,
        tau_1,
        delta_tau,
        gf,
        ac,
        bc,
        ancient,
        gf_times,
    } = scores;
    let mut s = ser.serialize_struct("Scores", 8)?;
    macro_rules! serfields {
        ($($id:ident)+) => {$(
            s.serialize_field(stringify!($id), $id)?;
        )+};
    }
    serfields! {
        theta
        tau_1
        delta_tau
        gf
        ac
        bc
        ancient
        gf_times
    };
    s.end()
}

//==================================================================================================
// Test.

#[cfg(test)]
mod tests {
    use std::iter;

    use float_eq::float_eq;
    use rand::prelude::*;
    use tch::{Device, Kind, Tensor};

    use super::*;

    #[test]
    fn parameters_scores_random_roundtrips() {
        let n_tests = 1000;
        let (min, max) = (-10., 10.);
        let seed = 12;
        let check_precision = 1e-7;
        macro_rules! from_rand {
            ($rd:expr) => {
                $rd * (max - min) + min
            };
        }

        // Check with basic float values.
        let mut rng = StdRng::seed_from_u64(seed);
        let mut randf64 = || from_rand!(rng.random::<f64>());
        let check = |a: &f64, e: &f64| {
            assert!(
                float_eq!(a, e, abs <= check_precision),
                "Expected {e}, got {a} (seed: {seed})."
            );
        };
        let checkf64 = check;

        // Check with tensors.
        tch::manual_seed(seed.try_into().unwrap());
        let mut randtch = || from_rand!(Tensor::rand([1,], (Kind::Double, Device::Cpu)));
        let checktch = |a: &Tensor, e: &Tensor| {
            check(
                &a.iter::<f64>().unwrap().next().unwrap(),
                &e.iter::<f64>().unwrap().next().unwrap(),
            );
        };

        // Abstract round-trip test over both.
        macro_rules! test {
            ($_ty:ident, $rand:ident, $check:ident$(, $valid:ident)?) => {
                // Draw original score.
                let o = Scores {
                    theta: $rand(),
                    tau_1: $rand(),
                    delta_tau: $rand(),
                    gf: $rand(),
                    ac: $rand(),
                    bc: $rand(),
                    ancient: $rand(),
                    gf_times: GeneFlowTimes(iter::repeat_with(&mut $rand).take(2).collect()),
                };
                // Roundtrip.
                let parms = o.to_parameters();
                $(assert!(parms.$valid(), "Random scores yielded invalid parameters.");)?
                let back = parms.to_scores();
                // Should be back from where we've started.
                let Scores {
                    theta,
                    tau_1,
                    delta_tau,
                    gf,
                    ac,
                    bc,
                    ancient,
                    gf_times,
                } = back;
                $check(&theta, &o.theta);
                $check(&tau_1, &o.tau_1);
                $check(&delta_tau, &o.delta_tau);
                $check(&gf, &o.gf);
                $check(&ac, &o.ac);
                $check(&bc, &o.bc);
                $check(&ancient, &o.ancient);
                $check(&gf_times.0[0], &o.gf_times.0[0]);
                $check(&gf_times.0[1], &o.gf_times.0[1]);
            };
        }
        for _ in 0..n_tests {
            test!(f64, randf64, checkf64, valid);
            test!(Tensor, randtch, checktch);
        }
    }
}
