// Describe the various considered scenarios for one tree.

use std::iter;

use crate::gene_tree::TripletTopology;

#[derive(Debug, PartialEq, Eq)]
pub(crate) struct IncompleteLineageSortingScenario(TripletTopology);
pub(crate) const N_ILS_SCENARIOS: usize = 3;

#[derive(Debug, PartialEq, Eq)]
#[allow(clippy::upper_case_acronyms)]
pub(crate) enum GeneFlowTopology {
    ABC, // ((A <> B), C) // (undistinguishable directions)
    ACB, // ((A => C), B)
    CAB, // ((C => A), B)
    BCA, // ((B => C), A)
    CBA, // ((C => B), A)
}
#[cfg(test)]
pub(crate) const N_GF_TOPOLOGIES: usize = 5;

#[derive(Debug, PartialEq, Eq)]
pub(crate) struct GeneFlowScenario {
    pub(crate) topology: GeneFlowTopology,
    pub(crate) time: usize, // Index into the vector of gene flow times.
}

#[derive(Debug, PartialEq, Eq)]
pub(crate) enum Scenario {
    NoEvent,
    IncompleteLineageSorting(IncompleteLineageSortingScenario),
    GeneFlow(GeneFlowScenario),
}

//--------------------------------------------------------------------------------------------------
// Every scenario suggests a triplet topology.

impl GeneFlowTopology {
    fn triplet_topology(&self) -> TripletTopology {
        use GeneFlowTopology as G;
        use TripletTopology as T;
        match self {
            G::ABC => T::ABC,          // C out.
            G::ACB | G::CAB => T::ACB, // B out.
            G::BCA | G::CBA => T::BCA, // A out.
        }
    }
}

impl Scenario {
    pub(crate) fn topology(&self) -> TripletTopology {
        use Scenario as S;
        use TripletTopology as T;
        match self {
            S::NoEvent => T::ABC,
            &S::IncompleteLineageSorting(IncompleteLineageSortingScenario(top)) => top,
            S::GeneFlow(GeneFlowScenario { topology, .. }) => topology.triplet_topology(),
        }
    }
}

//--------------------------------------------------------------------------------------------------
// Iterate over all scenarios.

impl GeneFlowTopology {
    fn iter() -> impl Iterator<Item = Self> {
        use GeneFlowTopology as G;
        [G::ABC, G::ACB, G::CAB, G::BCA, G::CBA].into_iter()
    }
}

#[cfg(test)]
pub(crate) fn n_scenarios(n_gf_times: usize) -> usize {
    1 + N_ILS_SCENARIOS + n_gf_times * N_GF_TOPOLOGIES
}

impl Scenario {
    pub(crate) fn iter(n_gf_times: usize) -> impl Iterator<Item = Self> {
        use Scenario as S;
        iter::once(S::NoEvent)
            .chain(
                TripletTopology::iter()
                    .map(|top| S::IncompleteLineageSorting(IncompleteLineageSortingScenario(top))),
            )
            .chain((0..n_gf_times).flat_map(|time| {
                GeneFlowTopology::iter()
                    .map(move |topology| S::GeneFlow(GeneFlowScenario { topology, time }))
            }))
    }
}

//==================================================================================================

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn iterate_scenarios() {
        use GeneFlowScenario as GS;
        use GeneFlowTopology as G;
        use IncompleteLineageSortingScenario as IS;
        use Scenario as S;
        use TripletTopology as T;

        let check = |n, scenarios: &[_]| {
            assert_eq!(Scenario::iter(n).collect::<Vec<_>>(), scenarios);
            assert_eq!(n_scenarios(n), scenarios.len());
        };

        // With no gene flow scenarios.
        check(
            0,
            &[
                S::NoEvent,
                S::IncompleteLineageSorting(IS(T::ABC)),
                S::IncompleteLineageSorting(IS(T::ACB)),
                S::IncompleteLineageSorting(IS(T::BCA)),
            ],
        );

        // With 1 possible gene flow event.
        #[rustfmt::skip]
        check(1,
            &[
                S::NoEvent,
                S::IncompleteLineageSorting(IS(T::ABC)),
                S::IncompleteLineageSorting(IS(T::ACB)),
                S::IncompleteLineageSorting(IS(T::BCA)),
                S::GeneFlow(GS { topology: G::ABC, time: 0 }),
                S::GeneFlow(GS { topology: G::ACB, time: 0 }),
                S::GeneFlow(GS { topology: G::CAB, time: 0 }),
                S::GeneFlow(GS { topology: G::BCA, time: 0 }),
                S::GeneFlow(GS { topology: G::CBA, time: 0 }),
            ]
        );

        // With 2 possible gene flow events.
        #[rustfmt::skip]
        check(2,
            &[
                S::NoEvent,
                S::IncompleteLineageSorting(IS(T::ABC)),
                S::IncompleteLineageSorting(IS(T::ACB)),
                S::IncompleteLineageSorting(IS(T::BCA)),
                S::GeneFlow(GS { topology: G::ABC, time: 0 }),
                S::GeneFlow(GS { topology: G::ACB, time: 0 }),
                S::GeneFlow(GS { topology: G::CAB, time: 0 }),
                S::GeneFlow(GS { topology: G::BCA, time: 0 }),
                S::GeneFlow(GS { topology: G::CBA, time: 0 }),
                S::GeneFlow(GS { topology: G::ABC, time: 1 }),
                S::GeneFlow(GS { topology: G::ACB, time: 1 }),
                S::GeneFlow(GS { topology: G::CAB, time: 1 }),
                S::GeneFlow(GS { topology: G::BCA, time: 1 }),
                S::GeneFlow(GS { topology: G::CBA, time: 1 }),
            ]
        );
    }
}
