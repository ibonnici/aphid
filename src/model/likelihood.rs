// Calculate chances that the given tree happens given parameters values.
// Two implementations of likelihood are intertwined here:
//  - One straighforward implementation with rust vectors and floats.
//  - One tensor-based implementation with torch,
//    which can be automatically differentiated.

use std::iter;

use itertools::izip;
use paste::paste;
use statrs::distribution::{Discrete, Poisson};
use tch::{Device, IndexOp, Tensor};

use crate::{
    gene_tree::{nb_mutations, triplet::GeneTriplet, TripletTopology},
    model::{
        scenarios::{GeneFlowScenario, GeneFlowTopology, Scenario, N_ILS_SCENARIOS},
        Parameters,
    },
};

//==================================================================================================
// Basic vectors + floats implementation.

// Entrypoint: log-probability that all triplets in the data occur.
#[allow(clippy::module_name_repetitions)]
pub fn ln_likelihood(
    // Receive in this format for better locality.
    triplets: &[GeneTriplet],
    p: &Parameters<f64>,
) -> f64 {
    let mut ln_prob = 0.;
    for gtrip in triplets {
        ln_prob += gtrip.likelihood(p).ln();
    }
    ln_prob
}

// Probability that the given gene triplet occurs,
// integrating over all possible scenarios.
impl GeneTriplet {
    pub fn likelihood(&self, p: &Parameters<f64>) -> f64 {
        let mut prob = 0.;
        for scenario in Scenario::iter(p.gf_times.0.len()) {
            if self.contributes(&scenario) {
                let prob_scenario = scenario.probability(p);
                let expected_lengths = scenario.branches_lengths(p);
                let prob_branches = self.branches_lengths_likelihood(expected_lengths);
                prob += prob_scenario * prob_branches;
            }
        }
        prob
    }

    // Decide whether contribution to the overall likelihood is relevant.
    // Only include concordant scenarios,
    // unless the triplet is not resolved enough.
    fn contributes(&self, scenario: &Scenario) -> bool {
        let triplet = &self.local;
        triplet.topology == scenario.topology() || !triplet.resolved
    }
}

//--------------------------------------------------------------------------------------------------
// Expressions for probability of having ILS and of not having gene flow.

// Use macro to ease abstraction over both floats and tensors.
macro_rules! p_ils {
    ($theta:expr, $tau_1:expr, $tau_2:expr, $Ty:ty) => {{
        let (theta, tau_1, tau_2) = ($theta, $tau_1, $tau_2);
        let p_ils = ((2.0 * (tau_1 - tau_2) / theta) as $Ty).exp();
        p_ils
    }};
}
macro_rules! probabilities_ils_nongf {
    ($parameters:expr, $Ty:ty) => {{
        let Parameters { theta, tau_1, tau_2, p_ab, p_ac, p_bc, .. } = $parameters;
        let p_ils = p_ils!(theta, tau_1, tau_2, $Ty);
        let p_ngf = 1. - p_ab - p_ac - p_bc;
        [p_ils, p_ngf]
    }};
}

pub(crate) fn p_ils(theta: f64, tau_1: f64, tau_2: f64) -> f64 {
    p_ils!(theta, tau_1, tau_2, f64)
}
#[allow(dead_code)] // Consistency.
pub(crate) fn p_ils_tensor(theta: Tensor, tau_1: Tensor, tau_2: Tensor) -> Tensor {
    p_ils!(theta, tau_1, tau_2, Tensor)
}

// (not actually used in this implementation)
impl Parameters<f64> {
    fn _probabilities_ils_nongf(&self) -> [f64; 2] {
        probabilities_ils_nongf!(self, f64)
    }
}
impl Parameters<Tensor> {
    fn _probabilities_ils_nongf(&self) -> [Tensor; 2] {
        probabilities_ils_nongf!(self, Tensor)
    }
}

//--------------------------------------------------------------------------------------------------
// Prior probability that the scenario occurs,
// with respect to other possible scenarios.

macro_rules! scenario_probability {
    ($scenario:expr, $parameters:expr
     // Weak hack to ease factorization in this very particular (f64, Tensor) abstraction.
     $(, $Ty:ident)?
     ) => {
        paste! {{
            let p = $parameters;
            let Parameters {
                theta,
                tau_1,
                tau_2,
                p_ab,
                p_ac,
                p_bc,
                ..
            } = p;
            {
                use Scenario as S;
                let p_ils = ((2.0 * (tau_1 - tau_2) / theta) $(as $Ty)?).exp();
                let p_ngf = 1. - p_ab - p_ac - p_bc;
                match $scenario {
                    S::NoEvent => (1. - p_ils) * p_ngf,
                    #[allow(clippy::cast_precision_loss)]
                    S::IncompleteLineageSorting(_) => (1. / N_ILS_SCENARIOS as f64) * p_ils * p_ngf,
                    S::GeneFlow(gf) => gf.[<probability $(_ $Ty:lower s)?>](p),
                }
            }
        }}
    };
}

impl Scenario {
    pub(crate) fn probability(&self, p: &Parameters<f64>) -> f64 {
        scenario_probability!(self, p)
    }
    pub(crate) fn probability_tensors(&self, p: &Parameters<Tensor>) -> Tensor {
        scenario_probability!(self, p, Tensor)
    }
}

//--------------------------------------------------------------------------------------------------
// Calculate gene flow probability depending on topology.

macro_rules! geneflow_probability {
    ($geneflow:expr, $parameters:expr) => {{
        let Self { topology, time } = $geneflow;
        let Parameters { p_ab, p_ac, p_bc, p_ancient, .. } = $parameters;
        let ngf = $parameters.n_gf_times();
        {
            use GeneFlowTopology as GT;
            let prob = match topology {
                GT::ABC => 1. * p_ab, // Trivial multiplication for better typecheck.
                GT::ACB | GT::CAB => 0.5 * p_ac,
                GT::BCA | GT::CBA => 0.5 * p_bc,
            };
            #[allow(clippy::cast_precision_loss)]
            if (*time) == 0 {
                p_ancient * prob
            } else {
                (1. - p_ancient) * prob / (ngf - 1) as f64
            }
        }
    }};
}

impl GeneFlowScenario {
    pub(crate) fn probability(&self, p: &Parameters<f64>) -> f64 {
        geneflow_probability!(self, p)
    }
    pub(crate) fn probability_tensors(&self, p: &Parameters<Tensor>) -> Tensor {
        geneflow_probability!(self, p)
    }
}

//--------------------------------------------------------------------------------------------------
// Calculate expected branches lengths.

// Expected branches lengths for the triplet, according to the given scenario:
// Return only pair of short/long branches lengths [S, L].
//
//          ┌──┴──┐
//         d│L-S  │
//        ┌─┴─┐  c│L
//       a│S b│S  │
//
macro_rules! compact_branches_lengths {
    ($scenario:expr, $parameters:expr) => {{
        use Scenario as S;
        let Parameters { theta, tau_1, tau_2, gf_times, .. } = $parameters;
        let gf_times = |i| &gf_times.0[i];
        match $scenario {
            S::NoEvent => [tau_1 + 0.5 * theta, tau_2 + 0.5 * theta],
            S::IncompleteLineageSorting(_) => {
                [tau_2 + (1. / 6.) * theta, tau_2 + (2. / 3.) * theta]
            }
            &S::GeneFlow(GeneFlowScenario { time: i, ref topology }) => {
                use GeneFlowTopology as G;
                let tau_g = tau_1 * gf_times(i);
                [
                    tau_g,
                    match topology {
                        G::ABC | G::CAB | G::CBA => tau_2,
                        G::ACB | G::BCA => tau_1,
                    } + 0.5 * theta,
                ]
            }
        }
    }};
}

// Expand the above compact [S, L] representation
// into the corresponding expected [a, b, c, d] order
// matching species topologies.
macro_rules! expand_lengths {
    ($scenario:expr, $sl:expr) => {{
        use TripletTopology as T;
        let [ref s, ref l] = $sl;
        let (d, [a, b, c]) = (
            l - s, // Internal branch always has this expected value.
            match $scenario.topology() {
                T::ABC => [s, s, l], // Long branch is C.
                T::ACB => [s, l, s], // Long branch is B.
                T::BCA => [l, s, s], // Long branch is A.
            },
        );
        ([a, b, c], d)
    }};
}

impl Scenario {
    pub(crate) fn branches_lengths(&self, parms: &Parameters<f64>) -> [f64; 4] {
        let sl = compact_branches_lengths!(self, parms);
        let ([&a, &b, &c], d) = expand_lengths!(self, sl);
        [a, b, c, d]
    }
    pub(crate) fn branches_lengths_tensor(&self, parms: &Parameters<Tensor>) -> Tensor {
        let sl = compact_branches_lengths!(self, parms);
        let ([a, b, c], ref d) = expand_lengths!(self, sl);
        Tensor::stack(&[a, b, c, d], 0)
    }
}

//--------------------------------------------------------------------------------------------------
// Probability that the given gene triplet occurs
// given the expected branches lengths.
impl GeneTriplet {
    fn branches_lengths_likelihood(&self, expected_lengths: [f64; 4]) -> f64 {
        // (locally match the paper notations)
        let alpha_i = self.relative_mutation_rate;
        let loc = &self.local;

        // Minimize .exp() evaluations within poisson density by incursing into log space.
        loc.branches_lengths
            .iter()
            .zip(expected_lengths.iter())
            .map(|(&a, &e)| {
                // The expected branch length is not yet a number of mutations.
                let actual = u64::from(a);
                let expected = alpha_i * nb_mutations(e, loc.sequence_length);
                ln_poisson_density(actual, expected)
            })
            .sum::<f64>()
            .exp()
    }
}

fn ln_poisson_density(n: u64, lambda: f64) -> f64 {
    Poisson::new(lambda).unwrap().ln_pmf(n)
}

//==================================================================================================
// Alternate implementation with torch tensors,
// useful for autograd + optimisation.
// Represent log-likelihood by lnl = F(X, P)
// with 'X' fixed data
// and 'P' user parameters to optimize.

// Construct all non(-potentially)-gradient-tracked tensors,
// referring to the immutable input data 'X',
// and used to calculate the eventual likelihood.
// Some of it, the untracked scores,
// is actually standing within the Scores struct.
pub(crate) fn data_tensors<'s>(
    triplets: &[GeneTriplet],
    n_scenarios: usize,
    scenarios: &'s [Scenario],
    dev: Device,
) -> DataTensors<'s> {
    let n_trees = triplets.len();
    let n_trees_i: i64 = n_trees
        .try_into()
        .unwrap_or_else(|_| panic!("Too many trees ({n_trees}) to be indexed with i64 by torch."));

    let mut n = Vec::new(); // Generic name for branch lengths a/b/c/d.
    let mut alpha = Vec::new();
    let mut seqlen = Vec::new();
    for trip in triplets {
        alpha.push(trip.relative_mutation_rate);
        n.push(trip.local.branches_lengths.map(f64::from));
        seqlen.push(f64::from(trip.local.sequence_length));
    }
    let alpha = Tensor::from_slice(&alpha).to_device(dev);
    let n = Tensor::from_slice2(&n).to_device(dev);

    // Costly pre-calculation.
    let minus_ln_factorial_n = -(&n + 1).special_gammaln();
    let seqlen = Tensor::from_slice(&seqlen).to_device(dev);
    // The value actually used to calculate poisson lambda parameter.
    let alphaseq = alpha * seqlen;

    // The likelihood formula differs for every scenario,
    // later combined into a sum.
    // For every scenario, pick indices of the trees matching it.
    let mut match_trees: Vec<_> = iter::repeat_with(Vec::new).take(n_scenarios).collect();
    for (i_tree, triplet) in triplets.iter().enumerate() {
        for (i_scenario, scenario) in scenarios.iter().enumerate() {
            if triplet.contributes(scenario) {
                match_trees[i_scenario].push(i64::try_from(i_tree).unwrap());
            }
        }
    }
    let scenario_trees_index = match_trees
        .into_iter()
        .map(|trees_indices| Tensor::from_slice(&trees_indices).to_device(dev))
        .collect::<Vec<_>>();

    // For every scenario, the immutable 'X' slice of relevant data
    // can be extracted for the trees concerned.
    let alphaseq_per_scenario = scenario_trees_index
        .iter()
        .map(|i_trees| alphaseq.i(i_trees))
        .collect::<Vec<_>>();
    let n_per_scenario = scenario_trees_index
        .iter()
        .map(|i_trees| n.i((i_trees, ..)))
        .collect::<Vec<_>>();
    let minus_ln_factorial_n_per_scenario = scenario_trees_index
        .iter()
        .map(|i_trees| minus_ln_factorial_n.i((i_trees, ..)))
        .collect::<Vec<_>>();

    // For every scenario, collect contribution of every tree to its likelihood,
    // in a sparse fashion corresponding to the columns of a conceptual scenario × gene trees matrix.
    // This matrix remains conceptual and is never actually reified into a tensor.
    let columns_indices = Tensor::cat(&scenario_trees_index, 0).to_device(dev);

    DataTensors {
        n_trees_i,
        scenarios,
        alphaseq_per_scenario,
        n_per_scenario,
        minus_ln_factorial_n_per_scenario,
        columns_indices,
    }
}

pub(crate) fn ln_likelihood_tensors(data: &DataTensors, parms: &Parameters<Tensor>) -> Tensor {
    let DataTensors {
        n_trees_i,
        scenarios,
        alphaseq_per_scenario,
        n_per_scenario,
        minus_ln_factorial_n_per_scenario,
        columns_indices,
    } = data;
    let (kind, device) = parms.iter().map(|t| (t.kind(), t.device())).next().unwrap();

    let mut columns = Vec::with_capacity(scenarios.len());

    for (scenario, alphaseq, n, minus_ln_factorial_n) in izip!(
        *scenarios,
        alphaseq_per_scenario,
        n_per_scenario,
        minus_ln_factorial_n_per_scenario,
    ) {
        // Scenario prior probability.
        let prior = scenario.probability_tensors(parms);

        // Expected branch lengths.
        let expected_abcd = scenario.branches_lengths_tensor(parms);

        // Log-densities of poisson distribution.
        let lambda = Tensor::outer(alphaseq, &expected_abcd);
        let ln_poisson = minus_ln_factorial_n + n * lambda.log() - lambda;

        // Sum over the branches and exponentiate
        // to get the product of poisson distributions.
        let poisson_product = ln_poisson.sum_dim_intlist(1, false, kind).exp();

        // Down to the final contribution of every tree
        // to the likelihood of this scenario.
        let contributions = prior * poisson_product;

        columns.push(contributions);
    }

    // Sum over all scenarios to get the total probability of every tree.
    // This is a sum over the rows of the conceptual sparse matrix.
    let columns = Tensor::cat(&columns, 0);
    let trees_prob =
        Tensor::zeros(*n_trees_i, (kind, device)).scatter_add(0, columns_indices, &columns);

    // And finally integrate this all into the total likelihood.
    trees_prob.log().sum(None)
}

// Constant tensors
// supposed not to be changed during the whole optimisation procedure.
pub(crate) struct DataTensors<'s> {
    n_trees_i: i64,
    scenarios: &'s [Scenario],
    alphaseq_per_scenario: Vec<Tensor>,
    n_per_scenario: Vec<Tensor>,
    minus_ln_factorial_n_per_scenario: Vec<Tensor>,
    columns_indices: Tensor,
}
