// Structure the parameters needed for calculating the likelihood.
// "Parameters" in the sense that they may be subject to optimisation.

use std::{
    borrow::Cow,
    fmt::{self, Display},
};

use arrayvec::ArrayVec;
use color_print::cwriteln;
use serde::{Deserialize, Serialize};
use tch::Tensor;

// The structure is generic among float and tensors
// to allow both likelihood implementations.
// NB: the required bounds to abstract over both in this program
// is very complicated: (f64 - Num, Num / f64, Num.exp(), etc.)
// and it needs to be repeated on every method implementation.
// As a consequence, either duplicate the code or use macros,
// and keep genericity just for factorizing over the parameters type.
pub trait Num: Sized {}
impl Num for f64 {}
impl Num for Tensor {}

// Constraints to uphold:
//  - all positive
//  - tau_1 <= tau_2
//  - p_* <= 1
//  - p_ab + p_ac + p_bc <= 1
#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct Parameters<F> {
    // Population size.
    pub theta: F,
    // Divergence times.
    pub tau_1: F,
    pub tau_2: F,
    // Gene flow.
    pub p_ab: F,
    pub p_ac: F,
    pub p_bc: F,
    pub p_ancient: F, // Only the most ancient gf time gets a special probability.
    // Date of inferred gene flow events.
    pub gf_times: GeneFlowTimes<F>, // At least one value, sorted decreasing, all positives.
}

// Parameters are stack-allocated, so don't overuse possible GF times.
pub const MAX_N_GF_TIMES: usize = 3;
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq, Default)]
#[serde(transparent)]
pub struct GeneFlowTimes<F>(pub(crate) ArrayVec<F, MAX_N_GF_TIMES>);

//--------------------------------------------------------------------------------------------------
// Minor methods.

impl<F> GeneFlowTimes<F> {
    pub(crate) fn new() -> Self {
        Self(ArrayVec::new())
    }
    pub(crate) fn len(&self) -> usize {
        self.0.len()
    }
}

impl<F> Parameters<F> {
    pub(crate) fn n_gf_times(&self) -> usize {
        self.gf_times.len()
    }
}

impl Parameters<Tensor> {
    pub fn to_floats(&self) -> Parameters<f64> {
        self.map(|t| t.double_value(&[]))
    }
}

//--------------------------------------------------------------------------------------------------
// Convenience iteration among parameters.

impl<I> Parameters<I> {
    /// Iterate names in canonical fields order.
    pub fn names_from_n(n_gf_times: usize) -> impl Iterator<Item = Cow<'static, str>> {
        // /!\ Order here must match the fields!
        [
            "theta",
            "tau_1",
            "tau_2",
            "p_ab",
            "p_ac",
            "p_bc",
            "p_ancient",
        ]
        .into_iter()
        .map(Cow::Borrowed)
        .chain(GeneFlowTimes::<I>::names_from_n(n_gf_times).map(Cow::Owned))
    }

    /// Iterate names in canonical fields order.
    pub fn names(&self) -> impl Iterator<Item = Cow<'static, str>> {
        Parameters::<I>::names_from_n(self.gf_times.len())
    }

    /// Iterate over fields in canonical order.
    pub fn iter(&self) -> impl Iterator<Item = &I> {
        let Self {
            theta,
            tau_1,
            tau_2,
            p_ab,
            p_ac,
            p_bc,
            p_ancient,
            gf_times,
        } = self;
        // /!\ Order must match fields!
        [theta, tau_1, tau_2, p_ab, p_ac, p_bc, p_ancient]
            .into_iter()
            .chain(gf_times.iter())
    }

    /// Consume fields in canonical order.
    #[allow(clippy::should_implement_trait)] // (wait on https://github.com/rust-lang/rust/issues/63063)
    pub fn into_iter(self) -> impl Iterator<Item = I> {
        let Self {
            theta,
            tau_1,
            tau_2,
            p_ab,
            p_ac,
            p_bc,
            p_ancient,
            gf_times,
        } = self;
        // /!\ Order must match fields!
        [theta, tau_1, tau_2, p_ab, p_ac, p_bc, p_ancient]
            .into_iter()
            .chain(gf_times.into_iter())
    }

    // /!\ ASSUMING the iterator yields values in canonical order.
    #[allow(clippy::should_implement_trait)] // (wait on https://github.com/rust-lang/rust/issues/63063)
    pub fn from_iter(mut it: impl Iterator<Item = I>) -> Self {
        let mut next = || it.next().expect("Missing field in init operator.");
        Self {
            theta: next(),
            tau_1: next(),
            tau_2: next(),
            p_ab: next(),
            p_ac: next(),
            p_bc: next(),
            p_ancient: next(),
            gf_times: GeneFlowTimes::from_iter(it),
        }
    }

    /// Iterate and transform in canonical fields order.
    pub fn map<O>(&self, f: impl FnMut(&I) -> O) -> Parameters<O> {
        Parameters::from_iter(self.iter().map(f))
    }

    /// Iterate and transform in order with canonical field names in canonical order.
    pub fn named_map<O>(&self, mut f: impl FnMut(Cow<'static, str>, &I) -> O) -> Parameters<O> {
        let mut names = self.names();
        self.map(move |v| f(names.next().unwrap(), v))
    }

    /// Fuse two structs into each other according to the given closure.
    pub fn into_merged_along<I2, O>(
        self,
        other: &Parameters<I2>,
        mut f: impl FnMut(I, &I2) -> O,
    ) -> Parameters<O> {
        Parameters::from_iter(self.into_iter().zip(other.iter()).map(|(a, b)| f(a, b)))
    }
}

// Same for this non-scalar parameter.
impl<I> GeneFlowTimes<I> {
    pub fn names_from_n(n_gf_times: usize) -> impl Iterator<Item = String> {
        (0..n_gf_times).map(|i| format!("gf_times_{i}"))
    }
    pub fn names(&self) -> impl Iterator<Item = String> {
        Self::names_from_n(self.len())
    }
    pub fn iter(&self) -> impl Iterator<Item = &I> {
        self.0.iter()
    }
    #[allow(clippy::should_implement_trait)] // (wait on https://github.com/rust-lang/rust/issues/63063)
    pub fn into_iter(self) -> impl Iterator<Item = I> {
        self.0.into_iter()
    }
    #[allow(clippy::should_implement_trait)] // (wait on https://github.com/rust-lang/rust/issues/63063)
    pub fn from_iter(it: impl Iterator<Item = I>) -> Self {
        GeneFlowTimes(it.collect())
    }
    pub fn map<O>(&self, f: impl FnMut(&I) -> O) -> GeneFlowTimes<O> {
        GeneFlowTimes::<O>::from_iter(self.iter().map(f))
    }
    pub fn named_map<O>(&self, mut f: impl FnMut(String, &I) -> O) -> GeneFlowTimes<O> {
        let mut names = self.names();
        self.map(|v| f(names.next().unwrap(), v))
    }
}

//==================================================================================================
// Display.

impl<F: Num + Display> Display for GeneFlowTimes<F> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_list()
            .entries(self.0.iter().map(|gf| format!("{gf}")))
            .finish()
    }
}

// Colored parameters for stdout.
pub struct Colored<'p, F: Num + Display + Sized>(&'p Parameters<F>);

impl<F: Num + Display + Sized + fmt::Debug> Display for Colored<'_, F> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "Parameters {{")?;
        let Parameters {
            theta,
            tau_1,
            tau_2,
            p_ab,
            p_ac,
            p_bc,
            p_ancient,
            gf_times,
        } = &self.0;
        cwriteln!(f, "  theta: <c>{theta:?}</>,")?;
        cwriteln!(f, "  tau_1: <c>{tau_1:?}</>,")?;
        cwriteln!(f, "  tau_2: <c>{tau_2:?}</>,")?;
        cwriteln!(f, "  p_ab: <c>{p_ab:?}</>,")?;
        cwriteln!(f, "  p_ac: <c>{p_ac:?}</>,")?;
        cwriteln!(f, "  p_bc: <c>{p_bc:?}</>,")?;
        cwriteln!(f, "  p_ancient: <c>{p_ancient:?}</>,")?;
        writeln!(f, "  gf_times: [")?;
        for t in &gf_times.0 {
            cwriteln!(f, "    <c>{t}</>,")?;
        }
        writeln!(f, "  ]")?;
        writeln!(f, "}}")
    }
}

impl<F: Num + Display + Sized> Parameters<F> {
    pub fn colored(&self) -> Colored<F> {
        Colored(self)
    }
}
