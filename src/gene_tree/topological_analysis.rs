// Trees are included or not
// depending on the species they contain
// compared to the species designated in taxa config.
//
// This requires a topological analysis
// based on the species names found in the config 'Taxa' section.
// This files describes the topological features extracted from the tree.

use std::collections::HashSet;

use super::GeneTree;
use crate::{config::Taxa, interner::SpeciesSymbol};

#[cfg_attr(test, derive(Debug, PartialEq))]
pub struct TopologicalAnalysis {
    // Status of each section.
    pub triplet: SectionAnalysis,
    pub outgroup: SectionAnalysis,
    // LCA of both sections: LCA(triplet, outgroup), if they both have one.
    pub top: Option<TreeTop>,
}

#[cfg_attr(test, derive(Debug, PartialEq))]
pub enum SectionAnalysis {
    AllMissing,
    // If at least some were found,
    // then we can calculate their LCA and the associated set of paraphyletic species.
    MissingSome(Vec<SpeciesSymbol>, SectionLca),
    AllFound(SectionLca),
}

#[cfg_attr(test, derive(Debug, PartialEq))]
pub struct SectionLca {
    pub id: usize,
    pub paraphyletic: Vec<SpeciesSymbol>,
}

#[cfg_attr(test, derive(Debug, PartialEq))]
pub struct TreeTop {
    // LCA(triplet, outgroup)
    pub lca: usize,
    // Species not descending from LCA(triplet, outgroup).
    // If non empty, then LCA(triplet, outgroup) is not the root of the tree.
    pub external: Vec<SpeciesSymbol>,
    // Species descending from LCA(triplet, outgroup),
    // but not from LCA(triplet) or LCA(outgroup).
    // Only defined if there is no direct lineage
    // between LCA(triplet) and LCA(outgroup).
    pub internal: Option<InternalSpecies>,
}

#[cfg_attr(test, derive(Debug, PartialEq))]
pub struct InternalSpecies {
    // Split into two contiguous parts:
    //   - Species branching between LCA(triplet) and LCA(triplet, outgroup).
    //   - Species branching between LCA(outgroup) and LCA(triplet, outgroup).
    pub species: Vec<SpeciesSymbol>,
    pub first_outgroup: usize, // <- Locate the split.
}

impl GeneTree {
    pub fn topological_analysis(&self, taxa: &Taxa) -> TopologicalAnalysis {
        // Find LCA and paraphyletic species for each sections.
        let triplet = self.section_analysis(taxa.triplet.iter());
        let outgroup = self.section_analysis(taxa.outgroup.iter().copied());

        // Check that they coalesce at the root.
        let top = if let (
            Some(&SectionLca { id: lca_triplet, .. }),
            Some(&SectionLca { id: lca_outgroup, .. }),
        ) = (triplet.lca(), outgroup.lca())
        {
            let (lca, is_triplet_first) = self.tree.common_parent(lca_triplet, lca_outgroup);
            // Collect external species.
            let root = 0;
            let external = self.leaves_excluding(lca, root).collect::<Vec<_>>();
            let internal = (triplet.is_monophyletic().unwrap()
                && outgroup.is_monophyletic().unwrap())
            .then(|| {
                // Since both triplet and outgroup are monophyletic,
                // and their leaves species are disjoint,
                // then they cannot be direct lineage of each other
                // and we can safely define/collect both types of 'internal' species.
                let &[right, left] = self.tree.nodes[lca].children().unwrap();
                let (triplet_side, outgroup_side) =
                    if is_triplet_first.unwrap() { (right, left) } else { (left, right) };
                let mut species = self
                    .leaves_excluding(lca_triplet, triplet_side)
                    .collect::<Vec<_>>();
                let first_outgroup = species.len();
                for o in self.leaves_excluding(lca_outgroup, outgroup_side) {
                    species.push(o);
                }
                InternalSpecies { species, first_outgroup }
            });
            Some(TreeTop { lca, external, internal })
        } else {
            None // No treetop analysis if triplet or outgroup is empty.
        };

        // Analysis completed.
        TopologicalAnalysis { triplet, outgroup, top }
    }

    fn section_analysis(&self, species: impl Iterator<Item = SpeciesSymbol>) -> SectionAnalysis {
        use SectionAnalysis as S;
        let mut missing = Vec::new();
        let mut required = HashSet::new();
        let indexes = species.filter_map(|sp| {
            required.insert(sp);
            if let Some(&i) = self.index.get(&sp) {
                Some(i)
            } else {
                missing.push(sp);
                None
            }
        });
        // Climb up the tree to find LCA.
        if let Some(lca) = self.tree.ancestor_of(indexes) {
            // Crawl down to find possible paraphyletic species.
            let paraphyletic = self
                .tree
                .leaves_of(lca)
                .filter_map(|tnode| {
                    let sp = tnode.payload;
                    (!required.contains(&sp)).then_some(sp)
                })
                .collect::<Vec<_>>();
            let lca = SectionLca { id: lca, paraphyletic };
            if missing.is_empty() {
                S::AllFound(lca)
            } else {
                S::MissingSome(missing, lca)
            }
        } else {
            S::AllMissing
        }
    }
}

impl GeneTree {
    // Collect all leaves species from the start node,
    // excluding descendants of the given node.
    fn leaves_excluding(
        &self,
        excluded: usize,
        start: usize,
    ) -> impl Iterator<Item = SpeciesSymbol> + '_ {
        self.tree
            .filter_node_to_leaves(
                start,
                (),
                move |(), (i, ..)| (i != excluded).then_some(()),
                move |(), (i, tnode, _)| (i != excluded).then_some(tnode.payload),
            )
            .map(|(_, name)| name)
    }
}

impl SectionAnalysis {
    pub fn lca(&self) -> Option<&SectionLca> {
        use SectionAnalysis as S;
        match self {
            S::AllMissing => None,
            S::MissingSome(_, lca) | S::AllFound(lca) => Some(lca),
        }
    }
    // Undefined if empty.
    pub(crate) fn is_monophyletic(&self) -> Option<bool> {
        self.lca().map(|lca| lca.paraphyletic.is_empty())
    }
}

impl InternalSpecies {
    pub fn triplet_side(&self) -> &[SpeciesSymbol] {
        &self.species[..self.first_outgroup]
    }
    pub fn outgroup_side(&self) -> &[SpeciesSymbol] {
        &self.species[self.first_outgroup..]
    }
}
