// Most species in a gene tree are only useful to calculate useful statistics,
// before we actually focus the analysis on the triplet of species of interest.

use std::fmt::{self, Display};

use serde::Serialize;

use super::{MeanNbBases, NbBases};
use crate::{
    config::SpeciesTriplet,
    gene_tree::nb_mutations,
    topological_analysis::{SectionAnalysis, SectionLca},
    tree::Node,
    GeneTree, TopologicalAnalysis,
};

// Matches the gene triplet topology onto the expected species topology.
#[derive(Serialize, Debug, Clone, Copy, PartialEq, Eq)]
#[allow(clippy::upper_case_acronyms)]
pub enum Topology {
    /// Concordant triplet: ((A, B), C).
    ABC,
    /// The outer node is B instead of C: ((A, C), B).
    ACB,
    /// The outer node is A instead of C: ((B, C), A).
    BCA,
}

// Extract the minimal information needed from a full gene tree
// for likelihood analysis.
#[allow(clippy::module_name_repetitions)]
#[cfg_attr(test, derive(PartialEq, Debug))]
pub struct LocalGeneTriplet {
    // Rounded branches lengths,
    // in an order making it easy to map
    // against species in the reference phylogenetic triplet.
    // If the triplet is ((A, B), C),
    // the canonical order is: [a, b, c, 'd'],
    // with 'd' the length of the "internal branch",
    // regardless of the topology actually observed.
    // This topology is stored aside to disambiguate,
    // as either 'ABC', 'ACB', 'BCA'.
    // Here are examples of lengths observations encoding:
    //    ((:a, :b):ab, :c)  →  [a, b, c, ab] + ABC
    //    (:b, (:a, :c):ac)  →  [a, b, c, ac] + ACB
    //    (:a, (:c, :b):bc)  →  [a, b, c, bc] + BCA
    //
    // Lengths stored here are not 'per-site' guesses,
    // they are a rounded integer estimate of the number of mutations.
    pub sequence_length: NbBases,
    pub branches_lengths: [NbBases; 4],
    pub topology: Topology,
    // Raise if the topology is considered sufficiently resolved
    // to exclude discordant scenarios.
    pub resolved: bool,
}

// Extend the above type with data
// which cannot be calculated from a tree alone,
// but from a reading through the whole forest.
#[allow(clippy::module_name_repetitions)]
pub struct GeneTriplet {
    pub local: LocalGeneTriplet,
    pub relative_mutation_rate: f64, // Known in the paper as "alpha_i".
}

// Match the gene tree to the associated topology.
// Refer to the species topology as ((a, b), c)
// and to the actual gene tree topology as ((u, v), w).
// Abort with None if the gene tree triplet
// is either incomplete or paraphyletic.
#[allow(clippy::many_single_char_names)]
pub fn extract_local(
    gtree: &GeneTree,
    analysis: &TopologicalAnalysis,
    species_triplet: &SpeciesTriplet,
    unresolved_length: Option<MeanNbBases>,
) -> Option<LocalGeneTriplet> {
    use Node as N;
    let seqlen = gtree.sequence_length;

    // Extract triplet ancestor.
    let SectionAnalysis::AllFound(SectionLca { id: lca_id, ref paraphyletic }) = analysis.triplet
    else {
        return None;
    };
    if !paraphyletic.is_empty() {
        return None;
    }

    let nodes = &gtree.tree.nodes;
    let N::Internal(lca) = &nodes[lca_id] else {
        unreachable!()
    };

    // Use it to extract triplet nodes, regardless of contingent tree ordering.
    let ([N::Internal(uv), N::Terminal(w)] | [N::Terminal(w), N::Internal(uv)]) =
        lca.children.map(|i| &nodes[i])
    else {
        unreachable!()
    };
    let [u, v] = uv.children.map(|i| {
        let N::Terminal(n) = &nodes[i] else {
            unreachable!()
        };
        n
    });

    // Compare to the species topology to figure the order.
    let [a, b, c] = species_triplet.as_array();
    let uv = nb_mutations(uv.branch, seqlen);
    Some(LocalGeneTriplet {
        branches_lengths: {
            // Reorganize terminal branches lengths so they match species order.
            let ids_lens = [u, v, w].map(|n| (n.payload, n.branch));
            let abc = [a, b, c].map(|s| ids_lens.iter().find(|(id, _)| *id == s).unwrap().1);
            let [a, b, c] = abc.map(|l| nb_mutations(l, seqlen));
            // And let the internal branch be whatever internal branch (ab, ac, bc),
            // its exact meaning being specified by the topology calculated hereafter.
            // Convert per-site estimates to total, sequence-wide estimates,
            // and round to an integer to match the Poisson distribution model.
            #[allow(clippy::cast_sign_loss, clippy::cast_possible_truncation)]
            [a, b, c, uv].map(|l| l.round() as NbBases)
        },
        topology: {
            use Topology as T;
            // Only the outermost node is useful in this respect.
            let w = w.payload;
            if w == c {
                T::ABC
            } else if w == b {
                T::ACB
            } else if w == a {
                T::BCA
            } else {
                unreachable!()
            }
        },
        sequence_length: seqlen,
        resolved: if let Some(ul) = unresolved_length { uv > ul } else { true },
    })
}

impl Topology {
    pub(crate) fn iter() -> impl Iterator<Item = Self> {
        use Topology as T;
        [T::ABC, T::ACB, T::BCA].into_iter()
    }
}

impl Display for Topology {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use Topology as T;
        f.write_str(match self {
            T::ABC => "ABC",
            T::ACB => "ACB",
            T::BCA => "BCA",
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{config::Taxa, interner::Interner};

    #[test]
    #[allow(clippy::too_many_lines)] // It does take vertical space to test many trees.
    fn extract_triplet_from_gene_tree() {
        use Topology as T;
        let mut interner = Interner::new();
        let mut g = |c| interner.get_or_intern(c);

        let taxa = Taxa {
            triplet: SpeciesTriplet { a: g("T"), b: g("U"), c: g("V") },
            outgroup: vec![g("O"), g("P"), g("Q")],
            other: vec![g("X"), g("Y"), g("Z")],
        };

        let unresolved_length = Some(0.5);

        let sequence_length = 10;
        let mut check = |input: &str, expected| {
            println!("Checking triplet extraction for tree:\n  {input}");
            let input = format!("{input} {sequence_length}");
            let gtree = GeneTree::from_input(&input, &mut interner).unwrap();
            let analysis = gtree.topological_analysis(&taxa);
            let actual = extract_local(&gtree, &analysis, &taxa.triplet, unresolved_length);
            assert_eq!(expected, actual);
        };

        //------------------------------------------------------------------------------------------
        // Excluded triplets.

        // Incomplete.
        //
        //          │
        //       ┌──0──┐
        //       │     │
        //     ┌─1─┐   │
        //     2   3   4
        //     T   X   V
        check("((T:2,X:3):1,V:4):0;", None);

        // Paraphyletic.
        //
        //             │
        //          ┌──0──┐
        //          │     │
        //       ┌──1──┐  │
        //       │     │  │
        //     ┌─2─┐   │  │
        //     3   4   5  6
        //     T   U   X  V
        check("(((T:3,U:4):2,X:5):1,V:6):0;", None);

        //------------------------------------------------------------------------------------------
        // Tree with only a triplet.
        //
        //          │
        //       ┌──0──┐
        //       │     │
        //     ┌─1─┐   │
        //     2   3   4
        //     T   U   V
        check(
            "((T:2,U:3):1,V:4):0;",
            Some(LocalGeneTriplet {
                branches_lengths: [20, 30, 40, 10],
                sequence_length,
                resolved: true,
                topology: T::ABC,
            }),
        );

        // Considered unresolved.
        for (uv, d) in [(0., 0), (0.03, 0), (0.05, 1) /* ⚠ Rounds to 1 ⚠ */] {
            check(
                &format!("((T:2,U:3):{uv},V:4):0;"),
                Some(LocalGeneTriplet {
                    branches_lengths: [20, 30, 40, d],
                    sequence_length,
                    resolved: false,
                    topology: T::ABC,
                }),
            );
        }
        // Considered resolved.
        let uv = 0.06;
        check(
            &format!("((T:2,U:3):{uv},V:4):0;"),
            Some(LocalGeneTriplet {
                branches_lengths: [20, 30, 40, 1],
                sequence_length,
                resolved: true,
                topology: T::ABC,
            }),
        );

        //------------------------------------------------------------------------------------------
        // Synonymous concordant triplets.
        //
        //          │
        //       ┌──0──┐
        //       │     │
        //       │   ┌─2─┐
        //       1   3   4
        //       V   T   U
        check(
            "(V:1,(T:3,U:4):2):0;",
            Some(LocalGeneTriplet {
                branches_lengths: [30, 40, 10, 20],
                sequence_length,
                resolved: true,
                topology: T::ABC,
            }),
        );

        // Considered unresolved.
        for (uv, d) in [(0., 0), (0.03, 0), (0.05, 1) /* ⚠ Rounds to 1 ⚠ */] {
            check(
                &format!("(V:1,(T:3,U:4):{uv}):0;"),
                Some(LocalGeneTriplet {
                    branches_lengths: [30, 40, 10, d],
                    sequence_length,
                    resolved: false,
                    topology: T::ABC,
                }),
            );
        }
        // Considered resolved.
        let uv = 0.06;
        check(
            &format!("(V:1,(T:3,U:4):{uv}):0;"),
            Some(LocalGeneTriplet {
                branches_lengths: [30, 40, 10, 1],
                sequence_length,
                resolved: true,
                topology: T::ABC,
            }),
        );

        //
        //          │
        //       ┌──0──┐
        //       │     │
        //       │   ┌─2─┐
        //       1   3   4
        //       V   U   T
        check(
            "(V:1,(U:3,T:4):2):0;",
            Some(LocalGeneTriplet {
                branches_lengths: [40, 30, 10, 20],
                sequence_length,
                resolved: true,
                topology: T::ABC,
            }),
        );

        //          │
        //       ┌──0──┐
        //       │     │
        //     ┌─1─┐   │
        //     2   3   4
        //     U   T   V
        check(
            "((U:2,T:3):1,V:4):0;",
            Some(LocalGeneTriplet {
                branches_lengths: [30, 20, 40, 10],
                sequence_length,
                resolved: true,
                topology: T::ABC,
            }),
        );

        //------------------------------------------------------------------------------------------
        // Discordant triplets.
        //
        //          │
        //       ┌──0──┐
        //       │     │
        //       │   ┌─2─┐
        //       1   3   4
        //       T   V   U
        check(
            "(T:1,(V:3,U:4):2):0;",
            Some(LocalGeneTriplet {
                branches_lengths: [10, 40, 30, 20],
                sequence_length,
                resolved: true,
                topology: T::BCA,
            }),
        );

        //
        //          │
        //       ┌──0──┐
        //       │     │
        //       │   ┌─2─┐
        //       1   3   4
        //       U   V   T
        check(
            "(U:1,(V:3,T:4):2):0;",
            Some(LocalGeneTriplet {
                branches_lengths: [40, 10, 30, 20],
                sequence_length,
                resolved: true,
                topology: T::ACB,
            }),
        );

        //
        //          │
        //       ┌──0──┐
        //       │     │
        //     ┌─1─┐   │
        //     2   3   4
        //     U   V   T
        check(
            "((U:2,V:3):1,T:4):0;",
            Some(LocalGeneTriplet {
                branches_lengths: [40, 20, 30, 10],
                sequence_length,
                resolved: true,
                topology: T::BCA,
            }),
        );

        //
        //          │
        //       ┌──0──┐
        //       │     │
        //     ┌─1─┐   │
        //     2   3   4
        //     T   V   U
        check(
            "((T:2,V:3):1,U:4):0;",
            Some(LocalGeneTriplet {
                branches_lengths: [20, 40, 30, 10],
                sequence_length,
                resolved: true,
                topology: T::ACB,
            }),
        );

        //------------------------------------------------------------------------------------------
        // Within a larger tree.
        //
        //                   │
        //             ┌─────0───────┐
        //        ┌────1───┐         │
        //        │        │   ┌─────8──────┐
        //        │        │   │            │
        //     ┌──2──┐     │   │        ┌───10─┐
        //     │     │     │   │        │      │
        //     │   ┌─4─┐   │   │     ┌──11─┐   │
        //     │   │   │   │   │     │     │   │
        //     │   │   │   │   │   ┌─12┐   │   │
        //     3   5   6   7   9   13  14  15  16
        //     O   P   Q   X   Y   T   U   V   Z
        check(
            "(((O:3,(P:5,Q:6):4):2,X:7):1,(Y:9,(((T:13,U:14):12,V:15):11,Z:16):10):8):0;",
            Some(LocalGeneTriplet {
                branches_lengths: [130, 140, 150, 120],
                sequence_length,
                resolved: true,
                topology: T::ABC,
            }),
        );

        // Discordant.
        //
        //                   │
        //             ┌─────0───────┐
        //        ┌────1───┐         │
        //        │        │   ┌─────8──────┐
        //        │        │   │            │
        //     ┌──2──┐     │   │        ┌───10─┐
        //     │     │     │   │        │      │
        //     │   ┌─4─┐   │   │     ┌──11─┐   │
        //     │   │   │   │   │     │     │   │
        //     │   │   │   │   │   ┌─12┐   │   │
        //     3   5   6   7   9   13  14  15  16
        //     O   P   Q   X   Y   U   V   T   Z
        check(
            "(((O:3,(P:5,Q:6):4):2,X:7):1,(Y:9,(((U:13,V:14):12,T:15):11,Z:16):10):8):0;",
            Some(LocalGeneTriplet {
                branches_lengths: [150, 130, 140, 120],
                sequence_length,
                resolved: true,
                topology: T::BCA,
            }),
        );
    }
}
