// The specific gene tree reader collects symbols into an index along the way.
// It is responsible for checking that names are not duplicated
// and that branches lengths are all present and valid..
// except the last (root) one which may be elided.

use std::{
    any,
    collections::{hash_map::Entry, HashMap},
};

use super::{BranchLength, NbBases};
use crate::{
    interner::{Interner, SpeciesSymbol},
    lexer::{errout, lexerr, Error as LexerError, Lexer},
    tree::parse::TreeDataReader,
    GeneTree,
};

// Bound to the interner lifetime.
struct GeneTreeDataReader<'n> {
    index: HashMap<SpeciesSymbol, usize>,
    interner: &'n mut Interner,
}

impl GeneTreeDataReader<'_> {
    fn read_name(&mut self, name: &str, i_node: usize) -> Result<SpeciesSymbol, LexerError> {
        let symbol = self.interner.get_or_intern(name);
        match self.index.entry(symbol) {
            Entry::Vacant(e) => e.insert(i_node),
            Entry::Occupied(e) => {
                let &i_previous = e.get();
                errout!(
                    "Node name {name:?} is given to node {i_node} \
                     but it was already given to node {i_previous}."
                );
            }
        };
        Ok(symbol)
    }
}

impl TreeDataReader<'_, BranchLength, Option<SpeciesSymbol>, SpeciesSymbol>
    for GeneTreeDataReader<'_>
{
    fn parse_branch_data(
        &mut self,
        input: &str,
        i_node: usize,
        is_internal: bool,
        is_last: bool,
    ) -> Result<BranchLength, LexerError> {
        if is_last && input.is_empty() {
            return Ok(0.);
        }
        input.parse::<BranchLength>().map_err(|e| {
            let an = if is_last { "the last" } else { "an" };
            let internal = if is_internal { "internal" } else { "external" };
            lexerr!(
                "Could not parse {input:?} \
                 as {an} {internal} branch data ({}) (node {i_node}): {e}",
                any::type_name::<BranchLength>()
            )
        })
    }

    fn parse_internal_node_data(
        &mut self,
        input: &str,
        i_node: usize,
    ) -> Result<Option<SpeciesSymbol>, LexerError> {
        Ok(if input.is_empty() { None } else { Some(self.read_name(input, i_node)?) })
    }

    fn parse_terminal_node_data(
        &mut self,
        input: &str,
        i_node: usize,
    ) -> Result<SpeciesSymbol, LexerError> {
        if input.is_empty() {
            errout!("No name given to leaf node {i_node}.");
        } else {
            self.read_name(input, i_node)
        }
    }
}

// Wrap basic tree reader into a gene tree reader.
impl Lexer<'_> {
    pub(crate) fn read_gene_tree(
        &mut self,
        interner: &mut Interner,
    ) -> Result<GeneTree, LexerError> {
        let mut reader = GeneTreeDataReader::new(interner);
        let tree = self.read_tree(&mut reader)?;
        let read = self.trim_start_on_line().read_block().ok_or_else(|| {
            lexerr!("Unexpected end of line while reading gene tree sequence length.")
        })?;
        let sequence_length = read
            .parse::<NbBases>()
            .map_err(|e| lexerr!("Invalid sequence length: {read:?}: {e}"))?;
        Ok(GeneTree { tree, index: reader.index, sequence_length })
    }
}

impl<'n> GeneTreeDataReader<'n> {
    pub fn new(interner: &'n mut Interner) -> Self {
        Self { index: HashMap::new(), interner }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    impl GeneTree {
        pub(crate) fn from_input(input: &str, interner: &mut Interner) -> Result<Self, LexerError> {
            Lexer::new(input).read_gene_tree(interner)
        }
    }

    #[test]
    fn parse_lengths() {
        let mut interner = Interner::new();
        // Extract the two numbers
        let mut nums = |input| {
            let gtree = GeneTree::from_input(input, &mut interner).unwrap();
            assert_eq!(gtree.len(), 1);
            (*gtree.tree.nodes[0].branch(), gtree.sequence_length())
        };
        // Any valid rust floating point notation is allowed for branch length.
        // https://doc.rust-lang.org/beta/std/primitive.f64.html#impl-FromStr-for-f64
        assert_eq!(nums("A; 0"), (0.0, 0)); // Default to 0 for the root.
        assert_eq!(nums("A:1; 1"), (1.0, 1)); // Integer are fine.
        assert_eq!(nums("A:1.; 2"), (1.0, 2)); // Optional comma.
        assert_eq!(nums("A:1.2; 3"), (1.2, 3)); // Decimals are ok.
        assert_eq!(nums("A:-.1; 4"), (-0.1, 4)); // Optional null integer part.
        assert_eq!(nums("A:1e-2; 5"), (0.01, 5)); // Scientific notation.
        assert_eq!(nums("A:+inf; 6"), (BranchLength::INFINITY, 6)); // Infinity.
        let (l, seql) = nums("A:NaN; 7"); // Not a number.
        assert!(l.is_nan());
        assert_eq!(seql, 7);
    }

    #[test]
    fn index() {
        // Check correct index construction.
        let mut interner = Interner::new();
        let i = &mut interner;
        check_index("A;", [("A", 0)], i);
        check_index("(A:1, B:2);", [("A", 1), ("B", 2)], i);
        check_index("(A:1, B:2)root;", [("root", 0), ("A", 1), ("B", 2)], i);
        check_index(
            "((A:1, B:2)C:3, D:4)root;",
            [("root", 0), ("C", 1), ("A", 2), ("B", 3), ("D", 4)],
            i,
        );
        // Naming internal nodes is optional.
        check_index("((A:1, B:2):3, D:4);", [("A", 2), ("B", 3), ("D", 4)], i);
    }
    fn check_index<const N: usize>(
        input: &str,
        index: [(&str, usize); N],
        interner: &mut Interner,
    ) {
        let input = format!("{input} 0"); // Add dummy sequence length.
        let gtree = GeneTree::from_input(&input, interner).unwrap();
        let mut expected_index = HashMap::new();
        for &(k, v) in &index {
            expected_index.insert(k, v);
        }
        let act = gtree
            .index
            .iter()
            .map(|(&k, &v)| (interner.resolve(k).unwrap(), v))
            .collect::<HashMap<_, _>>();
        assert_eq!(expected_index, act);
    }

    #[test]
    fn guards() {
        let mut interner = Interner::new();
        let mut err = |input, message| {
            println!("Check parse failure of:\n  {input}");
            let input = format!("{input} 0"); // Append dummy sequence length.
            match GeneTree::from_input(&input, &mut interner) {
                Err(e) => assert_eq!(message, format!("{e}")),
                Ok(_) => panic!("Unexpected successful parse."),
            }
        };
        // No default branch length for non-root nodes.
        err(
            "(A:1,B);",
            "Missing branch data after terminal node 2 payload \"B\" (found ')' instead of ':').",
        );
        err(
            "(A:1,B:);",
            "Could not parse \"\" as an external branch data (f64) (node 2): \
             cannot parse float from empty string",
        );
        err(
            "((A:1,B:2):, D:3);",
            "Could not parse \"\" as an internal branch data (f64) (node 1): \
             cannot parse float from empty string",
        );
        err(
            "((A:1,B:2),C:3);",
            "Missing branch data after internal node 1 payload \"\" (found ',' instead of ':').",
        );
        // Forbid duplicate names.
        err(
            "(A:1,A:2);",
            "Node name \"A\" is given to node 2 but it was already given to node 1.",
        );
        // Forbid leaf name elision.
        err("(A:1,:2);", "No name given to leaf node 2.");
    }
}
