// Use homebrew BFGS search
// to find scores values yielding maximum log-likelihood.
// Model log-likelihood as -F(X, P)
// with 'X' the 'data', constant throughout the optimisation,
// and 'P' the 'parameters' to optimize,
// and that we wish to derive F with respect to.

use std::{cell::LazyCell, fmt::Write};

use color_print::{cformat, cprintln};
use rayon::iter::{IndexedParallelIterator, IntoParallelRefIterator, ParallelIterator};
use serde::Serialize;
use snafu::{ResultExt, Snafu};
use tch::{Device, Tensor};

use crate::{
    config::Search,
    gene_tree::TripletTopology,
    io::{self, ToRelative},
    model::{
        likelihood::{self, data_tensors, ln_likelihood_tensors},
        parameters::GeneFlowTimes,
        scenarios::Scenario,
        scores::{self, Scores},
    },
    optim::{self, OptimResult},
    output, GeneTriplet, Parameters,
};

// One search success/failure status.
pub type Status = Result<BestFound, optim::Error>;

// Return both optimized inputs and output.
#[derive(Debug, Serialize)]
pub struct BestFound {
    /// The best parameters found during this search.
    pub parameters: Parameters<f64>,
    /// The unconstrained scores corresponding to these parameters.
    #[serde(serialize_with = "scores::ser")]
    pub scores: Scores<f64>,
    /// The corresponding gradient values for the scores at this point.
    #[serde(serialize_with = "scores::ser")]
    pub gradient: Scores<f64>,
    /// The best likelihood value found.
    pub ln_likelihood: f64,
    /// The number of times the likelihood function has been evaluated to find this result.
    pub n_evaluations: u64,
    /// The number of times the likelihood *derivative* has been evaluated to find this result.
    pub n_differentiations: u64,
}

// Aphid procedure to calculate a meaningful exploration starting point.
#[allow(clippy::similar_names)] // (ab, ac, bc)
pub fn heuristic_starting_point(
    triplets: &[GeneTriplet],
    theta: f64,
    gf_times: &GeneFlowTimes<f64>,
) -> Parameters<f64> {
    use TripletTopology as T;
    #[allow(clippy::cast_precision_loss)] // TODO: fail in case of precision loss?
    let (n_triplets, n_gf_times) = (triplets.len() as f64, gf_times.0.len() as f64);

    // Sum triplet branches lengths among the forest.
    let mut lengths = [0; 4];
    // Count discordant triplets.
    let [mut n_ac, mut n_bc] = [0; 2];
    for t in triplets {
        for (l, sum) in t.local.branches_lengths.iter().zip(lengths.iter_mut()) {
            *sum += l;
        }
        match t.local.topology {
            T::ABC => {}
            T::ACB => n_ac += 1,
            T::BCA => n_bc += 1,
        }
    }
    // Turn into means.
    let mean = |s| f64::from(s) / n_triplets;
    let [m_a, m_b, m_c, m_d] = lengths.map(mean);
    let [f_ac, f_bc] = [n_ac, n_bc].map(mean);

    // Apply formulae.
    let tau_1 = (m_a + m_b) * theta / 4.;
    let tau_2 = (m_c + m_d) * theta / 4. + tau_1;
    let p_ils = likelihood::p_ils(theta, tau_1, tau_2);
    let p_ac = f_ac - p_ils / 3.;
    let p_bc = f_bc - p_ils / 3.;
    let p_ab = (p_ac + p_bc) / 2.;
    let p_ancient = 1. / n_gf_times;

    Parameters {
        theta,
        tau_1,
        tau_2,
        p_ab,
        p_ac,
        p_bc,
        p_ancient,
        gf_times: gf_times.clone(),
    }
}

// Spin several optimisation threads,
// gathering either optimized results or errors.
pub fn optimize_likelihood(
    triplets: &[GeneTriplet],
    starts: &[Parameters<(f64, bool)>],
    search: &[Search],
) -> Result<Vec<Status>, Error> {
    println!("Launch {} independent optimisation searches:", starts.len());

    let statuses = starts
        .par_iter()
        .zip(search.par_iter())
        .map(|(p, s)| -> Result<Status, Error> {
            #[derive(Serialize)]
            struct Record {
                value: f64,
                fixed: bool,
            }
            write_to_file(
                &p.map(|&(value, fixed)| Record { value, fixed }),
                s.bfgs.log.as_ref(),
                output::file::trace::INIT,
                "init",
                "optimisation starting point",
            )?;
            let status = optimize_likelihood_single(triplets, p, s)?;
            // Write status in adacent file.
            write_to_file(
                &status,
                s.bfgs.log.as_ref(),
                output::file::trace::STATUS,
                "status",
                "optimisation status",
            )?;
            Ok(status)
        })
        // There are two nested levels of "errors" here:
        // abort early in case of e.g. i/o error to interrupt the program,
        // but keep going in case of search errors
        // that need to be displayed as a regular search 'status'.
        .collect::<Result<Vec<Status>, Error>>()?;

    // Still, fail if *all* statuses are 'failed'.
    if statuses.iter().all(Result::is_err) {
        let mut list = String::new();
        for (start, status) in starts.iter().zip(statuses.into_iter()) {
            let e = status.unwrap_err();
            writeln!(list, "---\n{start:?}\n ↓\n{e}").unwrap();
        }
        return AllFailedErr { list }.fail();
    }

    Ok(statuses)
}

fn write_to_file(
    content: &impl Serialize,
    log: Option<&optim::bfgs::Log>,
    filename: &'static str,
    short: &'static str,
    what: &'static str,
) -> Result<(), Error> {
    if let Some(log) = log {
        let mut path = log.folder.clone();
        path.push(filename);
        let file = io::create_file(&path, short)?;
        serde_json::to_writer_pretty(file, &content).with_context(|_| SerErr { what })?;
    }
    Ok(())
}

// Optimize from one single starting point.
pub fn optimize_likelihood_single(
    // Receive in this format for better locality.
    triplets: &[GeneTriplet],
    start: &Parameters<(f64, bool)>,
    search: &Search,
) -> Result<Status, Error> {
    // Tensors are small and control flow depends a lot on their values,
    // so roundtrips to a Gpu are not exactly interesting
    // in terms of performances.
    let dev = Device::Cpu;

    // Keep a copy of initial parameters around as a source tensor.
    let drop_fixed = |vf: &(_, _)| vf.0;
    let init = start.iter().map(drop_fixed).collect::<Vec<_>>(); // (into vector)
    let init = Tensor::from_slice(&init).to_device(dev); // (into tensor)
    let init = Parameters::from_iter(tensor_values(&init)); // (into structured parms)
    let init = init.into_merged_along(start, |init, &(_, fixed)| (init, fixed)); // (restore fix info)

    // Find out which scores correspond to this desired start.
    let scores = start.map(drop_fixed).to_scores();

    // In principle, these form the whole input tensor 'P', tracked for the gradient..
    let p = scores.iter().copied().collect::<Vec<_>>();
    let p = Tensor::from_slice(&p).to_device(dev);

    // .. but user's 'forced-fixed' parameters actually cut paths in the computation graph
    // as their value override the ones naturally flowing from the scores -> parm transformation,
    // making scores conceptually a mixture between 'P' and 'X'.
    // Viewed another way: some parameters within 'P' are not actually optimized
    // because they are being forced/overriden downstream.
    let parameters = |s: &Tensor| {
        // On every evaluation, construct parameters from the source tensor..
        let scores = Scores::from_iter(tensor_values(s));
        let parms = scores.to_parameters(); // <- (autograd graph construction starts here)

        // .. then override parameters with forced ones, effectively cutting graphs paths.
        let mut forced = init.iter();

        // This is what the likelihood is calculated with.
        parms.map(|tracked| {
            let (init, fixed) = forced.next().unwrap();
            if *fixed { init } else { tracked }.alias()
        })
    };

    // Extract all remaining data 'X' from the triplets.
    let ngf = start.n_gf_times();
    let scenarios = Scenario::iter(ngf).collect::<Vec<_>>();
    let n_scenarios = scenarios.len();
    let x = data_tensors(triplets, n_scenarios, &scenarios, dev);

    // Learn over the whole dataset.
    let f = |s: &Tensor| -ln_likelihood_tensors(&x, &parameters(s));

    let prefix = LazyCell::new(|| cformat!("<s>{}::</> ", search.bfgs.log.as_ref().unwrap().id));
    if let Some(log) = &search.bfgs.log {
        if log.main.is_some() {
            cprintln!(
                "{}Recording BFGS search at <b>{}</>.",
                *prefix,
                log.folder.to_relative()?.display()
            );
        }
    }

    // Log every step if a file was provided.
    let opt = match search.bfgs.minimize(f, &p) {
        Ok(o) => o,
        Err(e) => {
            // Failing to optimize is a valid status.
            cprintln!("<k>{}{e}</>", *prefix);
            cprintln!("{}<y>🗙</> Heuristic failed.", *prefix);
            return Ok(Err(e));
        }
    };

    // Final value.
    let vars = opt.best_vars().set_requires_grad(true);
    let loss = f(&vars);
    loss.backward();
    let grad = vars.grad();
    let scores = Scores::from_iter(tensor_values(&vars));
    cprintln!(
        "{}<g>✓</> Terminate heuristic after \
         <g>{}</> evaluations and \
         <g>{}</> differentiations.",
        *prefix,
        opt.n_eval(),
        opt.n_diff(),
    );

    let scores = scores.to_floats();
    Ok(Ok(BestFound {
        parameters: parameters(&vars).to_floats(),
        scores,
        gradient: Scores::from_iter(tensor_values(&grad)).to_floats(),
        ln_likelihood: -opt.best_loss(),
        n_evaluations: opt.n_eval(),
        n_differentiations: opt.n_diff(),
    }))
}
fn tensor_values(t: &Tensor) -> impl Iterator<Item = Tensor> + '_ {
    let n = t.size1().expect("Can't retrieve tensor size.");
    (0..)
        .map(move |i| t.get(i))
        .take(usize::try_from(n).expect("Tensor size too big for usize?"))
}

//==================================================================================================
// Errors.

#[derive(Debug, Snafu)]
#[snafu(context(suffix(Err)))]
pub enum Error {
    #[snafu(display(
        "The following initial parameters yielded non-finite log-likelihood ({lnl}) \
         even when (naively) subsampling the data:\n{parms:#?}"
    ))]
    NonFiniteLikelihood { lnl: f64, parms: Parameters<f64> },
    #[snafu(transparent)]
    Optim { source: optim::Error },
    #[snafu(transparent)]
    Io { source: io::Error },
    #[snafu(display("Error while serializing {what}: {source}."))]
    Ser {
        what: String,
        source: serde_json::Error,
    },
    #[snafu(display("All searches failed:\n{list}"))]
    AllFailed { list: String },
}
