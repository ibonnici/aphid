# v0.12.1 Force-fix parameters.

Parameters can now be set to a constant, unexplored value
from the `[init]` table in the config file.
See documentation for the `[init]` table.
