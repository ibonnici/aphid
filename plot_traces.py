"""Debug plotting of optimisation traces.
"""

from matplotlib import colormaps
import matplotlib.pyplot as plt
import numpy as np
import polars as ps
import scipy as sc

bfgs = "./target/bfgs.csv"
wolfe = "./target/bfgs_linsearch.csv"
grid = "./target/grid.csv"
# Theoretical min.
optx, opty = (0, -1)

# Rough'n brutal 'interactive animation' procedure:
# dismiss any data after this step.
MAX_STEP = 4
MAX_STEP = float("+inf")

bfgs = ps.read_csv(bfgs)
wolfe = ps.read_csv(wolfe)
try:
    grid = ps.read_csv(
        grid,
        has_header=False,
        new_columns=["x", "y", "z"],
        separator="\t",
    )
except FileNotFoundError:
    grid = None


def group_colnames(cols: list[str]) -> list[str | list[str]]:
    """Extract columns into clusters by name.
    >>> group_colnames(["a", "b_1", "b_2", "c", "d_1", "e_1", "e_2"])
    ['a', ['b_1', 'b_2'], 'c', ['d_1'], ['e_1', 'e_2']]
    """
    res = []
    previous_name = None
    for c in cols:
        try:
            name, number = c.rsplit("_", 1)
            number = int(number)
        except:
            name, number = c, None
        if number is None:
            res.append(c)
        else:
            if previous_name is None or previous_name != name:
                res.append([c])
            else:
                res[-1].append(c)
        previous_name = name
    return res


def group_columns(d: ps.DataFrame):
    """Second step to extract values from columns."""
    res = []
    for c in group_colnames(d.columns):
        if type(c) is list:
            res.append([d[name] for name in c])
        else:
            res.append(d[c])
    return res


bfgs = bfgs.filter(bfgs["step"] <= MAX_STEP)
wolfe = wolfe.filter(wolfe["id"] <= MAX_STEP)

# ---------------------------------------------------------------------------------------
# BFGS plots:
# First panel is the loss and step size.
# Next panels are the non-scalar values,
# with a color for every optimized input variable.
step, loss, step_size, vars, grad, direction = group_columns(bfgs)

fig, [ax, *vec_axes] = plt.subplots(4)
fig.subplots_adjust(
    right=0.75,
    left=0.05,
    top=0.95,
    bottom=0.01,
)

# Basic loss and step size.
loss_ax, size_ax = [ax, ax.twinx()]
loss_c, size_c, grad_c = ("blue", "red", "green")
loss_ax.plot(step, loss, c=loss_c, marker=".")
loss_ax.set_ylabel("Loss", color=loss_c)
size_ax.plot(step, step_size, c=size_c, marker=".")
size_ax.set_ylabel("Step size", color=size_c)
size_ax.set_yscale("log")
loss_ax.set_yscale("log")
loss_ax.set_title("BFGS")

# Non-scalar plots.
cm = colormaps["viridis"]
palette = [cm(i) for i in np.linspace(0, 1, len(vars))]
for vec, ax, name in zip(
    (vars, grad, direction), vec_axes, ("variables", "gradient", "direction")
):
    ax.set_title(name)
    ax.get_yaxis().set_visible(False)
    for i, (y, col) in enumerate(zip(vec, palette)):
        twin = ax.twinx()
        pos = 1 + i / 20
        twin.spines["right"].set_position(("axes", pos))
        twin.get_yaxis().get_offset_text().set_position((pos, 1.1))
        twin.set_frame_on(True)
        twin.patch.set_visible(False)
        twin.set_ylabel(i, rotation=0)
        twin.tick_params(axis="y", colors=col)
        twin.plot(step, y, color=col, label=i, marker=".")
        twin.set_yscale("symlog")

bfgs_vars = vars  # Useful in the next

# ---------------------------------------------------------------------------------------
# If a grid is available, draw the 2D trajectory.
if grid is not None:
    # Convert to image map.
    n = round(np.sqrt(len(grid)))
    x, y, z = grid["x"], grid["y"], grid["z"]
    minx, miny, maxx, maxy = (f(i) for f in (min, max) for i in (x, y))
    assert max(abs(x.unique().to_numpy() - np.linspace(minx, maxx, n))) < 1e-14
    assert max(abs(y.unique().to_numpy() - np.linspace(miny, maxy, n))) < 1e-14
    x = x[0:-1:n]
    y = y[0:n]
    z = z.to_numpy().reshape((n, n)).transpose()
    # Find minima within the matrix.
    shifts = [
        (range(1, n), range(n - 1, n)),
        (range(0, 0), range(n)),
        (range(0, 1), range(0, n - 1)),
    ]
    cat = np.concatenate
    minima = np.ones((n, n), bool)
    for ia, ib in shifts:
        a, b = z[:, ia], z[:, ib]
        for jc, jd in shifts:
            ac, ad = a[jc], a[jd]
            bc, bd = b[jc], b[jd]
            shifted = cat([cat([ac, ad]), cat([bc, bd])], 1)
            minima &= z <= shifted
    argminy, argminx = sc.signal.argrelmax(minima)
    low_x, low_y = x[argminx], y[argminy]
    az = int(np.argmin(z))
    globmin_x, globmin_y = x[az % n], y[az // n]
    # Plot.
    fig, ax = plt.subplots()
    ax.pcolormesh(x, y, z, norm="symlog", vmin=0)
    # Display minima.
    ax.scatter(low_x, low_y, c="red", zorder=2, s=0.1)
    ax.scatter(globmin_x, globmin_y, c="pink", zorder=2)
    [
        f(v, color="black", alpha=0.5, zorder=2, lw=0.5)
        for f, v in [(ax.axvline, optx), (ax.axhline, opty)]
    ]
    # Superimpose trajectory.
    x, y = vars
    ax.plot(x, y, c="black", linewidth=1)
    ax.scatter(x, y, c=range(len(x)), zorder=2)
    ax2d = ax  # Remember to later add linear search.

(
    id,
    alpha,
    loss,
    phigrad,
    vars,
    grad,
) = group_columns(wolfe)

# ---------------------------------------------------------------------------------------
# Wolfe searches plots.
# Vertical bars delimit successive searches,
# major x ticks show the BFGS steps identifiers,
# minor x ticks show the search duration.
# Plot the loss progress during every search.
# Also plot the step size search, wrapped by its supposedly shrinking search range.
search_sizes = ps.DataFrame(id).group_by("id", maintain_order=True).len()["len"]
steps = search_sizes.cum_sum() - 1
fig, (axa, axl, axg) = plt.subplots(3)
axa.axhline(1, color="black", lw=1)
for x in (axa, axg):
    x.set_xticks(steps, labels=range(len(steps)))
    size_ticks = steps - search_sizes / 2
    x.set_xticks(size_ticks, search_sizes, minor=True, color="gray")
[x.set_yscale("log") for x in (axa, axl, axg)]
axa.set_ylabel("Step size", c=size_c)
axl.plot(loss, color=loss_c)
axl.scatter(range(len(loss)), loss, color=loss_c, s=5)
axl.set_ylabel("Loss", color=loss_c)
axg.set_ylabel("Step derivative", c=grad_c)
prev = 0
for i_step, s in enumerate(steps):
    x = range(prev, s + 1)
    axa.plot(x, alpha[x], color=size_c)
    axa.scatter(x, alpha[x], color=size_c, s=5, zorder=10)
    axg.plot(x, phigrad[x], color=grad_c)
    axg.scatter(x, phigrad[x], color=grad_c, s=5)
    [x.axvline(s, color="black", lw=1, alpha=0.5) for x in (axa, axl, axg)]
    prev = x.stop
    if grid is not None:
        # Append linear search steps.
        x, y = (i[x] for i in vars)
        startx, starty = [v[i_step] for v in bfgs_vars]
        x, y = (np.concatenate([[s], v]) for s, v in [(startx, x), (starty, y)])
        (ax2d, minx, miny, maxx, maxy) = (ax2d, minx, miny, maxx, maxy)  # type: ignore
        opacity = 0.2 if i_step < len(steps) - 1 else 1
        ax2d.plot(x, y, c="blue", linewidth=0.5, zorder=1, alpha=opacity)
        ax2d.scatter(
            x,
            y,
            c=range(len(x)),
            cmap=colormaps["plasma"],
            s=2,
            zorder=3,
            alpha=opacity,
        )
        dx, dy = np.diff(x), np.diff(y)
        ax2d.quiver(
            x[:-1],
            y[:-1],
            0.1 * dx,
            0.1 * dy,
            range(len(x) - 1),
            cmap=colormaps["Reds"],
            alpha=opacity,
            angles="xy",
            scale_units="xy",
            scale=1,
            units="dots",
            width=2,
            zorder=2,
        )
        ax2d.scatter(x[-1], y[-1], marker="x", c="red", alpha=opacity)
        # [pseudo-animation]: Only extend the limits if the "current step" is off.
        ax2d.set_xlim(min(minx, min(x)), max(maxx, max(x)))
        ax2d.set_ylim(min(miny, min(y)), max(maxy, max(y)))
plt.show()
