# Aphid

This is a Rust implementation of the "Aphid" method,
following its specifications within Nicolas Galtier's draft paper
*Phylogenetic conflicts:
distinguishing gene flow from incomplete lineage sorting* (2023).

Sources available at: https://gitlab.com/iago-lito/aphid/  
Documentation at: https://iago-lito.gitlab.io/aphid/
