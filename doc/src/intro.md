<!-- build the project with the doc helper utils -->
<!-- cmdrun cargo build --manifest-path ../../Cargo.toml --features 'build-doc' -->
<!-- cmdrun cargo +nightly rustdoc --manifest-path ../../Cargo.toml --lib -- --output-format json -Z unstable-options -->

<!-- prepare convenience aliases -->
<!-- cmdrun ln -s ../../target/debug/doc . -->
<!-- cmdrun ln -s ../../target/debug/aphid . -->

# Aphid

This is the technical documentation
for the `aphid` method:
distinguishing gene flow from incomplete lineage sorting,
and the associated command-line program:

- The original paper describing the method:
  [https://doi.org/10.24072/pcjournal.359](https://doi.org/10.24072/pcjournal.359).
- The associated program to use the method:
  [https://gitlab.com/iago-lito/aphid][repo].

This document assumes that you are familiar
with the method described in the paper.
Reader interested in the context, the intuition,
or the meaning of the method
are encouraged to refer to the original paper instead,
as these are not covered here.

This document explains how to install the `aphid` program
and how to use it.
It also specifies the detail of expected inputs,
of the calculation performed within the program
and the outputs produced.

Bug reports, feature requests and contributions are welcome [here][repo].

[repo]: https://gitlab.com/iago-lito/aphid/
