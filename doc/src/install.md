# Installation

Either use a container or compile or your own system.
Neither option should be difficult.

## With Apptainer/Singularity

Aphid can be run from an apptainer
provided you have [Apptainer] (aka. Singularity) installed.

First, build the container image as a super user:

```sh
# Get definition file.
curl -L \
  https://gitlab.com/iago-lito/aphid/-/jobs/artifacts/main/raw/singularity.def?job=container-recipes \
  > aphid.def # (or download by hand)

# Build image.
sudo apptainer build aphid.sif aphid.def
```
<div class="warning">
TODO: provide image as an artefact?
</div>

Then run the container as regular user:

```sh
./aphid.sif --help
```

[Apptainer]: https://apptainer.org/

## With Docker

Aphid can be run from a docker container,
provided you have [Docker] installed.

First, build the container image:
```sh
docker buildx build -t aphid \
  https://gitlab.com/iago-lito/aphid/-/jobs/artifacts/main/raw/Dockerfile?job=container-recipes
```

<div class="warning">
TODO: provide image as an artefact?
</div>

Then run the container:
```sh
docker run --rm -it -v ${PWD}:/home/aphid aphid --help
```

[Docker]: https://www.docker.com/

## Manual compilation

The program is still fairly easy to compile yet,
provided you have [git], [torch]
and a [rust stable toolchain] installed on your system.

First clone and compile the project:

```sh
git clone --recursive https://gitlab.com/iago-lito/aphid/
cd aphid
cargo test --release
cargo build --release
```

Then run the binary obtained:

```sh
./target/release/aphid --help
```

[git]: https://git-scm.com/
[torch]: https://pytorch.org/get-started/locally/
[rust stable toolchain]: https://www.rust-lang.org/tools/install
