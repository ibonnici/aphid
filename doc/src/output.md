# Outputs

When run, for instance with:

```sh
$ aphid ./config.toml output
```


Aphid displays summarized information about the analysis
on the console standard output.
This output is only meant as an informative summary for humans.
It is not supposed to be easily processed by downstream programs
and its layout can arbitrarily change in the future.

In addition,
aphid outputs very detailed structured information about the analysis
under the form of a collection of files in a newly created folder
named after its second argument (the `output/` folder in the above example).
Here is what this folder contains:

```
<OUTPUT>/ # The folder name as per the second argument given on the command line.
├── config.json # Complete information about the configuration used for this run.
├── global.json # The most important "forest-level" results.
├── detail.json # Detailed results per gene tree.
├── trees.csv   # Summarized results per gene tree in tabular form.
└── search/ # Traces of the heuristic searches for likelihood (one per starting point).
    ├── 1/
    │   ├── init.csv    # Starting point for this search.
    │   ├── status.json # Result of the search (error or best parameters found).
    │   ├── global.csv  # Every BFGS step.
    │   └── detail.csv  # Every linear search step.
    ├── 2/ …
    ├── 3/ …
    ⋮
```

These files are formally structured with `.json` or `.csv` format
to ease their processing by downstream programs.
The meaning of their content is detailed below:

<!-- toc -->

<!-- TODO have rustdoc automatically list the fields? -->

## The `global.json` file { #global }
<!-- cmdrun ./doc output::global::Global -->

<!-- cmdrun ./fglobal Global n_trees -->
<!-- cmdrun ./fglobal Global n_excluded_triplets_topologies -->
<!-- cmdrun ./fglobal Global n_unresolved_triplets -->
<!-- cmdrun ./fglobal Global n_excluded_outgroup_topologies -->
<!-- cmdrun ./fglobal Global n_excluded_topologies -->
<!-- cmdrun ./fglobal Global mean_branch_length -->
<!-- cmdrun ./fglobal Global mean_length_triplet -->
<!-- cmdrun ./fglobal Global mean_length_outgroup_other -->
<!-- cmdrun ./fglobal Global imbalance -->
<!-- cmdrun ./fglobal Global triplet_longer -->
<!-- cmdrun ./fglobal Global shape -->
<!-- cmdrun ./fglobal Global n_excluded_geometries -->
<!-- cmdrun ./fglobal Global n_included_trees -->
<!-- cmdrun ./fglobal Global estimate -->
  <!-- cmdrun ./fglobal Estimate ln_likelihood -->
  <!-- cmdrun ./fglobal Estimate parameters -->

## The `detail.json` file { #detail }
<!-- cmdrun ./doc output::detail::Tree -->

<!-- cmdrun ./fdetail Tree id -->
<!-- cmdrun ./fdetail Tree n_bases -->
<!-- cmdrun ./fdetail Tree n_nodes_raw -->
<!-- cmdrun ./fdetail Tree n_nodes_pruned -->
<!-- cmdrun ./fdetail Tree triplet -->
  <!-- cmdrun ./fdetail Triplet lca -->
  <!-- cmdrun ./fdetail Triplet missing -->
  <!-- cmdrun ./fdetail Triplet paraphyletic -->
  <!-- cmdrun ./fdetail Triplet analysis -->
    <!-- cmdrun ./fdetail TripletAnalysis topology -->
    <!-- cmdrun ./fdetail TripletAnalysis branches_lengths -->
    <!-- cmdrun ./fdetail TripletAnalysis resolved -->
  <!-- cmdrun ./fdetail Triplet included -->
<!-- cmdrun ./fdetail Tree outgroup -->
  <!-- cmdrun ./fdetail Outgroup lca -->
  <!-- cmdrun ./fdetail Outgroup missing -->
  <!-- cmdrun ./fdetail Outgroup paraphyletic -->
  <!-- cmdrun ./fdetail Outgroup included -->
<!-- cmdrun ./fdetail Tree top -->
  <!-- cmdrun ./fdetail Top lca -->
  <!-- cmdrun ./fdetail Top internal -->
    <!-- cmdrun ./fdetail Internal triplet -->
    <!-- cmdrun ./fdetail Internal outgroup -->
  <!-- cmdrun ./fdetail Top external -->
  <!-- cmdrun ./fdetail Top included -->
<!-- cmdrun ./fdetail Tree topology_included -->
<!-- cmdrun ./fdetail Tree mean_lengths -->
  <!-- cmdrun ./fdetail MeanLengths total -->
  <!-- cmdrun ./fdetail MeanLengths triplet -->
  <!-- cmdrun ./fdetail MeanLengths outgroup_other -->
<!-- cmdrun ./fdetail Tree local_shape -->
<!-- cmdrun ./fdetail Tree geometry_included -->
<!-- cmdrun ./fdetail Tree mutation_rate -->
<!-- cmdrun ./fdetail Tree ln_likelihood -->

## The summarized `trees.csv` table { #summary }

There is one line in this table per gene tree analyzed.
Columns represent a redundant, flattened version
of the structured information available in the above `detail.json` file,
but we expect that it be easier to work with
using table-processing downstream software.

## The `search/` traces  { #search }

Since several starting point may be used
in the likelihood maximization heuristics,
aphid may produce several exploration traces stored within subfolders here.

### The `init.json` file. # { init }

This file is a reminder which initial parameters have been used for the search.

### The `status.json` summary. { #status }

This file summarizes the terminal status of the search:
either detail about search failure if it failed,
or the following information:

<!-- cmdrun ./structfield learn::BestFound parameters -->
<!-- cmdrun ./structfield learn::BestFound scores -->
<!-- cmdrun ./structfield learn::BestFound gradient -->
<!-- cmdrun ./structfield learn::BestFound ln_likelihood -->
<!-- cmdrun ./structfield learn::BestFound n_evaluations -->
<!-- cmdrun ./structfield learn::BestFound n_differentiations -->

### The `global.csv` trace. { #global-trace }

If produced,
there is one line in this table per BFGS step taken during the search.
(see [BFGS](./learn.md#bfgs))

### The `detail.csv` trace. { #local-trace }

If produced,
there is one line in this table per linear search step taken
for every BFGS step in search for a step size meeting strong Wolfe criteria.
(see [Linear Search](./learn.md#wolfe))
