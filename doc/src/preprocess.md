# Preprocessing

This chapter explains how gene trees from the input gene forest
are processed by aphid prior to calculating likelihood.

When referring to a tree,
the construct "`LCA(species_group)`"
refers to the single last common ancestor of these species in the tree,
*i.e.* the closest node upstream from all species in the group.

## Pruning { #prune }

Every gene tree is first pruned
so that only species of interest are kept.
Species of interest are species occuring in either
the [`triplet`](./use.md#triplet),
the [`outgroup`](./use.md#outgroup) or
the [`other`](./use.md#other)
section of the input [`[taxa]`](./use.md#taxa) table.

This process reduces the number of node in every tree,
but the branches lengths are conserved.
For instance,
pruning species
`A`, `C`, `E` and `H`
in the following raw gene tree:

```
     │17
 ┌───┴────────┐
 │            │16
 │      ┌─────┴──────┐
 │      │            │15
 │      │        ┌───┴────────┐
 │1     │6       │            │14
 │      │        │      ┌─────┴──┐
 │   ┌──┴──┐     │      │12      │
 │   │     │5    │7  ┌──┴──┐     │
 │   │2    │     │   │     │11   │13
 │   │   ┌─┴─┐   │   │8  ┌─┴─┐   │
 │   │   │3  │4  │   │   │9  │10 │
 A   B   C   D   E   F   G   H   I
```

results in the following pruned tree:
```
       │17
       │+
       │16
  ┌────┴─────┐
  │          │15
  │          │+
  │6         │14
  │       ┌──┴──┐
┌─┴─┐     │12   │
│   │   ┌─┴─┐   │
│2  │5  │   │11 │13
│   │+  │8  │+  │
│   │4  │   │9  │
B   D   F   G   I
```

The process may a leave an artefactual *scar* on the root branch length
when external nodes like `A` are pruned.

In subsequent analysis, every "tree" refers to a pruned tree
whose root branch length is ignored.

## Topology pass { #topology }

The topology of every tree is analyzed
with respect to the species of interest.
A tree is rejected if either:

- One or several `triplet` species are missing from the tree.
- The `triplet` species form a paraphyletic group.
- No `outgroup` species appear in the tree.
- The `outgroup` species form a paraphyletic group.
- The `LCA(triplet, outgroup)` is not the root of the tree.
- If the [`triplet_other_monophyly`](./use.md#tom) parameter is set:
  - One or several `other` species branch from the `outgroup` side of the root.

For example, given the following species of interest:
```toml
triplet_other_monophyly = false

[taxa]
triplet = "((T, U), V)"
outgroup = "O P Q"
other = "X Y Z"
```

The following tree topology is included:
```
                │
      ┌─────────┴───────────┐
      │                ┌────┴───┐
┌─────┴──────┐         │        │
│            │         │        │
│        ┌───┴──┐   ┌──┴──┐     │
│        │      │   │     │     │
│     ┌──┴──┐   │   │   ┌─┴─┐   │
│     │     │   │   │   │   │   │
│   ┌─┴─┐   │   │   │   │   │   │
Y   T   U   V   Z   O   P   Q   X
```

But it is excluded with
```toml
triplet_other_monophyly = true
```

because the `other` species `X` branches between `LCA(outgroup)` and the root,
instead of between `LCA(triplet)` and the root.

In subsequent analysis, every "tree"
refers to a tree with an accepted topology.

## Geometry pass { #geometry }

Three *"mean branch lengths"* are calculated for every gene tree \\(g\\) :
- The overal mean length from root to leaves,
  called `mean_length`:
  \\(l_g\\)
- The mean length from `LCA(triplet)` to the triplet species,
  called `mean_triplet`:
  \\(t_g\\)
- The mean length from root to either `outgroup` or `other` species
  (excluding `triplet` species),
  called `mean_rest`:
  \\(r_g\\)

If the [`max_clock_ratio`][mcr] parameter is set,
then the "shape" of every tree is calculated as
\\[
  q_g = \frac{t_g}{r_g}
\\]
And the "global shape" of the whole tree forest is calculated as
\\[
  Q = \frac{\sum_g{t_g}}{\sum_g{r_g}}
\\]

The 'imbalance' of every tree is calculated with respect to the whole forest.
\\[
  i_g = \max(\frac{Q}{q_g}, \frac{q_g}{Q})
\\]
If a tree has a high imbalance value,
it means that it is dissimilar to its enclosing forest,
and the hypothesis that mutation rate is constant accross the tree(s)
is weakened.

The tree geometry is rejected if its imbalance is greater than
[`max_clock_ratio`][mcr].

If the parameter is not set, no tree geometry is excluded.

[mcr]: ./use.md#mcr

In subsequent analysis, every "tree"
refers to a tree with an accepted geometry.

## Mutation rate estimation { #mutation }

The `global_mean_length` is calculated over the \\(G\\) remaining trees:
\\[
  L = \frac{\sum_g{l_g}}{G}
\\]

And the relative `mutation_rate` of every tree is calculated as:
\\[
  𝛼_g = \frac{l_g}{L}
\\]

This mutation rate is the value used during likelihood calculation,
along with the corresponding gene sequence length.
